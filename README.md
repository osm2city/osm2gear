About
=====

Please start with reading the [readme](https://gitlab.com/osm2city) at the group level.

**osm2gear** is a set of procedural programs, which create plausible [FlightGear](http://www.flightgear.org) scenery objects (buildings, roads, power lines, piers, etc.) based on [OpenStreetMap](http://www.osm.org/) (OSM) data.

It is intended as a replacement for the Python based [osm2city](https://gitlab.com/osm2city) with the goal to be 5-10x faster and use significantly less memory, such that additional OSM data points can be included without further negatively impacting resource usage during generation.

NB: this is experimental and incomplete code currently in the proof-of-concept stage - while the author at the same time learns modern C++.

There is extensive documentation included in [osm2city](https://gitlab.com/osm2city).
