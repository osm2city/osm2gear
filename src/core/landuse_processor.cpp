// SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#include "landuse_processor.h"

#include <cassert>
#include <map>
#include <memory>
#include <optional>
#include <random>
#include <utility>
#include <vector>

#include "../osm/constants.h"
#include "../proto/proto_writer.h"
#include "aptdat_io.h"
#include "boost/log/trivial.hpp"
#include "buildings.h"
#include "geos/geom/Geometry.h"
#include "geos/operation/buffer/BufferOp.h"
#include "geos/simplify/TopologyPreservingSimplifier.h"
#include "models.h"
#include "parameters.h"
#include "plotting.h"
#include "trees.h"
#include "would_be_buildings.h"

static std::map<int, std::unique_ptr<geos::geom::Polygon>> CreateGridIndexBoxes () {
    std::map<int, std::unique_ptr<geos::geom::Polygon>> grid_index_boxes;
    auto [min_coord, max_coord] = TileHandler::Get()->GetTileExtentLocal();
    const auto grids_x = static_cast<int>((max_coord.x - min_coord.x) / Parameters::Get().OWBB_GRID_SIZE) + 1;
    const auto grids_y = static_cast<int>((max_coord.y - min_coord.y) / Parameters::Get().OWBB_GRID_SIZE) + 1;
    int index {0};
    for (int x = 0; x < grids_x; ++x) {
        for (int y = 0; y < grids_y; ++y) {
            ++index;
            std::unique_ptr<geos::geom::Polygon> box = MakeLocalBox(min_coord.x + x * Parameters::Get().OWBB_GRID_SIZE,
                                                                    min_coord.y + y * Parameters::Get().OWBB_GRID_SIZE,
                                                                    min_coord.x + (x + 1) * Parameters::Get().OWBB_GRID_SIZE,
                                                                    min_coord.y + (y + 1) * Parameters::Get().OWBB_GRID_SIZE);
            grid_index_boxes[index] = std::move(box);
        }
    }
    return grid_index_boxes;
}

static std::vector<std::unique_ptr<BuildingZone>> ProcessBuildingZones(const OSMReadResult &result,
                                                                       const OSMReadRelationsResult &rel_result,
                                                                       const std::string &what) {
    std::vector<std::unique_ptr<BuildingZone>> zones {};
    for (auto & it : result.ways_dict) {
        std::optional<BuildingZoneType> bzt = ParseTagsForBuildingZoneType(it.second->GetTags());
        if (not bzt.has_value()) {
            continue;
        }
        try {
            std::unique_ptr<geos::geom::Polygon> geom = it.second->PolygonFromOSMWay(result.nodes_dict);
            auto bz = std::make_unique<BuildingZone>(it.second->GetOSMId(), std::move(geom),
                                                                              bzt.value(), BuildingZoneOrigin::osm);
            zones.push_back(std::move(bz));
        } catch (const InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Total relations: " << rel_result.relations_dict.size();
    for (auto & it : rel_result.relations_dict) {
        std::optional<BuildingZoneType> bzt = ParseTagsForBuildingZoneType(it.second->GetTags());
        if (not bzt.has_value()) {
            continue;
        }
        bool is_first {true};
        auto polys = it.second->PolygonsFromAllMembers(rel_result, true);
        for (auto & poly : polys) {
            osm_id_t the_id = is_first ? it.second->GetOSMId() : IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::landuse);
            auto bz = std::make_unique<BuildingZone>(the_id, std::move(poly),
                                                                              bzt.value(), BuildingZoneOrigin::osm);
            zones.push_back(std::move(bz));
            if (is_first) {
                is_first = false;
            }
        }
    }
    BOOST_LOG_TRIVIAL(info) << what << " building zones found: " << zones.size();
    return zones;
}

static std::vector<std::unique_ptr<BuildingZone>> ProcessAerodromes(const OSMDataReaderOverpass &osm_reader) {
    const OSMReadResult result = osm_reader.FetchWaysFromKeyValues({{k_aeroway, v_aerodrome}});
    const OSMReadRelationsResult rel_result = osm_reader.FetchRelationsAerodrome();
    return ProcessBuildingZones(result, rel_result, "aerodrome");
}

static std::vector<std::unique_ptr<BuildingZone>> ProcessBuiltUpLandUse(OSMDataReaderOverpass &osm_reader) {
    const OSMReadResult result = osm_reader.FetchWaysFromKeys({k_landuse});
    osm_reader.SetUseExtendedBoundary(true);
    const OSMReadRelationsResult rel_result = osm_reader.FetchRelationsLanduse();
    osm_reader.SetUseExtendedBoundary(false);
    return ProcessBuildingZones(result, rel_result, "built-up");
}

/*! \brief Fetches specific water areas from OSM and then applies a buffer.
 *
 */
static std::vector<std::unique_ptr<geos::geom::Geometry>> ProcessWaterAreas(const OSMDataReaderOverpass &osm_reader) {
    std::vector<std::unique_ptr<geos::geom::Geometry>> water_areas {};
    const OSMReadRelationsResult rel_result = osm_reader.FetchRelationsRiverbanks();
    for (auto & it : rel_result.relations_dict) {
        auto polys = it.second->PolygonsFromMembers(rel_result, MemberRole::outer);
        for (auto & poly : polys) {
            auto buffered_poly = poly->buffer(Parameters::Get().OWBB_BUILT_UP_BUFFER);
            water_areas.push_back(std::move(buffered_poly));
        }
    }
    // then add water areas (mostly when natural=water, but not always consistent)
    OSMReadResult result = osm_reader.FetchWaysFromKeyValues({{k_water, v_moat},
                                                              {k_water, v_river},
                                                              {k_water, v_canal},
                                                              {k_waterway, v_riverbank}});
    for (auto & it : result.ways_dict) {
        try {
            const std::unique_ptr<geos::geom::Polygon> poly = it.second->PolygonFromOSMWay(result.nodes_dict);
            auto buffered_poly = poly->buffer(Parameters::Get().OWBB_BUILT_UP_BUFFER);
            water_areas.push_back(std::move(buffered_poly));
        } catch (const InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
        }
    }
    return water_areas;
}

static std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> ProcessNaturalWater(const OSMDataReaderOverpass &osm_reader,
                                                                                    const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
    const OSMReadResult result = osm_reader.FetchWaysFromKeyValues({{k_natural, v_water}});
    const OSMReadRelationsResult rel_result = osm_reader.FetchRelationsNaturalWater();

    std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> natural_waters {};
    for (auto & it : result.ways_dict) {
        try {
            std::unique_ptr<geos::geom::Polygon> geom = it.second->PolygonFromOSMWay(result.nodes_dict);
            auto gipp = std::make_unique<GridIndexedPreparedPolygon>(std::move(geom)->buffer(Parameters::Get().TREES_NATURAL_WATER_BUFFER));
            gipp->AssignGridIndices(grid_index_boxes);
            if (gipp->HasGridIndices()) {
                natural_waters.push_back(std::move(gipp));
            }
        } catch (const InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
        }
    }
    BOOST_LOG_TRIVIAL(debug) << "Total relations for natural waters: " << rel_result.relations_dict.size();
    for (auto & it : rel_result.relations_dict) {
        auto polys = it.second->PolygonsFromAllMembers(rel_result, true);
        for (auto & poly : polys) {
            auto gipp = std::make_unique<GridIndexedPreparedPolygon>(std::move(poly)->buffer(Parameters::Get().TREES_NATURAL_WATER_BUFFER));
            gipp->AssignGridIndices(grid_index_boxes);
            if (gipp->HasGridIndices()) {
                natural_waters.push_back(std::move(gipp));
            }
        }
    }
    BOOST_LOG_TRIVIAL(info) <<  "Natural waters found: " << natural_waters.size();
    return natural_waters;
}

/*!
 * See RemoveOverlappingBuildingZoneParts(...)
 */
static void UpdateBuildingZonesFromOverlapTestOneBuilding(const BuildingZone* first_zone,
                                                          std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                                                          const int min_area) {
    std::vector<osm_id_t> to_remove {};
    std::vector<std::unique_ptr<BuildingZone>> to_add {};
    for (const auto &second_zone : built_up_zones) {
        if (first_zone->GetOSMId() == second_zone->GetOSMId()) {
            continue;
        }
        if (first_zone->GetBuildingZoneType() > second_zone->GetBuildingZoneType()) {
            continue;
        }
        auto parts = PolygonsFromGeometryDifference(second_zone->GetGeometry(), first_zone->GetGeometry(),
                                                    min_area);
        if (not parts.has_value()) {
            continue;
        }
        if (parts.value().empty()) {
            to_remove.push_back(second_zone->GetOSMId());
        } else {
            second_zone->SetGeometry(std::move(parts.value()[0]));
            for (std::size_t i = 1; i < parts.value().size(); ++i) {
                auto generated = std::make_unique<BuildingZone>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::landuse),
                                                                std::move(parts.value()[i]),
                                                                second_zone->GetBuildingZoneType(),
                                                                second_zone->GetBuildingZoneOrigin());
                to_add.push_back(std::move(generated));
            }
        }
    }
    for (const auto osm_id : to_remove) {
        auto itr = built_up_zones.begin();
        while (itr != built_up_zones.end()) {
            if (itr->get()->GetOSMId() == osm_id) {
                itr = built_up_zones.erase(itr);
            } else {
                ++itr;
            }
        }
    }
    for (auto &zone_to_add : to_add) {
        built_up_zones.push_back(std::move(zone_to_add));
    }
}

/*!
 * See RemoveOverlappingBuildingZoneParts(...)
 */
static void UpdateBuildingsZonesFromOverlapTestForType(const BuildingZoneType requested_type,
                                                       std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                                                       const int min_area) {
    // find all zones which have the requested type
    std::vector<osm_id_t> valid_first_zones {};
    for (auto &zone : built_up_zones) {
        if (zone->GetBuildingZoneType() == requested_type) {
            valid_first_zones.push_back(zone->GetOSMId());
        }
    }
    // now loop the requested zones - we do it this way because built_up_zones can be changed in every call and then
    // the iterators get invalidated.
    // even a requested zone might get removed due to overlap from zones with same requested type
    for (const auto osm_id : valid_first_zones) {
        for (auto &first_zone : built_up_zones) {
            if (first_zone->GetOSMId() == osm_id) {
                UpdateBuildingZonesFromOverlapTestOneBuilding(first_zone.get(), built_up_zones, min_area);
                break;
            }
        }
    }
}


/*! \brief Make sure that the geometry of building zones do not overlap.
 *
 * This is not only for correctness of representation, but also because e.g. trees would be generated twice
 * in overlaps.
 * It is not uncommon in OSN data that land-use zones drawn across each other.
 *
 * We are applying a hierarchy of zones. E.g. the geometry of aerodromes shall be more important then e.g. retail.
 *
 * @param built_up_zones
 */
static void RemoveOverlappingBuildingZoneParts(std::vector<std::unique_ptr<BuildingZone>> &built_up_zones) {
    const auto min_area = Parameters::Get().OWBB_MIN_CITY_BLOCK_AREA;
    //code below must be aligned with BuildingZoneType values
    UpdateBuildingsZonesFromOverlapTestForType(BuildingZoneType::aerodrome, built_up_zones, min_area);
    UpdateBuildingsZonesFromOverlapTestForType(BuildingZoneType::port, built_up_zones, min_area);
    UpdateBuildingsZonesFromOverlapTestForType(BuildingZoneType::industrial, built_up_zones, min_area);
    UpdateBuildingsZonesFromOverlapTestForType(BuildingZoneType::retail, built_up_zones, min_area);
    UpdateBuildingsZonesFromOverlapTestForType(BuildingZoneType::farmyard, built_up_zones, min_area);
    UpdateBuildingsZonesFromOverlapTestForType(BuildingZoneType::commercial, built_up_zones, min_area);
    UpdateBuildingsZonesFromOverlapTestForType(BuildingZoneType::residential, built_up_zones, min_area);
}

static std::vector<std::shared_ptr<Building>> RelateBuildingsToBuildingZones(std::vector<std::shared_ptr<Building>> &buildings,
                                                             std::vector<std::unique_ptr<BuildingZone>> &built_up_zones) {
    std::vector<std::shared_ptr<Building>> buildings_outside {};
    for (auto&& building : buildings) {
        bool found = false;
        for (auto&& zone : built_up_zones) {
            try {
                if (zone->ContainsOrIntersects(building->GetOuterPolygon())) {
                    zone->RelateBuilding(building);
                    found = true;
                    break;
                }
            } catch (const geos::util::TopologyException &) {
                BOOST_LOG_TRIVIAL(info) << "TopologyException for building " << building->GetOSMId() << " in zone " << zone->GetOSMId();
            }
        }
        if (not found) {
            buildings_outside.push_back(building);
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Buildings assigned to existing zones: " << buildings.size() - buildings_outside.size();
    BOOST_LOG_TRIVIAL(info) << "Buildings outside of existing zones: " << buildings_outside.size();
    return buildings_outside;
}

/*!
Checks whether a generated building zone's geometry is Multipolygon. If yes, then split into polygons.
Algorithm distributes buildings and checks that minimal size and buildings are respected.
 */
static std::vector<std::unique_ptr<BuildingZone>> SplitMultiPolygonBuildingZone(std::unique_ptr<BuildingZone> &&zone) {
    assert(zone->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTIPOLYGON);
    std::vector<std::unique_ptr<BuildingZone>> replacement_zones {};
    std::vector<std::unique_ptr<BuildingZone>> generated_replacement_zones {};
    const auto number_geometries = zone->GetGeometry()->getNumGeometries();
    //create new building zones
    for (std::size_t i = 0; i < number_geometries; ++i) {
        auto generated = std::make_unique<BuildingZone>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::landuse),
                                                        std::move(zone->GetGeometry()->getGeometryN(i)->clone()),
                                                        zone->GetBuildingZoneType(),
                                                        zone->GetBuildingZoneOrigin());
        generated_replacement_zones.push_back(std::move(generated));
    }
    //transfer the buildings from the original one
    while (zone->HasOSMBuildings()) {
        auto my_building = zone->PopOSMBuilding();
        my_building->SetZoneNull();
        //reallocate pointer between zone and building
        for (const auto & generated : generated_replacement_zones) {
            if (generated->ContainsOrIntersects(my_building->GetOuterPolygon())) {
                generated->RelateBuilding(my_building);
                break;
            }
        }
        if (not my_building->HasZone()) { // maybe no intersection -> fall back as each building needs a zone
            generated_replacement_zones[0]->RelateBuilding(my_building);
        }
    }
    //transfer to the result - but for generated from buildings only if they actually contain buildings
    // (there might not be in the split away part)
    for (auto & generated : generated_replacement_zones) {
        if (generated->GetBuildingZoneOrigin() == BuildingZoneOrigin::from_buildings and !generated->HasOSMBuildings()) {
            continue;
        } else {
            replacement_zones.push_back(std::move(generated));
        }
    }
    return replacement_zones;
}

//Reduces the holes in a polygon based on a minimum size and returns a new polygon.
//If the geometry is not a polygon or if the number of holes could not be reduced (e.g. there were none), then
//null is returned
std::optional<std::unique_ptr<geos::geom::Geometry>> ReduceSmallHolesInPolygon(const geos::geom::Geometry * original,
                                                                               const int min_area) {
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    // remove interior holes, which are too small
    if (original->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON) {
        const auto poly = dynamic_cast<const geos::geom::Polygon*>(original);
        const auto number_interior = poly->getNumInteriorRing();
        if (number_interior > 0) {
            std::vector<std::unique_ptr<geos::geom::LinearRing>> new_interiors {};
            for (std::size_t i = 0; i < number_interior; ++i) {
                auto copy_ring = factory->createLinearRing(std::move(std::move(poly->getInteriorRingN(i)->clone()->getCoordinates())));
                const auto inner_poly_ptr = factory->createPolygon(std::move(copy_ring));
                BOOST_LOG_TRIVIAL(debug) << "Hole area: " << inner_poly_ptr->getArea();
                if (inner_poly_ptr->getArea() >= min_area) {
                    new_interiors.push_back(factory->createLinearRing(std::move(poly->getInteriorRingN(i)->clone()->getCoordinates())));
                }
            }
            if (number_interior > new_interiors.size()) {
                BOOST_LOG_TRIVIAL(debug) << "Number of holes reduced: from " << number_interior << " to " << new_interiors.size();
                std::unique_ptr<geos::geom::LinearRing> external_ring = factory->createLinearRing(std::move(std::move(poly->getExteriorRing()->clone()->getCoordinates())));
                std::unique_ptr<geos::geom::Geometry> replacement_poly = factory->createPolygon(std::move(external_ring), std::move(new_interiors));
                return replacement_poly;
            }
        }
    }
    return std::nullopt;
}

void MergeOrCreateZoneCandidateForBuffer(std::map<osm_id_t, std::unique_ptr<BuildingZone>> &zone_candidates, std::unique_ptr<geos::geom::Geometry> &buffer_polygon,
                                         std::optional<std::shared_ptr<Building>>& building_outside) {
    bool touches_existing_zone_candidate = false;
    for (auto & it : zone_candidates) {
        if (it.second->ContainsOrIntersects(buffer_polygon.get())) {
            auto combined_geom = it.second->GetGeometry()->Union(buffer_polygon.get());
            if (combined_geom->isValid()) {
                it.second->SetGeometry(std::move(combined_geom));
                if (building_outside.has_value()) {
                    it.second->RelateBuilding(building_outside.value());
                }
                touches_existing_zone_candidate = true;
                break;
            }
        }
    }
    if (not touches_existing_zone_candidate) {
        BuildingZoneOrigin origin = building_outside.has_value() ? BuildingZoneOrigin::from_buildings : BuildingZoneOrigin::from_residential_streets;
        auto generated = std::make_unique<BuildingZone>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::landuse),
                                                                                 std::move(buffer_polygon),
                                                                                 BuildingZoneType::nonOSM,
                                                                                 origin);
        if (building_outside.has_value()) {
            generated->RelateBuilding(building_outside.value());
        }
        zone_candidates[generated->GetOSMId()] = std::move(generated);
    }
}

/*! \brief Find building zone candidates based on residential ways disjoint from mapped building zones in OSM.
 * Using disjoint to have a higher chance of finding residential ways which really should have plausible buildings.
 * Only using residential ways because in most situations they look like plausible candidates in OSM.
 * Service roads could also be considered, but are harder to handle due to different types of service roads -
 * e.g. they can serve within a residential area and not only in retail/industrial areas etc.
 */
std::map<osm_id_t, std::unique_ptr<BuildingZone>>
CreateBuildingZoneCandidatesFromDisjointResidentialWays(const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                                                        const std::map<osm_id_t, std::unique_ptr<Highway>> &highways) {
    std::map<osm_id_t, std::unique_ptr<BuildingZone>> zone_candidates {};
    std::optional<std::shared_ptr<Building>> optional = std::nullopt;
    for (auto &it : highways) {
        if (not it.second->IsResidentialWay()) {
            continue;
        }
        auto buffer = it.second->GetGeometry()->buffer(Parameters::Get().OWBB_GENERATE_LANDUSE_RESIDENTIAL_WAY_BUFFER_DISTANCE);
        bool disjoint = true;
        for (auto &zone : built_up_zones) {
            if (zone->ContainsOrIntersects(buffer.get())) {
                disjoint = false;
                break;
            }
        }
        if (disjoint) {
            MergeOrCreateZoneCandidateForBuffer(zone_candidates, buffer, optional);
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Candidate land-uses from disjoint residential ways found: " << zone_candidates.size();
    return zone_candidates;
}

/*! \brief Find building zone candidates from buildings, which are outside mapped zones in OSM.
 */
std::map<osm_id_t, std::unique_ptr<BuildingZone>>
CreateBuildingZoneCandidatesFromBuildings(const std::vector<std::shared_ptr<Building>> &buildings_outside) {
    std::map<osm_id_t, std::unique_ptr<BuildingZone>> zone_candidates {};
    for (auto building_outside : buildings_outside) {
        int buffer_distance {Parameters::Get().OWBB_GENERATE_LANDUSE_BUILDING_BUFFER_DISTANCE};
        if (building_outside->GetArea() > pow(Parameters::Get().OWBB_GENERATE_LANDUSE_BUILDING_BUFFER_DISTANCE, 2)) {
            const double factor = sqrt(building_outside->GetArea() / pow(Parameters::Get().OWBB_GENERATE_LANDUSE_BUILDING_BUFFER_DISTANCE, 2));
            buffer_distance = static_cast<int>(fmin(factor * Parameters::Get().OWBB_GENERATE_LANDUSE_BUILDING_BUFFER_DISTANCE,
                                                    Parameters::Get().OWBB_GENERATE_LANDUSE_BUILDING_BUFFER_DISTANCE_MAX));
        }
        std::unique_ptr<geos::geom::Geometry> buffer_polygon = building_outside->GetOuterPolygon()->buffer(buffer_distance);
        std::optional<std::shared_ptr<Building>> optional = building_outside;
        MergeOrCreateZoneCandidateForBuffer(zone_candidates, buffer_polygon, optional);
    }
    BOOST_LOG_TRIVIAL(info) << "Candidate land-uses from buildings found: " << zone_candidates.size();
    return zone_candidates;
}

void GenerateMissingBuildingZonesFromCandidates(std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                                                std::map<osm_id_t, std::unique_ptr<BuildingZone>> &zone_candidates) {
    //Search once again within zone candidates for intersections in order to account for randomness in checks
    std::set<osm_id_t> merged_candidate_ids {};
    if (zone_candidates.size() > 1) {
        std::vector<osm_id_t> keys{};
        keys.reserve(zone_candidates.size());
        for (auto &it : zone_candidates) {
            keys.push_back(it.first);
        }
        for (std::size_t i = 0; i < zone_candidates.size() - 1; ++i) {
            for (std::size_t j = i + 1; j < zone_candidates.size(); ++j) {
                if (zone_candidates[keys[i]]->ContainsOrIntersects(zone_candidates[keys[j]]->GetGeometry())) {
                    auto combined_geom = zone_candidates[keys[j]]->GetGeometry()->Union(zone_candidates[keys[i]]->GetGeometry());
                    if (combined_geom->isValid()) {
                        merged_candidate_ids.insert(keys[i]);
                        zone_candidates[keys[j]]->SetGeometry(std::move(combined_geom));
                        for (auto osm_building : zone_candidates[keys[i]]->GetOSMBuildings()) {
                            zone_candidates[keys[j]]->RelateBuilding(osm_building);
                        }
                        break;
                    }
                }
            }
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Candidate land-uses merged into other candidates: " << merged_candidate_ids.size();
    std::vector<std::unique_ptr<BuildingZone>> kept_candidates {};
    for (auto & it : zone_candidates) {
        if (merged_candidate_ids.contains(it.first)) {
            continue;  // do not keep merged candidates
        }
        auto simplified_geom = geos::simplify::TopologyPreservingSimplifier::simplify(it.second->GetGeometry(),
                                                                                                Parameters::Get().OWBB_GENERATE_LANDUSE_SIMPLIFICATION_TOLERANCE);
        if (simplified_geom->isValid()) {
            it.second->SetGeometry(std::move(simplified_geom));
        }
        // remove interior holes, which are too small
        std::optional<std::unique_ptr<geos::geom::Geometry>> replacer = ReduceSmallHolesInPolygon(it.second->GetGeometry(),
                                                                                                  Parameters::Get().OWBB_GENERATE_LANDUSE_HOLES_MIN_AREA);
        if (replacer.has_value() and replacer.value()->isValid()) {
            it.second->SetGeometry(std::move(replacer.value()));
        }
        kept_candidates.push_back(std::move(it.second));
    }
    BOOST_LOG_TRIVIAL(info) << "Candidate land-uses with sufficient area found: " << kept_candidates.size();

    // Make sure that new generated buildings zones do not intersect with other building zones.
    // due to buffering around buildings, this is possible
    std::vector<std::unique_ptr<BuildingZone>> final_candidates {};
    for (auto & candidate : kept_candidates) {
        bool is_final {true};
        for (auto &&zone : built_up_zones) {
            if (zone->GetGeometry()->contains(candidate->GetGeometry())) {
                is_final = false;
                for (auto osm_building : candidate->GetOSMBuildings()) {
                    zone->RelateBuilding(osm_building);
                }
                break;
            }
            if (zone->GetGeometry()->intersects(candidate->GetGeometry())) {
                std::unique_ptr<geos::geom::Geometry> replacement_poly =
                    zone->NonIntersectingDifference(candidate->GetGeometry());
                if (replacement_poly->isValid()) {
                    candidate->SetGeometry(std::move(replacement_poly));
                }
            }
        }
        if (is_final) {
            final_candidates.push_back(std::move(candidate));
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Candidate land-uses after overlap check: " << final_candidates.size();

    // now make sure that there are no MultiPolygons
    const auto before = built_up_zones.size();
    for (auto & candidate : final_candidates) {
        if (candidate->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTIPOLYGON) {
            std::vector<std::unique_ptr<BuildingZone>> split_zones = SplitMultiPolygonBuildingZone(std::move(candidate));
            for (auto & split : split_zones) {
                built_up_zones.push_back(std::move(split));
            }
        } else {
            built_up_zones.push_back(std::move(candidate));
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Total added generated buildings zones: " << built_up_zones.size() - before;
}

/*!
 * Reads both nodes and areas from OSM, but then transforms areas to nodes.
 * We trust that there is no double tagging of a place as both node and area.
 * Only urban places (city, town) and farms are looked at. The rest in between (e.g. villages)
 * are not taken as places.
 */
static std::pair<std::vector<std::unique_ptr<Place>>, std::vector<std::unique_ptr<Place>>> ProcessPlaces(OSMDataReaderOverpass &osm_reader) {
    std::vector<std::unique_ptr<Place>> urban_places {};
    std::vector<std::unique_ptr<Place>> farm_places {};

    std::vector<std::pair<std::string, std::string>> place_key_values {{k_place, v_city}, {k_place, v_farm}, {k_place, v_town}};

    osm_reader.SetUseExtendedBoundary(true);
    //points
    auto result = osm_reader.FetchNodesIsolatedFromKeyValues(place_key_values);
    for (auto & it : result.nodes_dict) {
        try {
            std::unique_ptr<geos::geom::Point> geom = it.second->PointFromOSMNode();
            auto place = std::make_unique<Place>(it.second->GetOSMId(), std::move(geom), it.second->GetTags(), true);
            if (not place->IsDefinedType()) {
                continue;
            }
            if (place->GetPlaceType() == PlaceType::farm) {
                farm_places.push_back(std::move(place));
            } else {
                urban_places.push_back(std::move(place));
            }
        } catch (const InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
        }
    }

    //areas
    result = osm_reader.FetchWaysFromKeyValues(place_key_values);
    for (auto & it : result.ways_dict) {
        try {
            std::unique_ptr<geos::geom::Polygon> geom = it.second->PolygonFromOSMWay(result.nodes_dict);
            auto place = std::make_unique<Place>(it.second->GetOSMId(), std::move(geom), it.second->GetTags(), false);
            if (not place->IsDefinedType()) {
                continue;
            }
            if (place->GetPlaceType() == PlaceType::farm) {
                farm_places.push_back(std::move(place));
            } else {
                place->TransformToPoint();
                urban_places.push_back(std::move(place));
            }
        } catch (const InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
        }
    }

    // relations
    auto rel_result = osm_reader.FetchRelationsPlaces();
    for (auto & it : rel_result.relations_dict) {
        auto polygons = it.second->PolygonsFromMembers(rel_result, MemberRole::outer);
        std::unique_ptr<geos::geom::Polygon>* largest_area_poly = nullptr;
        for (auto &polygon : polygons) {
            if (largest_area_poly) {
                if (largest_area_poly->get()->getArea() < polygon->getArea()) {
                    largest_area_poly = &polygon;
                }
            } else {
                largest_area_poly = &polygon;
            }
        }
        if (largest_area_poly) {
            std::unique_ptr<geos::geom::Point> geom = largest_area_poly->get()->getCentroid();
            auto place = std::make_unique<Place>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::place),
                                                                   std::move(geom), it.second->GetTags(), true);
            if (place->IsDefinedType()) {
                if (place->GetPlaceType() == PlaceType::farm) {
                    farm_places.push_back(std::move(place));
                } else {
                    urban_places.push_back(std::move(place));
                }
            }
        }
    }

    osm_reader.SetUseExtendedBoundary(false);
    BOOST_LOG_TRIVIAL(info) << "Number of valid places found: urban=" << urban_places.size() << ", farm=" << farm_places.size();
    return {std::move(urban_places), std::move(farm_places)};
}

template<typename T>
static std::map<osm_id_t, std::unique_ptr<T>> ProcessLinearStrings(OSMDataReaderOverpass &osm_reader,
                                                                   std::vector<std::string> &required_keys, std::string feature_name) {
    OSMReadResult result = osm_reader.FetchWaysFromKeys(required_keys);
    std::map<osm_id_t, std::unique_ptr<T>> linear_features{};
    for (auto & it : result.ways_dict) {
        try {
            std::unique_ptr<geos::geom::LineString> line = Way::LineStringFromRefs(it.second->GetOSMId(),
                                                                                   it.second->GetRefs(),
                                                                                   result.nodes_dict);
            std::unique_ptr<T> linear_feature = std::make_unique<T>(it.second->GetOSMId(), std::move(line), it.second->GetTags());
            if (linear_feature->IsDefinedType()) {
                linear_features[linear_feature->GetOSMId()] = std::move(linear_feature);
            }
        } catch (InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
        }
    }
    BOOST_LOG_TRIVIAL(info) << feature_name << " found: " << linear_features.size();

    return linear_features;
}

std::vector<std::unique_ptr<geos::geom::Polygon>> CreateBufferAroundMajorTransportLines(std::map<osm_id_t, std::unique_ptr<Highway>> &highways,
                                                                                         std::map<osm_id_t, std::unique_ptr<RailwayLine>> &railways,
                                                                                         std::map<osm_id_t, std::unique_ptr<Waterway>> &waterways) {
    //create buffers around major transport
    std::vector<std::unique_ptr<geos::geom::Geometry>> line_buffers {};
    for (auto & it : highways) {
        if (it.second->UseForBuildingZoneSplit()) {
            line_buffers.push_back(it.second->GetGeometry()->buffer(it.second->GetWidth() / 2.));
        }
    }
    for (auto & it : railways) {
        if (it.second->UseForBuildingZoneSplit()) {
            line_buffers.push_back(it.second->GetGeometry()->buffer(it.second->GetWidth() / 2.));
        }
    }
    for (auto & it : waterways) {
        if (it.second->UseForBuildingZoneSplit()) {
            line_buffers.push_back(it.second->GetGeometry()->buffer(it.second->GetWidth() / 2.));
        }
    }
    return MergeGeomsToPolys(line_buffers);
}

std::vector<std::unique_ptr<geos::geom::Geometry>> ProcessLandUseForLighting(const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones) {
    std::vector<std::unique_ptr<geos::geom::Geometry>> buffered_polygons {};
    buffered_polygons.reserve(built_up_zones.size());
    for (auto & zone: built_up_zones) {
        if (zone->IsAerodrome() or zone->IsFarmyard() or zone->IsMilitary()) {
            continue;
        }
        buffered_polygons.push_back(zone->GetGeometry()->buffer(Parameters::Get().OWBB_BUILT_UP_BUFFER));
    }
    std::vector<std::unique_ptr<geos::geom::Polygon>> merged_polygons = MergeGeomsToPolys(buffered_polygons);

    std::vector<std::unique_ptr<geos::geom::Geometry>> lit_areas {};
    // check for minimal area of inner holes and create final list of polygons
    for (auto & polygon : merged_polygons) {
        std::optional<std::unique_ptr<geos::geom::Geometry>> replacer = ReduceSmallHolesInPolygon(polygon.get(),
                                                                                                  Parameters::Get().OWBB_BUILT_UP_BUFFER);
        if (replacer.has_value()) {
            lit_areas.push_back(std::move(replacer.value()));
        } else {
            lit_areas.push_back(std::move(polygon));
        }
    }
    return lit_areas;
}


/*!
Splits generated building zones into several sub-zones along major transport lines and waterways.
Major transport lines are motorways, trunks as well as certain railway lines.
Using buffers (= polygons) instead of lines because Shapely cannot do it natively.
Using buffers directly instead of trying to entangle line strings of highways/railways due to
easiness plus performance wise most probably same or better.

We are only interested in the generated building zones, not the original one
because splitting is done to guess the land-use type. If the person mapping a zone in OSM
has decided that it goes across major lines, then we accept that.
 */
void SplitGeneratedBuildingZonesByMajorLines(std::map<osm_id_t, std::unique_ptr<Highway>> &highways,
                                             std::map<osm_id_t, std::unique_ptr<RailwayLine>> &railways,
                                             std::map<osm_id_t, std::unique_ptr<Waterway>> &waterways,
                                             std::vector<std::unique_ptr<BuildingZone>> &built_up_zones) {
    const std::vector<std::unique_ptr<geos::geom::Polygon>> merged_buffers = CreateBufferAroundMajorTransportLines(highways, railways, waterways);
    if (merged_buffers.empty()) {
        return;
    }

    // walk through all buffers and where intersecting get the difference with zone geometry as new zone polygon(s).
    std::vector<std::unique_ptr<BuildingZone>> after_list {};
    for (auto & buffer : merged_buffers) {
        const auto prep_buffer = std::make_unique<geos::geom::prep::PreparedPolygon>(buffer.get());
        while (not built_up_zones.empty()) {
            auto zone = std::move(built_up_zones.back());
            built_up_zones.pop_back();
            if (zone->IsGenerated() and prep_buffer->intersects(zone->GetGeometry())) {
                auto diff = zone->GetGeometry()->difference(buffer.get());
                // only if the diff leads to several polygons, then we split the zone
                if (diff->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTIPOLYGON) {
                    zone->SetGeometry(std::move(diff));
                    auto split_zones = SplitMultiPolygonBuildingZone(std::move(zone));
                    // it could be that the returned list is empty because all parts are below size criteria for
                    // generated zones, which is ok
                    for (auto & split : split_zones) {
                        after_list.push_back(std::move(split));
                    }
                } else {
                    after_list.push_back(std::move(zone));
                }
            } else {
                after_list.push_back(std::move(zone));
            }
        }
        built_up_zones.swap(after_list);
    }
}

std::vector<std::unique_ptr<geos::geom::Polygon>> ProcessLandUseForSettlementAreas(const std::vector<std::unique_ptr<geos::geom::Geometry>> &lit_areas,
                                                                                    const std::vector<std::unique_ptr<geos::geom::Geometry>> &osm_water_areas) {
    std::vector<std::unique_ptr<geos::geom::Geometry>> all_areas {};
    all_areas.reserve(lit_areas.size() + osm_water_areas.size());
    for (auto & geom : lit_areas) {
        all_areas.push_back(geom->clone());
    }
    for (auto & geom : osm_water_areas) {
        all_areas.push_back(geom->clone());
    }
    return MergeGeomsToPolys(all_areas);
}

/*! \brief Settlement clusters based on lit areas and specific water areas.

The reason for using specific water areas is that in cities with much water (e.g. Stockholm) otherwise
the parts of the city would get too isolated and less density - often there is only one clear city center
from OSM data, but the city stretches longer.
 */
std::vector<std::unique_ptr<GridIndexedGeometry>> CreateClustersOfSettlements(const std::vector<std::unique_ptr<geos::geom::Geometry>> &lit_areas,
                                                                              const std::vector<std::unique_ptr<geos::geom::Geometry>> &osm_water_areas,
                                                                              const std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> &dense_circles,
                                                                              const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
    std::vector<std::unique_ptr<GridIndexedGeometry>> clusters {};
    std::vector<std::unique_ptr<geos::geom::Polygon>> candidate_settlements = ProcessLandUseForSettlementAreas(lit_areas, osm_water_areas);
    for (auto & candidate : candidate_settlements) {
        for (auto & dense_circle : dense_circles) {
            if (not dense_circle->IsDisjoint(candidate.get())) {
                auto gig = std::make_unique<GridIndexedGeometry>(candidate->clone());
                gig->AssignGridIndices(grid_index_boxes);
                clusters.push_back(std::move(gig));
            }
        }
    }
    return clusters;
}

void AssignMinimumSettlementTypeToZones(const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                                        const std::vector<std::unique_ptr<GridIndexedGeometry>> &settlement_clusters) {
    for (auto & building_zone : built_up_zones) {
        if (building_zone->IsAerodrome()) {
            continue;
        }
        for (auto & settlement_cluster : settlement_clusters) {
            if (GridIndexed::HaveCommonIndices(building_zone->GetGridIndices(), settlement_cluster->GetGridIndices())) {
                if (settlement_cluster->ContainsOther(building_zone->GetGeometry())) {
                    building_zone->SetMaxSettlementType(SettlementType::periphery);
                    break;
                }
            }
        }
    }
}

/*! \brief Returns highways that are within a building_zone or intersecting with a building_zone.

Highways_dict gets reduced by those highways, which were within, such that searching in other
areas gets quicker due to reduced volume.
 */
std::vector<geos::geom::Geometry *> TestHighwayIntersectionArea(const std::unique_ptr<BuildingZone> &building_zone,
                                                                const std::vector<std::unique_ptr<GridIndexedGeometry>> &grid_highways) {
    std::vector<geos::geom::Geometry *> intersecting_highways {};
    const auto prep_area = geos::geom::prep::PreparedPolygon(building_zone->GetGeometry());
    for (auto & grid_highway : grid_highways) {
        if (GridIndexed::HaveCommonIndices(building_zone->GetGridIndices(), grid_highway->GetGridIndices())) {
            if (prep_area.containsProperly(grid_highway->GetGeometry()) or prep_area.intersects(grid_highway->GetGeometry())) {
                intersecting_highways.push_back(grid_highway->GetGeometry());
            }
        }
    }
    return intersecting_highways;
}

/*!
Splits the land-use into (city) blocks, i.e. areas surrounded by streets.
Brute force by buffering all highways, then take the geometry difference, which splits the zone into
multiple polygons. Some polygons will be real city blocks, others will be border areas.

Could also be done by using something like Python networkx.algorithms.cycles.cycle_basis.html.
However, it is a bit more complicated logic and programming wise, but might be faster.
 */
int AssignCityBlocks(std::unique_ptr<BuildingZone> &building_zone,
                      const std::vector<std::unique_ptr<GridIndexedGeometry>> &grid_highways) {
    int assigned_city_blocks {};
    building_zone->ResetCityBlocks();
    std::vector<std::unique_ptr<geos::geom::Geometry>> polygons {};
    auto intersecting_highways = TestHighwayIntersectionArea(building_zone, grid_highways);
    if (not intersecting_highways.empty()) {
        std::vector<std::unique_ptr<geos::geom::Geometry>> buffers {};
        buffers.reserve(intersecting_highways.size());
        for (const auto * geometry: intersecting_highways) {
            buffers.push_back(geometry->buffer(Parameters::Get().OWBB_CITY_BLOCK_HIGHWAY_BUFFER, 1, geos::operation::buffer::BufferOp::CAP_SQUARE));
        }
        const auto factory = TileHandler::Get()->GetGeometryFactory();
        const std::unique_ptr<geos::geom::MultiPolygon> collection = factory->createMultiPolygon(std::move(buffers));
        const std::unique_ptr<geos::geom::Geometry> unified = collection->Union();
        auto parts = PolygonsFromGeometryDifference(building_zone->GetGeometry(), unified.get(),
                                                    Parameters::Get().OWBB_MIN_CITY_BLOCK_AREA);
        if (parts.has_value()) {
            for (auto &part: parts.value()) {
                polygons.push_back(std::move(part));
            }
        }
        BOOST_LOG_TRIVIAL(debug) << "Found " << polygons.size() << " city blocks in building zone osm_ID=" << building_zone->GetOSMId();

        for (auto & polygon : polygons) {
            auto block = std::make_unique<CityBlock>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::landuse),
                                                     std::move(polygon), building_zone->GetBuildingZoneType());
            block->SetSettlementType(building_zone->GetSettlementType());
            building_zone->AddCityBlock(std::move(block));
            ++assigned_city_blocks;
        }
    } else { //we need to make a virtual one by using the geometry of the building zone (e.g. when a building zone is only along a highway
        auto block = std::make_unique<CityBlock>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::landuse),
                                                 std::move(building_zone->GetGeometry()->clone()), building_zone->GetBuildingZoneType());
        block->SetSettlementType(building_zone->GetSettlementType());
        building_zone->AddCityBlock(std::move(block));
        ++assigned_city_blocks;
    }

    //now assign the osm_buildings to the city blocks
    building_zone->ReassignOSMBuildingsToCityBlocks();
    return assigned_city_blocks;
}

void AssignCityBlocksToZones(std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                             const std::vector<std::unique_ptr<GridIndexedGeometry>> &grid_highways) {
    int total_assigned_city_blocks {};
    for (auto & zone : built_up_zones) {
        if (not zone->IsAerodrome()) {
            total_assigned_city_blocks += AssignCityBlocks(zone, grid_highways);
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Total number of assigned city blocks: " << total_assigned_city_blocks;
}

void AssignUrbanSettlementType(std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                               const SettlementType current_settlement_type,
                               std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> &urban_settlements) {
    for (auto & urban_settlement: urban_settlements) {
        for (const auto &building_zone: built_up_zones) {
            if (building_zone->IsAerodrome() or building_zone->GetSettlementType() == SettlementType::rural) {
                continue;
            }
            if (GridIndexed::HaveCommonIndices(urban_settlement->GetGridIndices(), building_zone->GetGridIndices())) {
                if (urban_settlement->ContainsOther(building_zone->GetGeometry()) or
                    urban_settlement->Intersects(building_zone->GetGeometry())) {
                    building_zone->SetMaxSettlementType(current_settlement_type);
                    building_zone->SetSettlementTypeCityBlocks(current_settlement_type, urban_settlement);
                }
            }
        }
    }
}

void CountZonesRelatedBuildings(std::vector<std::shared_ptr<Building>> &buildings, std::string text, const bool check_block = false) {
    long total_related = 0;
    long total_not_block = 0;
    const auto total_buildings_before = buildings.size();

    for (auto itr = buildings.cbegin(); itr != buildings.cend();) {
        if (itr->get()->HasZone()) {
            ++total_related;
            const auto my_zone = itr->get()->GetZone();
            if (check_block and my_zone->GetZoneType() != ZoneType::city_block) {
                ++total_not_block;
                BOOST_LOG_TRIVIAL(debug) << "Building with osm_id=" << itr->get()->GetOSMId() << " not in a city block";
            }
            if (my_zone->GetGeometry()->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTIPOLYGON) {
                LogDebugWarning("Building with osm_id=" + std::to_string(itr->get()->GetOSMId()) + "has Multipolygon - " + text);
                //FIXME: is this a good idea in Python at all? building.zone.geometry = building.zone.geometry.geoms[0]  # just use the first polygon
                //instead it should be an assertion earlier, that all zone are non-Multipolygon
            }
            ++itr;
        } else {
            BOOST_LOG_TRIVIAL(debug) << "Building with osm_id=" << itr->get()->GetOSMId() << " removed because it has not associated zone - " << text;
            itr = buildings.erase(itr);
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Total number of buildings reduced with " << total_buildings_before - buildings.size() << " because not in a zone - " << text;
    BOOST_LOG_TRIVIAL(info) << total_related << " out of " << buildings.size() << " buildings are related to zone " << text << " - " << total_not_block << " not in city block";
}

/*! \brief Relate neighbours and checks whether some buildings should have the same parent based on shared references.
 *
 * This is due to the fact that relations in OSM are not always done clearly - and building:parts are often
 * only related by geometry and not distinct modelling in OSM.
 *
 * Therefore, the following assumptions are made for buildings which are not building_parts:
 *   * If a building shares 4 or more nodes with another building, then it is related and they get the same parent
 *   * If a buildings shares 3 consecutive nodes and for each segment the length is more than 2 metres, then
 *  they are related and get the same parent. In order not to depend on correct sequence etc. it is just assumed
 *  that the distance between each of the 3 points must be more than 2 metres. It is very improbable that this
 *  would not be the case for the distance between the 2 points not directly connected. We take the risk.
 *
 *  When a building is a building_part:
 *    * If it shares 2 or more nodes with another building, then it is related and tye get the same parent
 *
 *  Most buildings might share only 2 nodes - e.g. row-houses or buildings in a city.
 */
void RelateBuildingsBasedOnSharedReferences(const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                                            const node_map_t &building_nodes) {
    long new_relations{0};
    long counter {0};
    for (auto &building_zone : built_up_zones) {
        if (++counter % 100 == 0) {
            BOOST_LOG_TRIVIAL(info) << "Checked building relations in " << counter << " out of " << built_up_zones.size() << " built up zones.";
        }
        auto city_blocks = building_zone->GetCityBlocks();
        for (const auto &city_block : *city_blocks) {
            auto buildings = city_block->GetOSMBuildings();
            std::shared_ptr<Building> first_building;
            std::shared_ptr<Building> second_building;
            for (std::size_t i = 0; i < buildings.size(); i++) {
                first_building = buildings[i];
                for (std::size_t j = i + 1; j < buildings.size(); j++) {
                    second_building = buildings[j];
                    std::vector<osm_id_t> common_refs{};
                    if (first_building->GetOSMId() ==
                        second_building->GetOSMId()) {
                        continue;  // do not compare with self
                    }
                    for (auto &first_ref : *first_building->GetOuterRefs()) {
                        for (auto &second_ref :
                             *second_building->GetOuterRefs()) {
                            if (first_ref == second_ref) {
                                common_refs.push_back(first_ref);
                                first_building->AddSharedRef(second_building, first_ref);
                                second_building->AddSharedRef(first_building, second_ref);
                            }
                        }
                    }
                    // now check for requirements for BuildingParents
                    if (first_building->HasParent() and not first_building->GetParent()->ContainsChild(second_building)) {
                        if (common_refs.size() < 2) {
                            continue;
                        } else if (common_refs.size() == 2) {
                            if (not first_building->IsBuildingPart() and not second_building->IsBuildingPart()) {
                                continue;
                            }
                        } else if (common_refs.size() == 3) {
                            auto sg_0 = building_nodes.at(common_refs.at(0))->GetSGGeod();
                            auto sg_1 = building_nodes.at(common_refs.at(1))->GetSGGeod();
                            auto sg_2 = building_nodes.at(common_refs.at(2))->GetSGGeod();
                            if (SGGeodesy::distanceM(sg_0, sg_1) < 2. or
                                SGGeodesy::distanceM(sg_1, sg_2) < 2. or
                                SGGeodesy::distanceM(sg_0, sg_2) < 2.) {
                                continue;
                            }
                        }
                        // now we know that we have at least 3 points with at least 2m distance
                        new_relations++;
                        if (first_building->HasParent() and second_building->HasParent()) {
                            // need to decide on one parent (we pick first) and transfer all children from other parent
                            BuildingParent::MoveChildrenToOtherParent(second_building->GetParent(),first_building->GetParent());
                        } else if (first_building->HasParent()) {
                            first_building->GetParent()->AddChild(second_building);
                        } else if (second_building->HasParent()) {
                            second_building->GetParent()->AddChild(first_building);
                        } else {
                            auto building_parent = std::make_shared<BuildingParent>(IdCreator::Get().GetNextPseudoOSMId(
                                OSMFeatureType::building_relation), false);
                            Building::SetParentAndSetAsChild(first_building,
                                                             building_parent);
                            Building::SetParentAndSetAsChild(second_building,
                                                             building_parent);
                        }
                    }
                }
            }
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Created " << new_relations << " new parent building relations based on shared references";
}

static std::vector<std::shared_ptr<Building>> RemoveVeryCloseNodes(const node_map_t &building_nodes,
                                                                   std::vector<std::shared_ptr<Building>> &buildings_before) {
    std::vector<std::shared_ptr<Building>> buildings_after {};
    const auto size_before = buildings_before.size();
    long counter {0};
    long total_number_removed {0};
    for(auto &building : buildings_before) {
        if (++counter % 10000 == 0) {
            BOOST_LOG_TRIVIAL(info) << "Checked distance nodes for " << counter << " out of " << buildings_before.size() << " buildings_before.";
        }
        auto[keep_building, nodes_removed] = building->CheckDistanceNodes(building_nodes);
        total_number_removed += nodes_removed;
        auto reason = " too few nodes";
        try {
            building->RefreshOuterPolygon(building_nodes);
        } catch (const InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
            keep_building = false;
            reason = " geometry problem";
        }
        if (keep_building) {
            buildings_after.push_back(building);
        } else {
            BOOST_LOG_TRIVIAL(info) << "Removing building " << building->GetOSMId() << reason;
            Building::PrepareToBeReleased(building);
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Removed " << total_number_removed << " nodes from " << size_before << " buildings_before";
    buildings_before.clear();
    buildings_before.shrink_to_fit();
    BOOST_LOG_TRIVIAL(info) << "Removed " << size_before - buildings_after.size() << " buildings due to node problems";
    return buildings_after;
}

/*! \brief Remove all those buildings, which are not going to be rendered.
 *
 * It is ok to do so when we have the zones and guessed their type.
 */
static std::vector<std::shared_ptr<Building>> RemoveNotRenderedBuildings(std::vector<std::shared_ptr<Building>> &buildings_before) {
    std::vector<std::shared_ptr<Building>> buildings_after {};
    const auto size_before = buildings_before.size();
    const auto generator = TileHandler::Get()->GetRandomNumberGenerator();
    std::uniform_real_distribution<double> uniform_dist(0.0,1.0);
    for (auto &building : buildings_before) {
        if (building->IsBuildingRendered(uniform_dist(*generator))) {
            buildings_after.push_back(building);
        } else {
            Building::PrepareToBeReleased(building);
        }
    }
    buildings_before.clear();
    buildings_before.shrink_to_fit();
    BOOST_LOG_TRIVIAL(info) << "Removed " << size_before - buildings_after.size() << " not to be rendered buildings";
    BOOST_LOG_TRIVIAL(info) << "Before there were " << size_before << " buildings, now there are " << buildings_after.size();
    return buildings_after;
}

/*! \brief Remove all those buildings, which are outside the tile boundary.
 *
 * If a building is has no parent and its first node is outside the tile, then it is removed.
 * If a building has a parent - and the parent's midpoint is outside the tile, then it is to be removed.
 */
static std::vector<std::shared_ptr<Building>> RemoveBuildingsOutsideTile(const node_map_t &building_nodes,
                                                                         std::vector<std::shared_ptr<Building>> &buildings_before) {
    std::vector<std::shared_ptr<Building>> buildings_after{};
    const auto size_before = buildings_before.size();

    // first we need to find all building parents
    std::map<osm_id_t, std::shared_ptr<BuildingParent>> parents {};
    for (auto &building : buildings_before) {
        if (building->HasParent()) {
            auto parent = building->GetParent();
            if (not parents.contains(parent->GetOSMId())) {
                parents[parent->GetOSMId()] = parent;
            }
        }
    }

    // then for each building parent we need to calculate the midpoint
    for (auto &it : parents) {
        LonLat average = it.second->CalculateMidPointOfAllChildren(building_nodes);
        if (not TileHandler::Get()->GetTileBoundary().IsInsideBoundary(average)) {
            it.second->SetOutsideTile();
        }
    }

    // now we can exclude the buildings
    for (auto &building : buildings_before) {
        if (building->HasParent()) {
            if (building->GetParent()->IsInsideTile()) {
                buildings_after.push_back(building);
            } else {
                // the building's parent is outside the tile. Therefore,
                // we do not add it to buildings_after and make sure that the
                // building's zone does not link to it anymore
                Building::PrepareToBeReleased(building);
            }
        } else {
            auto lon_lat = building_nodes.at(building->GetOuterRefs()->at(0))->GetLonLat();
            if (TileHandler::Get()->GetTileBoundary().IsInsideBoundary(lon_lat)) {
                buildings_after.push_back(building);
            } else {
                Building::PrepareToBeReleased(building);
            }
        }
    }
    buildings_before.clear();
    buildings_before.shrink_to_fit();
    BOOST_LOG_TRIVIAL(info) << "Removed " << size_before - buildings_after.size() << " buildings outside tile boundary";
    BOOST_LOG_TRIVIAL(info) << "Before there were " << size_before << " buildings, now there are " << buildings_after.size();
    return buildings_after;
}

/*! \brief simplify the geometry - useful to get simpler roof geometry and fewer facades.
 *
 */
static std::vector<std::shared_ptr<Building>> SimplifyBuildingGeometries(const node_map_t &building_nodes,
                                                                         std::vector<std::shared_ptr<Building>> &buildings_before) {
    std::vector<std::shared_ptr<Building>> buildings_after{};
    const auto size_before = buildings_before.size();
    long count {0};
    for (auto &building : buildings_before) {
        bool keep_building = true;
        if (not building->HasParent()) { // do not simplify if in parent/child relationship
            count += building->SimplifyOuterGeometry(building_nodes);
        }
        try {
            building->RefreshOuterPolygon(building_nodes);
        } catch (const InvalidGeometryFromOSM& e) {
            LogDebugWarning(e.what());
            keep_building = false;
        }
        if (keep_building) {
            buildings_after.push_back(building);
        } else {
            BOOST_LOG_TRIVIAL(info) << "Removing building " << building->GetOSMId() << " due to geometry problem";
            Building::PrepareToBeReleased(building);
        }
    }

    buildings_before.clear();
    buildings_before.shrink_to_fit();
    BOOST_LOG_TRIVIAL(info) << "Made " << count << " simplifications in total (there can be more than 1 simplification in a building).";
    BOOST_LOG_TRIVIAL(info) << "Removed " << size_before - buildings_after.size() << " buildings due to geometry problems";
    BOOST_LOG_TRIVIAL(info) << "Before there were " << size_before << " buildings, now there are " << buildings_after.size();
    return buildings_after;
}

void ProcessLandUse(OSMDataReaderOverpass &osm_reader, const bool &create_trees) {
    TimeLogger time_logger {};

    auto grid_index_boxes = CreateGridIndexBoxes();

    std::vector<std::unique_ptr<BuildingZone>> aerodrome_zones = ProcessAerodromes(osm_reader);
    std::vector<std::unique_ptr<BuildingZone>> built_up_zones = ProcessBuiltUpLandUse(osm_reader);
    auto[urban_places, farm_places] = ProcessPlaces(osm_reader);
    auto [buildings, building_nodes_dict] = ProcessBuildings(osm_reader, time_logger);
    std::vector<std::string> required_keys {k_highway};
    std::map<osm_id_t, std::unique_ptr<Highway>> highways = ProcessLinearStrings<Highway>(osm_reader, required_keys, "Highways");
    required_keys = {k_railway};
    std::map<osm_id_t, std::unique_ptr<RailwayLine>> railways = ProcessLinearStrings<RailwayLine>(osm_reader, required_keys, "Railways");
    required_keys = {k_waterway};
    std::map<osm_id_t, std::unique_ptr<Waterway>> waterways = ProcessLinearStrings<Waterway>(osm_reader, required_keys, "Waterways");
    std::vector<std::unique_ptr<geos::geom::Geometry>> osm_water_areas = ProcessWaterAreas(osm_reader);
    const auto natural_waters = ProcessNaturalWater(osm_reader, grid_index_boxes);

    time_logger.Log("Time used in seconds for parsing OSM data");

    // =========== PROCESS AERODROME INFORMATION ============================
    std::map<std::string, std::unique_ptr<Airport>> airports = ReadAptDatFile();
    int apt_dat_aerodromes {0};
    for (auto &airport : airports) {
        for (std::vector<std::unique_ptr<geos::geom::Polygon>> polys = airport.second->CreateBoundaryPolygons(); auto &poly : polys) {
            auto bz = std::make_unique<BuildingZone>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::landuse),
                                                                              std::move(poly),
                                                                              BuildingZoneType::aerodrome,
                                                                              BuildingZoneOrigin::apt_dat);
            aerodrome_zones.push_back(std::move(bz));
            apt_dat_aerodromes++;
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Added " << apt_dat_aerodromes << " aerodrome zones from apt_dat";
    for (auto & aerodrome_zone : aerodrome_zones) {
        built_up_zones.push_back(std::move(aerodrome_zone));
    }
    time_logger.Log("Time used in seconds for processing aerodromes incl. apt_dat");

    // =========== Make sure we do not have overlapping building zones ======================
    RemoveOverlappingBuildingZoneParts(built_up_zones);
    BOOST_LOG_TRIVIAL(info) << built_up_zones.size() << " building zones after overlap check";

    // =========== READ LAND-USE DATA FROM EXTERNAL SOURCE ==================================
    // TODO: https://gitlab.com/osm2city/osm2gear/-/issues/17 (built-up areas and water)

    // =========== GENERATE ADDITIONAL LAND-USE ZONES FOR AND/OR FROM BUILDINGS =============
    auto buildings_outside = RelateBuildingsToBuildingZones(buildings, built_up_zones);
    time_logger.Log("Time used in seconds for assigning buildings to OSM zones");
    auto zone_candidates_from_buildings = CreateBuildingZoneCandidatesFromBuildings(buildings_outside);
    GenerateMissingBuildingZonesFromCandidates(built_up_zones, zone_candidates_from_buildings);
    time_logger.Log("Time used in seconds for generating building zones from buildings");

    CountZonesRelatedBuildings(buildings, "after generating zones from buildings");

    // =========== GENERATE ADDITIONAL LAND-USE ZONES FROM LONELY RESIDENTIAL ROADS =============
    // this needs to be done after buildings outside for logical reasons
    auto zone_candidates_from_disjoint_ways = CreateBuildingZoneCandidatesFromDisjointResidentialWays(built_up_zones, highways);
    GenerateMissingBuildingZonesFromCandidates(built_up_zones, zone_candidates_from_disjoint_ways);
    time_logger.Log("Time used in seconds for generating building zones from disjoint residential streets");

    CountZonesRelatedBuildings(buildings, "after generating zones from disjoint residential streets");

    // =========== CREATE POLYGONS FOR LIGHTING OF STREETS ================================
    // Needs to be before finding city blocks as we need the boundary
    std::vector<std::unique_ptr<geos::geom::Geometry>> lit_areas = ProcessLandUseForLighting(built_up_zones);
    BOOST_LOG_TRIVIAL(info) << "Number of lit areas found: " << lit_areas.size();
    time_logger.Log("Time used in seconds for finding lit areas");

    CountZonesRelatedBuildings(buildings, "after lighting");

    // =========== MAKE SURE GENERATED LAND-USE DOES NOT CROSS MAJOR LINEAR OBJECTS =======
    // often not done because it takes a lot of time and the effect might be minor
    // should be done by proper manual mapping in OSM instead
    if (Parameters::Get().OWBB_SPLIT_AT_MAJOR_LINES) {
        SplitGeneratedBuildingZonesByMajorLines(highways, railways, waterways,
                                                built_up_zones);
        BOOST_LOG_TRIVIAL(info) << "Building zones after split major lines: "
                                << built_up_zones.size();
        time_logger.Log(
            "Time used in seconds for splitting building zones by major lines");

        CountZonesRelatedBuildings(buildings, "after split major lines");
    }
    // assign grid index now that we have all built_up_zones
    for (auto &[fst, snd] : grid_index_boxes) {
        for (auto &building_zone : built_up_zones) {
            if (not building_zone->GetGeometry()->disjoint(snd.get())) {
                building_zone->AddGridIndex(fst);
            }
        }
    }
    // if a zone is totally outside the tile, we do not want to discard it, but it needs to have a grid index nonetheless
    for (auto &building_zone : built_up_zones) {
        if (not building_zone->HasGridIndices()) {
            building_zone->AddGridIndexOutside();
        }
    }

    // =========== Link urban places with settlement_area buffers ==================================
    std::vector<std::unique_ptr<GridIndexedGeometry>> grid_highways {};
    for (auto & it : highways) {
        if (it.second->PopulateBuildingsAlong()) {
            auto gig = std::make_unique<GridIndexedGeometry>(it.second->CloneOfGeometry());
            gig->AssignGridIndices(grid_index_boxes);
            if (not gig->HasGridIndices()) {
                continue; // we are not interested in highways entirely outside the tile
            }
            grid_highways.push_back(std::move(gig));
        }
    }
    std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> centre_circles {};
    std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> block_circles {};
    std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> dense_circles {};
    for (auto & place : urban_places) {
        std::unique_ptr<GridIndexedPreparedPolygon> centre_circle, block_circle, dense_circle;
        std::tie(centre_circle, block_circle, dense_circle) = place->CreatePreparedPlacePolygons();
        if (centre_circle->GetArea() > 0) {
            centre_circle->AssignGridIndices(grid_index_boxes);
            if (centre_circle->HasGridIndices()) {
                centre_circles.push_back(std::move(centre_circle));
            }
        }
        if (block_circle->GetArea() > 0) {
            block_circle->AssignGridIndices(grid_index_boxes);
            if (block_circle->HasGridIndices()) {
                block_circles.push_back(std::move(block_circle));
            }
        }
        if (dense_circle->GetArea() > 0) {
            dense_circle->AssignGridIndices(grid_index_boxes);
            if (dense_circle->HasGridIndices()) {
                dense_circles.push_back(std::move(dense_circle));
            }
        }
    }
    std::vector<std::unique_ptr<GridIndexedGeometry>> settlement_clusters = CreateClustersOfSettlements(lit_areas, osm_water_areas, dense_circles,
                                                                                                        grid_index_boxes);
    BOOST_LOG_TRIVIAL(info) << "Settlement clusters created: " << settlement_clusters.size();
    time_logger.Log("Time used in seconds for creating settlement clusters");
    CountZonesRelatedBuildings(buildings, "after settlement clusters");
    AssignMinimumSettlementTypeToZones(built_up_zones, settlement_clusters);
    time_logger.Log("Time used in seconds assigning minimum settlement type");
    AssignCityBlocksToZones(built_up_zones, grid_highways);
    time_logger.Log("Time used in seconds assigning city blocks to zones");
    AssignUrbanSettlementType(built_up_zones, SettlementType::centre, centre_circles);
    time_logger.Log("Time used in seconds assigning centre settlement type");
    AssignUrbanSettlementType(built_up_zones, SettlementType::block, block_circles);
    time_logger.Log("Time used in seconds assigning block settlement type");
    AssignUrbanSettlementType(built_up_zones, SettlementType::dense, dense_circles);
    time_logger.Log("Time used in seconds assigning dense settlement type");

    CountZonesRelatedBuildings(buildings, "after settlement linking");

    // =========== Now that settlement areas etc. are done, we can reduce the lit areas to those having a minimum area
    auto before_size_lit = lit_areas.size();
    lit_areas.erase(std::remove_if(lit_areas.begin(), lit_areas.end(), [](const auto & it) {
        return it->getArea() <= Parameters::Get().OWBB_BUILT_UP_MIN_LIT_AREA;
    }), lit_areas.end());
    BOOST_LOG_TRIVIAL(info) << "Reduced the number of lit areas from " << before_size_lit << " to " << lit_areas.size();

    // ============ Finally guess the land-use type ========================================
    for (auto & zone : built_up_zones) {
        if (zone->IsGenerated()) {
            zone->GuessBuildingZoneType(farm_places);
        }
    }
    time_logger.Log("Time used in seconds for guessing zone types");

    // =========== Now generate buildings if asked for ======================================
    if (Parameters::Get().OWBB_GENERATE_BUILDINGS) {
        BOOST_LOG_TRIVIAL(info) << "Generating potentially missing buildings";

        GeneratePlausibleBuildings(osm_reader, built_up_zones, highways, railways, waterways, grid_index_boxes, building_nodes_dict, buildings);

        CountZonesRelatedBuildings(buildings, "after generating buildings");
        time_logger.Log("Time used in seconds for generating buildings");
    } else {
        BOOST_LOG_TRIVIAL(info) << "No generation of buildings requested";
    }


    // ============ Exclude buildings not rendered =========================================
    std::vector<std::shared_ptr<Building>> r_buildings = RemoveNotRenderedBuildings(buildings);
    time_logger.Log("Time used in seconds for removing not rendered buildings");

    std::vector<std::shared_ptr<Building>> t_buildings = RemoveBuildingsOutsideTile(building_nodes_dict, r_buildings);
    time_logger.Log("Time used in seconds for removing buildings outside the tile boundary");

    // ============ Now let us do final relations as long as we have the nodes dict and zones =================
    //See whether we can do more building relations
    //This is done as late as possible to reduce exec time by only looking in the building's same zone
    RelateBuildingsBasedOnSharedReferences(built_up_zones, building_nodes_dict);
    time_logger.Log("Time used in seconds for relating buildings based on shared references");

    //Remove nodes that are too close together. Reasons to do so:
    // * Generally reduce mesh size due to stuff, which is not visible in the simulator
    // * Make sure that rounding errors (e.g. to float in Python) do not make overlapping points
    // * Make sure stuff like triangulation does not get disturbed by unnecessary nodes
    //Cannot be done before because we need to know which nodes are related due to neighbours.
    std::vector<std::shared_ptr<Building>> u_buildings = RemoveVeryCloseNodes(building_nodes_dict, t_buildings);
    time_logger.Log("Time used in seconds for removing very close nodes");

    // Simplification also refreshes the outer polygon for plotting and processing of trees
    std::vector<std::shared_ptr<Building>> v_buildings = SimplifyBuildingGeometries(building_nodes_dict, u_buildings);
    time_logger.Log("Time used in seconds for simplifying buildings");

    // After simplification, we can now calculate the roof ridge orientation and L-shaped roofs.
    long count {0};
    for (auto &building : v_buildings) {
        building->CalcNeighboursAndRoofHints(building_nodes_dict);
        if (building->HasRoofHint() and building->GetRoofHint(false)->HasInnerNodeDefined()) {
            count++;
        }
    }
    BOOST_LOG_TRIVIAL(info) << count << " L-shaped roofs with inner-nodes.";
    time_logger.Log("Time used in seconds for calculating roof hints");

    // Previously in Python the following was called at the end:
    //     building.update_geometry_from_refs(nodes_dict, coords_transform)
    // But because we now export the stuff and geometry is built from scratch in Python, this is not needed.

    // =========== Do the plotting =============================================
    if (Parameters::Get().DEBUG_PLOT_LANDUSE) {
        DrawLandUse(v_buildings, built_up_zones, lit_areas, settlement_clusters);
    }

    // =========== Save data to proto files for transfer to Python
    WriteBuildingStuffToProtobuf(building_nodes_dict, v_buildings, lit_areas);

    // =========== Do other stuff depending on current data structures before they go out of scope
    if (create_trees) {
        ProcessTrees(v_buildings, osm_reader, natural_waters, highways, railways, grid_index_boxes, building_nodes_dict);
    }
}
