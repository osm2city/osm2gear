// SPDX-FileCopyrightText: (C) 2022 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "geos/geom/Coordinate.h"

#include "models.h"
#include "../osm/data_reader_overpass.h"

enum class TreeOrigin {
    mapped,
    park,
    small_forest,
    scrub,
    garden,
};

/*! \brief A single tree from OSM or interpreted OSM data.

    Cf. https://wiki.openstreetmap.org/wiki/Tag:natural=tree?uselang=en
    Cf. https://wiki.openstreetmap.org/wiki/Key%3Adenotation
 */
class Tree : public GridIndexed {
    std::unique_ptr<geos::geom::Point> point_;
    TreeType type_;
    TreeOrigin origin_;
    osm_id_t city_block_id_{0};
    void ParseTags(const tags_t* tags);
public:
    Tree(LonLat lon_lat, const tags_t* tags, TreeOrigin origin, TreeType tree_type);
    Tree(const geos::geom::Coordinate &coord, TreeOrigin origin, TreeType tree_type);
    [[nodiscard]] double GetX() const {
        return point_->getX();
    }
    [[nodiscard]] double GetY() const {
        return point_->getY();
    }
    [[nodiscard]] geos::geom::Point* GetPointPtr() const {
        return point_.get();
    }
    [[nodiscard]] TreeOrigin GetTreeOrigin() const {
        return origin_;
    }
    [[nodiscard]] bool IsMappedTreeOrigin() const {
        return origin_ == TreeOrigin::mapped;
    }
    [[nodiscard]] TreeType GetTreeType() const {
        return type_;
    }
    void SetCityBlockId(const osm_id_t osm_id) {
        city_block_id_ = osm_id;
    }
    [[nodiscard]] osm_id_t GetCityBlockId() const {
        return city_block_id_;
    }
    /*! \brief Assign grid index for this tree - return false if tree not within grid boxes
     * @param grid_index_boxes
     * @return
     */
    [[nodiscard]] bool AssignGridIndex(const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes);
};

void ProcessTrees(std::vector<std::shared_ptr<Building>> &buildings, OSMDataReaderOverpass &osm_reader,
                  const std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> &natural_waters,
                  const std::map<osm_id_t, std::unique_ptr<Highway>> &highways,
                  const std::map<osm_id_t, std::unique_ptr<RailwayLine>> &railways,
                  const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                  const node_map_t &building_nodes);
