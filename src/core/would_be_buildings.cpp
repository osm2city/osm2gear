// SPDX-FileCopyrightText: (C) 2023 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#include "would_be_buildings.h"

#include "boost/log/trivial.hpp"

#include <geos/linearref/LengthIndexedLine.h>
#include "geos/operation/buffer/BufferParameters.h"

#include "plotting.h"
#include "models.h"

/*! \brief Link blocked areas to buildings zones if they intersect.
 * If a geometry is totally within a building_zone, then it cannot be in another and therefore can be removed.
 * Thereby geometry processing can be optimized.
 */
static void ProcessBlockedAreasForBuildingZone(BuildingZone *building_zone, std::map<osm_id_t, std::shared_ptr<GridIndexedGeometry>> &candidates) {
    std::vector<osm_id_t> to_remove;
    for (auto & it : candidates) {
        if (GridIndexed::HaveCommonIndices(building_zone->GetGridIndices(), it.second->GetGridIndices())) {
            bool is_blocked = false;
            if (it.second->WithinOther(building_zone->GetGeometry())) {
                is_blocked = true;
                to_remove.push_back(it.first);
            } else if (it.second->Intersects(building_zone->GetGeometry())) {
                is_blocked = true;
            }
            if (is_blocked) {
                building_zone->AssignBlockedArea(it.second);
            }
        }
    }
    for (auto key : to_remove) {
        candidates.erase(key);
    }
}

/*! \brief Prepares a building zone with blocked areas and highway parts.
 As soon as an element is entirely within a building zone, it is removed
 from the lists in order to speedup by reducing the number of stuff to compare in the next
 building zone (the lists are shared / passed by reference).
 In order to make these removals fast the data structures must be dicts instead of lists.

 Another possibility would be parallel processing, but that would lead to big
 memory consumption due to copies of data structures in each process
 or overhead due to shared data structure (e.g. Manager.dict()).
 A last possibility would be a global variable in the module, which on at least Linux
 would be shared by all.
 */
static void PrepareBuildingZonesForBuildingGeneration(std::vector<BuildingZone*> &used_zones,
                                                      const std::map<osm_id_t, std::unique_ptr<Highway>> &highways,
                                                      const std::map<osm_id_t, std::unique_ptr<RailwayLine>> &railways,
                                                      const std::map<osm_id_t, std::unique_ptr<Waterway>> &waterways,
                                                      std::map<osm_id_t, std::shared_ptr<GridIndexedGeometry>> &blocked_spaces,
                                                      const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
    // first we need to construct blocked areas and assign them a grid index
    constexpr int cap_size = 4;
    constexpr int end_cap_style = geos::operation::buffer::BufferParameters::CAP_FLAT;
    std::map<osm_id_t, std::shared_ptr<GridIndexedGeometry>> grid_highways;
    for (auto &it : highways){
        auto gig = std::make_shared<GridIndexedGeometry>(it.second->GetGeometry()->buffer(it.second->GetWidth() / 2, cap_size, end_cap_style));
        gig->AssignGridIndices(grid_index_boxes);
        if (gig->HasGridIndices()) {
            grid_highways[it.first] = std::move(gig);
        }
    }
    std::map<osm_id_t, std::shared_ptr<GridIndexedGeometry>> grid_railways;
    for (auto &it : railways){
        auto gig = std::make_shared<GridIndexedGeometry>(it.second->GetGeometry()->buffer(it.second->GetWidth() / 2, cap_size, end_cap_style));
        gig->AssignGridIndices(grid_index_boxes);
        if (gig->HasGridIndices()) {
            grid_railways[it.first] = std::move(gig);
        }
    }
    std::map<osm_id_t, std::shared_ptr<GridIndexedGeometry>> grid_waterways;
    for (auto &it : waterways){
        auto gig = std::make_shared<GridIndexedGeometry>(it.second->GetGeometry()->buffer(it.second->GetWidth() / 2, cap_size, end_cap_style));
        gig->AssignGridIndices(grid_index_boxes);
        if (gig->HasGridIndices()) {
            grid_waterways[it.first] = std::move(gig);
        }
    }
    for (auto zone : used_zones) {
        zone->ProcessOwnBuildingsAsBlockedAreas(grid_index_boxes);
        zone->ProcessCityBlocksWithBuildingsAsBlockedAreas(grid_index_boxes);
        ProcessBlockedAreasForBuildingZone(zone, blocked_spaces);
        ProcessBlockedAreasForBuildingZone(zone, grid_highways);
        ProcessBlockedAreasForBuildingZone(zone, grid_railways);
        ProcessBlockedAreasForBuildingZone(zone, grid_waterways);
    }
}

/*! \brief Link highways to BuildingZones to prepare for building generation.
 Either as whole if entirely within or as a set of intersections if intersecting.
 */
static std::vector<std::unique_ptr<GenBuildingWay>>
FindGenBuildingWays(std::vector<BuildingZone*> &used_zones, std::map<osm_id_t, std::unique_ptr<Highway>> &highways) {
    std::vector<std::unique_ptr<GenBuildingWay>> gen_roads;
    std::vector<std::unique_ptr<geos::geom::LineString>> temp_geoms;
    for (auto zone : used_zones) {
        auto zone_buffered_geom = zone->GetGeometry()->buffer(Parameters::Get().OWBB_BUILDING_ZONE_HIGHWAY_SEARCH_BUFFER);
        for (auto &it : highways) {
            temp_geoms.clear();
            if (not it.second->PopulateBuildingsAlong()) {
                continue;
            }
            if (zone_buffered_geom->contains(it.second->GetGeometry())) {
                auto cloned_geom = it.second->CloneOfGeometry();
                if (cloned_geom->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_LINESTRING) {
                    std::unique_ptr<geos::geom::LineString> linestring(dynamic_cast<geos::geom::LineString*>(cloned_geom.release()));
                    temp_geoms.push_back(std::move(linestring));
                } // else there is nothing to do - should never happen anyway
            } else if (it.second->GetGeometry()->intersects(zone_buffered_geom.get())) {
                auto intersection_geom = it.second->GetGeometry()->intersection(zone_buffered_geom.get());
                if (intersection_geom->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_LINESTRING or
                    intersection_geom->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_LINEARRING) {
                    // We guess that the cast was successful, because we checked the type.
                    // Therefore, no need to double-check that linestr_ptr is not a nupllptr
                    std::unique_ptr<geos::geom::LineString> linestring(dynamic_cast<geos::geom::LineString*>(intersection_geom.release()));
                    temp_geoms.push_back(std::move(linestring));
                } else if (intersection_geom->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTILINESTRING) {
                    const auto number_geometries = intersection_geom->getNumGeometries();
                    for (std::size_t i = 0; i < number_geometries; ++i) {
                        auto part_geom = intersection_geom->getGeometryN(i)->clone();
                        if (part_geom->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_LINESTRING and
                            !part_geom->isEmpty() and
                            part_geom->isValid() and
                            part_geom->getLength() >= Parameters::Get().OWBB_MIN_STREET_LENGTH) {
                            std::unique_ptr<geos::geom::LineString> linestring(dynamic_cast<geos::geom::LineString*>(part_geom.release()));
                            temp_geoms.push_back(std::move(linestring));
                        }
                    }
                }
            }
            // create the gen_ways
            for (auto &geom : temp_geoms) {
                gen_roads.push_back(std::make_unique<GenBuildingWay>(std::move(geom), it.second->GetWidth(), zone, it.second->GetOSMId()));
            }
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Number of GenBuildingWays found along which buildings can be generated: " << gen_roads.size();
    return gen_roads;
}

/*! \brief Generate plausible buildings along a way.
 * The central assumption is that existing blocked areas incl. buildings du not need a buffer.
 * The to be populated buildings all bring their own constraints with regards to distance to road, distance to other
 * buildings etc.

 * @param if is_along is true, then direction along way is from start to end

 * Updates the referenced buildings vector with all new generated buildings
 */
static void GenerateBuildingsAlongWay(std::unique_ptr<GenBuildingWay> &gen_way,
                                      std::unique_ptr<geos::geom::Polygon> & tile_boundary,
                                      bool is_along, const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                                      node_map_t &building_nodes, std::vector<std::shared_ptr<Building>> &buildings) {
    double travelled_along {0};
    double extra_travelled_along {0}; //additional length given that a prev gen_building has been added and therefore some steps can be skipped
    const double line_length = gen_way->GetLineString()->getLength();
    const geos::linearref::LengthIndexedLine lil(gen_way->GetLineString());
    geos::geom::Coordinate this_point;
    geos::geom::Coordinate prev_point;
    std::unique_ptr<GenBuilding> gen_building;
    CityBlock * current_city_block;
    if (is_along) {
        this_point = lil.extractPoint(0, 0);
    } else {
        this_point = lil.extractPoint(0, line_length);
    }
    bool valid_new_gen_building {true}; // true to trigger first GenBuilding
    bool prev_building_attached {false};

    while (travelled_along < line_length) {

        if (extra_travelled_along > 0) { // i.e. we just placed a building
            int step_distance = Parameters::Get().OWBB_STEP_DISTANCE;
            if (prev_building_attached) {
                step_distance = 1;
            }
            travelled_along += step_distance + extra_travelled_along;
            extra_travelled_along = 0; // reset
        } else {
            travelled_along += Parameters::Get().OWBB_STEP_DISTANCE;
        }
        prev_point = this_point;
        if (is_along) {
            this_point = lil.extractPoint(travelled_along);
        } else {
            this_point = lil.extractPoint(line_length - travelled_along);
        }
        const double angle_deg = CalcAngleOfLineLocal(prev_point.x, prev_point.y, this_point.x, this_point.y);
        auto p = CalcPointAngleAway(this_point.x, this_point.y,
                                    gen_way->GetWidth() / 2 + Parameters::Get().OWBB_BUILDING_ZONE_HIGHWAY_SEARCH_BUFFER,
                                    NormalizedDegrees(angle_deg + 90));
        auto city_block = gen_way->GetBuilingZone()->FindCityBlockForPoint(p.first, p.second);
        if (not city_block.has_value()) {
            continue; //if there is no city_block, then we should not try to place a building
        } else {
            if (city_block.value()->HasOSMBuildings() and not city_block.value()->IsUsedForBuildingGeneration()) {
                continue;
            } else {
                current_city_block = city_block.value();
                current_city_block->SetUsedForBuildingGeneration();
            }
        }
        if (valid_new_gen_building) {
            gen_building = current_city_block->CreateTypicalGenBuilding();
        }
        auto perimeter = gen_building->CreatePerimeterRectangle(gen_way->GetWidth(), this_point, angle_deg,
                                                                true, grid_index_boxes);
        valid_new_gen_building = false;
        if (tile_boundary->contains(perimeter->GetGeometry())) {
            if (gen_way->GetBuilingZone()->HasIntersectingBlockedArea(perimeter)) {
                continue;
            }
            if (not current_city_block->ContainsProperly(perimeter->GetGeometry())) {
                continue;
            }
            valid_new_gen_building = true;
            auto blocked_area = gen_building->CreatePerimeterRectangle(gen_way->GetWidth(), this_point, angle_deg,
                                                                       false, grid_index_boxes);
            gen_way->GetBuilingZone()->AssignBlockedArea(blocked_area);
            if (gen_building->IsAttached()) {
                prev_building_attached = true;
            }
            std::shared_ptr<Building> new_building = gen_building->TransformToBuilding(gen_way->GetWidth(), this_point, angle_deg, building_nodes);
            current_city_block->RelateBuilding(new_building);
            buildings.push_back(new_building);
            extra_travelled_along = gen_building->GetWidthWithBuffer() / 2;
        }
    }
}

static void
GenerateExtraBuildings(std::vector<std::unique_ptr<GenBuildingWay>> & gen_ways,
                       const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                       node_map_t &building_nodes, std::vector<std::shared_ptr<Building>> &buildings) {
    auto tile_boundary = TileHandler::Get()->GetTileExtentLocalPolygon();
    const size_t buildings_before = buildings.size();
    for (auto &gen_way : gen_ways) {
        GenerateBuildingsAlongWay(gen_way, tile_boundary, true, grid_index_boxes, building_nodes, buildings);
        GenerateBuildingsAlongWay(gen_way, tile_boundary, false, grid_index_boxes, building_nodes, buildings);
    }
    const size_t buildings_diff = buildings.size() - buildings_before;
    BOOST_LOG_TRIVIAL(info) << "Generated " << buildings_diff << " extra buildings";
}

/*! \brief Make sure generated attached buildings are attached to other buildings.
 *
 * During generation of buildings attached buildings are added very close to existing buildings, but
 * do not hare references. Here we search for close nodes in neighbour buildings and then use
 * shared nodes.
 * @param buildings
 */
static void
AttachAttachedBuildings(const std::vector<BuildingZone*> &used_zones, const node_map_t &building_nodes) {
    long attachments_made {};
    for (const auto &bz : used_zones) {
        auto city_blocks = bz->GetCityBlocks();
        for (const auto &city_block : *city_blocks) {
            if (city_block->IsUsedForBuildingGeneration() and city_block->IsBuildingGenAttached()) {
                auto buildings = city_block->GetOSMBuildings();
                std::shared_ptr<Building> first_building;
                std::shared_ptr<Building> second_building;
                for (std::size_t i = 0; i < buildings.size(); i++) {
                    first_building = buildings[i];
                    for (std::size_t j = i + 1; j < buildings.size(); j++) {
                        second_building = buildings[j];
                        if (first_building->GetOSMId() == second_building->GetOSMId()) {
                            continue;  // do not compare with self
                        }
                        auto first_refs = *first_building->GetOuterRefs();
                        auto second_refs = *second_building->GetOuterRefs();
                        std::map<std::size_t, osm_id_t> matched_second;
                        for (std::size_t ref_i = 0; ref_i < first_refs.size(); ref_i++) {
                            auto sg_first = building_nodes.at(first_refs[ref_i])->GetSGGeod();
                            double smallest_dist_second {999.};
                            for (std::size_t ref_j = 0; ref_j < second_refs.size(); ref_j++) {
                                auto sg_second = building_nodes.at(second_refs[ref_j])->GetSGGeod();
                                auto dist = SGGeodesy::distanceM(sg_first, sg_second);
                                if (dist < smallest_dist_second and SGGeodesy::distanceM(sg_first, sg_second) < Parameters::Get().OWBB_ATTACHED_NODES_MAX_DIST) {
                                    matched_second[ref_j] = first_refs[ref_i];
                                    smallest_dist_second = dist;
                                    break; // can only match one from second for same first
                                }
                            }
                        }
                        if (matched_second.size() == 2) {
                            second_building->UpdateOuterRefsAttached(matched_second, building_nodes);
                            attachments_made++;
                        }
                    }
                }
            }
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Related " << attachments_made << " attached generated buildings";
}

void GeneratePlausibleBuildings(OSMDataReaderOverpass &osm_reader,
                                const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                                std::map<osm_id_t, std::unique_ptr<Highway>> &highways,
                                std::map<osm_id_t, std::unique_ptr<RailwayLine>> &railways,
                                std::map<osm_id_t, std::unique_ptr<Waterway>> &waterways,
                                const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                                node_map_t &building_nodes,
                                std::vector<std::shared_ptr<Building>> &buildings) {
    TimeLogger time_logger {};

    // =========== SELECT ZONES FOR GENERATION OF BUILDINGS =============
    std::vector<BuildingZone*> used_zones {};
    for (auto & b_zone : built_up_zones){
        if(b_zone->UseForBuildingGeneration()){
            used_zones.push_back(b_zone.get());
        }
    }
    BOOST_LOG_TRIVIAL(debug) << "Number of selected building zones for generation of buildings: " << used_zones.size() << " out of " << built_up_zones.size();
    time_logger.Log("Time used in seconds for selecting building zones for generation");

    // =========== START GENERATION OF BUILDINGS =============
    auto blocked_spaces = ProcessOpenSpaces(osm_reader, grid_index_boxes, false);
    time_logger.Log("Time used in seconds for creating open spaces from OSM");
    PrepareBuildingZonesForBuildingGeneration(used_zones, highways, railways, waterways, blocked_spaces, grid_index_boxes);
    time_logger.Log("Time used in seconds for preparing building zones for building generation");
    auto gen_building_ways = FindGenBuildingWays(used_zones, highways);
    time_logger.Log("Time used in seconds for finding ways for building generation");

    GenerateExtraBuildings(gen_building_ways, grid_index_boxes, building_nodes, buildings);
    time_logger.Log("Time used in seconds for generating extra buildings");

    AttachAttachedBuildings(used_zones, building_nodes);
    time_logger.Log("Time used in seconds for attaching attached buildings");
}