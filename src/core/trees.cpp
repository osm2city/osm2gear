// SPDX-FileCopyrightText: (C) 2022 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#include "trees.h"

#include <fstream>
#include <iomanip>
#include <random>
#include <sstream>
#include <vector>

#include "boost/log/trivial.hpp"

#include <geos/geom/GeometryFactory.h>
#include <geos/linearref/LengthIndexedLine.h>
#include "geos/operation/buffer/BufferParameters.h"

#include "osgDB/Options"

#include <simgear/scene/util/SGImageUtils.hxx>

#include "buildings.h"
#include "plotting.h"
#include "stg_io.h"
#include "elev_probe.h"
#include "../osm/constants.h"

static constexpr int REMAINING_CITY_BLOCK_ID {0};

void Tree::ParseTags(const tags_t* tags) {
    if (tags->contains(k_denotation)) {  //significant trees should be treated as such
        type_ = TreeType::significant_trees;
    }
}

Tree::Tree(const LonLat lon_lat, const tags_t* tags, const TreeOrigin origin, const TreeType tree_type): origin_ {origin}, type_ {tree_type} {
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    point_ = factory->createPoint(TileHandler::Get()->ToLocal(lon_lat));
    ParseTags(tags);
}

Tree::Tree(const geos::geom::Coordinate &coordinate, const TreeOrigin origin,
           const TreeType tree_type): origin_ {origin}, type_ {tree_type} {
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    point_ = factory->createPoint(coordinate);
}

bool Tree::AssignGridIndex(const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
    for (auto & gib : grid_index_boxes) {
        if (not gib.second->disjoint(GetPointPtr())) {
            AddGridIndex(gib.first);
            return true;
        }
    }
    return false;
}

void AssignTreesToCityBlocks(std::vector<std::shared_ptr<Tree>> &trees,
                             const std::set<CityBlock*> &city_blocks, const CityBlock * remaining_block,
                             std::map<osm_id_t, std::unique_ptr<std::vector<std::shared_ptr<Tree>>>> &trees_in_city_block_map) {
    for (auto & tree : trees) {
        auto city_block_id = remaining_block->GetOSMId(); // default if not in CityBlock
        for (const auto city_block : city_blocks) {
            if (not tree->GetPointPtr()->disjoint(city_block->GetGeometry())) {
                city_block_id = city_block->GetOSMId();
                break;
            }
        }
        tree->SetCityBlockId(city_block_id);
        trees_in_city_block_map[city_block_id]->push_back(tree);
    }
}

/*! \brief Extend the existing list of trees with new trees if the new trees have a minimal dist from the existing ones.
 *
 * @param potential_trees
 * @param existing_trees
 */
void ExtendTreesIfDistanceOk(std::vector<std::shared_ptr<Tree>> &potential_trees,
                             std::vector<std::shared_ptr<Tree>> &existing_trees) {
    std::vector<std::shared_ptr<Tree>> to_be_extended_trees {};
    for (auto &pot : potential_trees) {
        bool is_good = true;
        for (const auto &other : existing_trees) {
            if (GridIndexed::HaveCommonIndices(pot->GetGridIndices(), other->GetGridIndices())) {
                if (CalcDistanceLocal(pot->GetX(), pot->GetY(), other->GetX(), other->GetY()) < Parameters::Get().TREES_DIST_MINIMAL) {
                    is_good = false;
                    break;
                }
            }
        }
        if (is_good) {
            to_be_extended_trees.push_back(pot);
        }
    }
    //we first expand at the end to be able to reserve a good amount of space and in order not to compare
    //with the distance of newly added trees
    existing_trees.reserve(existing_trees.size() + to_be_extended_trees.size());
    for (auto &tree : to_be_extended_trees) {
        existing_trees.push_back(tree);
    }
}


/*! \brief Extend the existing list of trees with new trees if the new trees have a minimal dist from the existing ones.
 *
 * @param potential_trees
 * @param trees_to_extend
 */
void ExtendTreesIfDistanceOkCityBlock(std::vector<std::shared_ptr<Tree>> &potential_trees,
                                      std::vector<std::shared_ptr<Tree>> &trees_to_extend,
                                      std::map<osm_id_t, std::unique_ptr<std::vector<std::shared_ptr<Tree>>>> &trees_in_city_block_map) {
    std::vector<std::shared_ptr<Tree>> to_be_extended_trees {};
    for (auto &potential_tree : potential_trees) {
        bool is_good = true;
        //only test against trees in real city blocks - we accept too close trees in remaining city zone for performance reasons
        //and because probability is low
        if (potential_tree->GetCityBlockId() != REMAINING_CITY_BLOCK_ID) {
            for (const auto &existing_tree : *trees_in_city_block_map[potential_tree->GetCityBlockId()]) {
                if (GridIndexed::HaveCommonIndices(potential_tree->GetGridIndices(), existing_tree->GetGridIndices())) {
                    if (CalcDistanceLocal(potential_tree->GetX(), potential_tree->GetY(), existing_tree->GetX(), existing_tree->GetY()) < Parameters::Get().TREES_DIST_MINIMAL) {
                        is_good = false;
                        break;
                    }
                }
            }
        }
        if (is_good) {
            to_be_extended_trees.push_back(potential_tree);
        }
    }
    //we first expand at the end to be able to reserve a good amount of space and in order not to compare
    //with the distance of newly added trees
    trees_to_extend.reserve(trees_to_extend.size() + to_be_extended_trees.size());
    for (auto &tree : to_be_extended_trees) {
        trees_to_extend.push_back(tree);
        // NB: cannot be added to map before, because otherwise the test above will be against itself
        trees_in_city_block_map[tree->GetCityBlockId()]->push_back(tree);
    }
}

/*! \brief Place trees based on manually mapped single trees in OSM.
 *
 * @param osm_reader
 * @return
 */
std::vector<std::shared_ptr<Tree>> ProcessOSMTreeNodes(const OSMDataReaderOverpass &osm_reader,
                                                       const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
    std::vector<std::shared_ptr<Tree>> trees {};
    std::vector<std::string> tree_kv {};
    auto [nodes_dict, ways_dict] = osm_reader.FetchNodesIsolatedFromKeyValues({{k_natural, v_tree}});
    for (auto &it : nodes_dict) {
        auto tree = std::make_shared<Tree>(it.second->GetLonLat(), it.second->GetTags(), TreeOrigin::mapped,
                                                          TreeType::significant_trees);
        if (tree->AssignGridIndex(grid_index_boxes)) {
            trees.push_back(std::move(tree));
        }
    }
    return trees;
}

/*! \brief Trees in a row as mapped in OSM (natural=tree_row).
 *
 * Cf. https://wiki.openstreetmap.org/wiki/Tag:natural%3Dtree_row - the single trees do not have to be mapped.
 * Therefore, we use a heuristic in the code to test whether the trees probably were mapped or not.
 *
 * @param existing_trees gets extended with additional trees
 */
void ProcessOSMTreeRows(std::vector<std::shared_ptr<Tree>> &existing_trees,
                        const OSMDataReaderOverpass &osm_reader,
                        const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
    std::vector<std::string> tree_row_kv {};
    auto [nodes_dict, ways_dict] = osm_reader.FetchWaysFromKeyValues({{k_natural, v_tree_row}});

    std::vector<std::shared_ptr<Tree>> potential_trees {};
    for (auto &it : ways_dict) {
        try {
            const std::unique_ptr<geos::geom::LineString> line = Way::LineStringFromRefs(it.second->GetOSMId(),
                                                                                         it.second->GetRefs(),
                                                                                         nodes_dict);
            if (line->isValid()) {
                if (line->getLength() / static_cast<double>(it.second->GetRefs()->size()) > Parameters::Get().TREES_MAX_AVG_DIST_TREES_ROW) {
                    auto length_indexed_line = geos::linearref::LengthIndexedLine(line.get());
                    for (int i = 0; i < static_cast<int>(line->getLength() / Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED); ++i) {
                        auto my_coord = length_indexed_line.extractPoint(i * Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED);
                        auto tree = std::make_shared<Tree>(my_coord, TreeOrigin::mapped, TreeType::significant_trees);
                        if (tree->AssignGridIndex(grid_index_boxes)) {
                            potential_trees.push_back(std::move(tree));
                        }
                    }
                    // and then always the last node
                    const auto last_node = nodes_dict[it.second->GetRefs()->back()].get();
                    auto tree = std::make_shared<Tree>(last_node->GetLonLat(),
                                                       last_node->GetTags(),
                                                       TreeOrigin::mapped,
                                                       TreeType::significant_trees);
                    if (tree->AssignGridIndex(grid_index_boxes)) {
                        potential_trees.push_back(std::move(tree));
                    }
                } else {
                    for (auto ref : *it.second->GetRefs()) {
                        const auto node = nodes_dict[ref].get();
                        auto tree = std::make_shared<Tree>(node->GetLonLat(),
                                                           node->GetTags(),
                                                           TreeOrigin::mapped,
                                                           TreeType::significant_trees);
                        if (tree->AssignGridIndex(grid_index_boxes)) {
                            potential_trees.push_back(std::move(tree));
                        }
                    }
                }
            }
        } catch (const InvalidGeometryFromOSM &e) {
            LogDebugWarning(e.what());
        }
    }
    ExtendTreesIfDistanceOk(potential_trees, existing_trees);
}

/*! \brief Trees in a line as mapped in OSM (tree_lined=*).
 *
 * Cf. https://wiki.openstreetmap.org/wiki/Key:tree_lined
 * We assume that "yes" is the same as "both".
 *
 * Must be called after TreeRows - because tree in rows is an actual mapping, while this is a feature indication.
 *
 * @param existing_trees gets extended with additional trees
 * @param osm_reader
 */
void ProcessOSMTreeLines(std::vector<std::shared_ptr<Tree>> &existing_trees,
                         const OSMDataReaderOverpass &osm_reader,
                         const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
    auto [nodes_dict, ways_dict] = osm_reader.FetchWaysFromKeys({k_tree_lined});

    std::vector<std::shared_ptr<Tree>> potential_trees {};
    for (auto &[osm_id, way] : ways_dict) {
        if (way->GetTags()->contains(k_natural) and way->GetTags()->at(k_natural) == v_tree_row) {
            continue; // was already processed in ProcessOSMTreeRows()
        }
        auto tag_value = way->GetTags()->at(k_tree_lined);
        if (tag_value == v_no) {
            continue;
        }
        const std::unique_ptr<geos::geom::LineString> line = Way::LineStringFromRefs(way->GetOSMId(),
                                                                                     way->GetRefs(),
                                                                                     nodes_dict);
        if (line->isValid()) {
            auto length_indexed_line = geos::linearref::LengthIndexedLine(line.get());
            for (int i = 0; i < static_cast<int>(line->getLength() / Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED); ++i) {
                if (tag_value == v_left or tag_value == v_both or tag_value == v_yes) {
                    auto my_coord = length_indexed_line.extractPoint(
                            i * Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED, 4.0);
                    auto tree = std::make_shared<Tree>(my_coord, TreeOrigin::mapped, TreeType::significant_trees);
                    if (tree->AssignGridIndex(grid_index_boxes)) {
                        potential_trees.push_back(std::move(tree));
                    }
                }
                if (tag_value == v_right or tag_value == v_both or tag_value == v_yes) {
                    auto my_coord = length_indexed_line.extractPoint(
                            i * Parameters::Get().TREES_DIST_TREES_ROW_CALCULATED, -4.0);
                    auto tree = std::make_shared<Tree>(my_coord, TreeOrigin::mapped, TreeType::significant_trees);
                    if (tree->AssignGridIndex(grid_index_boxes)) {
                        potential_trees.push_back(std::move(tree));
                    }
                }
            }
        }
    }
    ExtendTreesIfDistanceOk(potential_trees, existing_trees);
}

/*! \brief Extracts all city blocks from existing buildings.
 *
 * If the zone linked to a building is not a CityBlock, then use a generic one spanning the whole tile.
 * In buildings._clean_building_zones_dangling_children there is also some cleanup of not valid buildings
 * However, that uses significant runtime and e.g. for Edinburgh out of ca. 110k buildings only ca.
 * 150 would be removed - which does not matter for what buildings are used here (collision detection)
 *
 * @param buildings
 * @return
 */
std::set<CityBlock*> PrepareCityBlocks(CityBlock* remaining_block, std::vector<std::shared_ptr<Building>> &buildings) {
    std::set<CityBlock*> city_blocks {};

    for (auto &building : buildings) {
        if (const auto zone = building->GetZone(); zone->GetZoneType() == ZoneType::city_block) {
            city_blocks.insert(dynamic_cast<CityBlock*>(zone));
        } else {
            remaining_block->RelateBuilding(building);
        }
    }
    return city_blocks;
}

/*! \brief Find the geometry of small forests
 * 'landuse=>recreation_ground' would be a possibility for parks, but often has also swimming pools etc.
 * making it a bit difficult
 */
std::vector<std::shared_ptr<geos::geom::Geometry>> ProcessTreeArea(const OSMDataReaderOverpass &osm_reader, const TreeOrigin &tree_origin) {
    std::vector<std::pair<std::string, std::string>> query_kv{};
    if (tree_origin == TreeOrigin::small_forest) {
        query_kv.emplace_back(k_landuse, v_forest);
    } else if (tree_origin == TreeOrigin::scrub) {
        query_kv.emplace_back(k_natural, v_scrub);
    } else {
        query_kv.emplace_back(k_leisure, v_park);
    }
    auto [nodes_dict, ways_dict] = osm_reader.FetchWaysFromKeyValues(query_kv);

    std::vector<std::shared_ptr<geos::geom::Geometry>> tree_areas{};
    for (auto &[osm_id, way] : ways_dict) {
        if (auto poly = way->PolygonFromOSMWay(nodes_dict); poly->isValid()) {
            if (tree_origin == TreeOrigin::park) {
                if (poly->getArea() > Parameters::Get().TREES_PARK_MIN_SIZE) {
                    tree_areas.push_back(std::move(poly));
                }
            } else { //small forests or scrub
                if (poly->getArea() < Parameters::Get().TREES_SMALL_FOREST_MAX_SIZE) {
                    tree_areas.push_back(std::move(poly));
                }
            }
        }
    }
    return tree_areas;
}

/*! \brief Tests whether a point is within a building respectively not at least min_distance away.
 *
 * This is a fast method not using geometry and conservative, because e.g. if building is like a diamond in bounds,
 * then lots of space is given away. But it is ok, because often trees are not too close to buildings.
 *
 * For holes in buildings we need to use more precise method - but then again there are not so many
 * buildings with holes. And we increase the test distance, because we do not want a tree to fill the whole hole.
 *
 * The use of CityBlocks should speed up the process considerably by using fewer geometric calculations.
 * @return true if point is considered within the building - and therefore there should not be placed something.
 */
bool TestIsCoordInBuilding(const geos::geom::Point* point, const CityBlock* city_block, const double min_dist,
                           const bool check_building_holes, const node_map_t &building_nodes) {
    // outside of building (could also be a building inside a hole in a building
    for (const auto& building : city_block->GetOSMBuildings()) {
        if (check_building_holes) {
            if (building->CheckEnoughSpaceInHoles(point->buffer(min_dist * 1.2), building_nodes)) {
                return false; // the point is in a hole of the building and therefore not in the building
            }
        }
        auto bounds = building->GetOuterPolygon()->getEnvelopeInternal();
        if ((bounds->getMinX() - min_dist) < point->getX() and point->getX() < (bounds->getMaxX() + min_dist) and (bounds->getMinY() - min_dist) < point->getY() and point->getY() < (bounds->getMaxY() + min_dist)) {
            return true; // the point is in the building
        }
    }
    return false;
}

/*! \brief Creates a random set of points for trees within a polygon based on an average distance and a skip rate.
 *
 * This is not a very efficient algorithm, but it should be enough to give an ok distribution.
 * @param area_bounds
 * @param area_prep_geom
 * @param default_distance
 * @param keep_rate
 * @return
 */
std::vector<std::unique_ptr<geos::geom::Coordinate>> GenerateRandomTreePointsInArea(const geos::geom::Envelope* area_bounds,
                                                                                    const std::unique_ptr<geos::geom::prep::PreparedPolygon> &area_prep_geom,
                                                                                    const int default_distance, const double keep_rate) {
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    std::vector<std::unique_ptr<geos::geom::Coordinate>> random_coords {};

    const auto [min_coord, max_coord] = TileHandler::Get()->GetTileExtentLocal();
    const double min_x = std::max(min_coord.x, area_bounds->getMinX());
    const double max_x = std::min(max_coord.x, area_bounds->getMaxX());
    const double min_y = std::max(min_coord.y, area_bounds->getMinY());
    const double max_y = std::min(max_coord.y, area_bounds->getMaxY());

    const int run_x {static_cast<int>(std::floor((max_x - min_x) / default_distance))};
    const int run_y {static_cast<int>(std::floor((max_y - min_y) / default_distance))};

    const auto generator = TileHandler::Get()->GetRandomNumberGenerator();
    std::uniform_real_distribution<double> keeping(0.0,1.0);
    std::uniform_real_distribution<double> variation(-default_distance / Parameters::Get().TREES_RADIUS_TEST,
                                                     default_distance / Parameters::Get().TREES_RADIUS_TEST);
    for (int i = 0; i < run_x; ++i) {
        for (int j = 0; j < run_y; ++j) {
            if (keeping(*generator) > keep_rate) { // e.g. if random = 0.5 and keep_rate = 0.3, then this tree is not kept
                continue;
            }
            auto x = min_x + i * default_distance + variation(*generator);
            auto y = min_y + j * default_distance + variation(*generator);
            auto coord = geos::geom::Coordinate(x, y);
            if (const auto point = factory->createPoint(coord); area_prep_geom->contains(point.get())) {
                random_coords.push_back(std::make_unique<geos::geom::Coordinate>(x, y));
            }
        }
    }
    return random_coords;
}

/*! \brief Creates random trees in an area respecting the presence of buildings.
 *
 * The trees are geometrically tested against the boundary box of buildings (not the whole tree, just the centre).
 * In parks trees are often around open spaces and not just randomly distributed - this heuristic is taken into
 * account by using a Perlin noise image.
 * @param area_bounds
 * @param area_prep_geom
 * @param city_blocks
 * @param potential_trees created trees are added to this container
 * @param default_distance
 * @param keep_rate
 * @return
 */
void PlaceRandomTreesInArea(const geos::geom::Envelope* area_bounds,
                            const std::unique_ptr <geos::geom::prep::PreparedPolygon> &area_prep_geom,
                            const std::vector<std::shared_ptr<geos::geom::Geometry>> &test_against_areas,
                            const std::set<CityBlock*> &city_blocks,
                            const CityBlock* remaining_block,
                            const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                            std::vector<std::shared_ptr<Tree>> &potential_trees,
                            const int default_distance, const double keep_rate, const TreeOrigin &tree_origin,
                            const node_map_t &building_nodes) {
    // create random points
    std::vector<std::unique_ptr<geos::geom::Coordinate>> random_coords = GenerateRandomTreePointsInArea(area_bounds,
                                                                                                        area_prep_geom,
                                                                                                        default_distance,
                                                                                                        keep_rate);
    if (random_coords.empty()) {
        return;  //can happen if the area is too small for trees -> do not waste more computing
    }

    // read the Perlin noise file
    std::filesystem::path file_path = std::string(GetEnv("O2C_PATH_TO_DATA"));
    file_path /= "tex";
    file_path /= "perlin_noise_trees.png";
    std::ifstream file(file_path, std::ios_base::in | std::ios_base::binary);
    if (not std::filesystem::exists(file_path)) {
        std::string message = "File ";
        message.append(file_path);
        message.append(" does not exist");
        throw std::invalid_argument(message);
    }
    osg::ref_ptr<osgDB::Options> options = new osgDB::Options();
    auto perlin_noise = simgear::ImageUtils::readStream(file, options);

    // find the city blocks, which might have buildings which could interfere
    std::set<CityBlock*> test_city_blocks {};
    for (auto city_block : city_blocks) {
        if (not area_prep_geom->disjoint(city_block->GetGeometry())) {
            test_city_blocks.insert(city_block);
        }
    }

    // now we are ready to place trees
    auto factory = TileHandler::Get()->GetGeometryFactory();
    int s_coord;
    int t_coord;
    int offset;
    TreeType tree_type;
    float red_value;
    constexpr int perlin_image_size{512};
    for (auto &random_coord : random_coords) {
        if (tree_origin == TreeOrigin::park) {
            if (random_coord->x >= 0.) {
                s_coord = static_cast<int>(random_coord->x) % perlin_image_size;
            } else {
                offset = (-1 * static_cast<int>(random_coord->x) / perlin_image_size + 1) *
                         perlin_image_size;
                s_coord = (offset + static_cast<int>(random_coord->x)) % perlin_image_size;
            }
            if (random_coord->y >= 0.) {
                t_coord = static_cast<int>(random_coord->y) % perlin_image_size;
            } else {
                offset = (-1 * static_cast<int>(random_coord->y) / perlin_image_size + 1) *
                         perlin_image_size;
                t_coord = (offset + static_cast<int>(random_coord->y)) % perlin_image_size;
            }

            red_value = perlin_noise->getColor(s_coord, t_coord).r();
            if (red_value <= 0.45) {
                continue;  // no tree planted - open space (more white in Perlin image)
            }
            if (red_value <= 0.60) {
                tree_type = TreeType::garden_vegetation;
            } else {
                tree_type = TreeType::significant_trees;
            }
        } else if (tree_origin == TreeOrigin::small_forest) {
            tree_type = TreeType::significant_trees;
        } else { // scrub
            tree_type = TreeType::garden_vegetation;
        }
        auto point = factory->createPoint(geos::geom::Coordinate(random_coord->x, random_coord->y));

        //test against other tree areas
        bool is_in_test_against_area {false};
        for (auto &test_against_area : test_against_areas) {
            if (test_against_area->contains(point.get())) {
                is_in_test_against_area = true;
                break;
            }
        }
        if (is_in_test_against_area) {
            continue; //we do not want this tree
        }

        //test against city blocks
        bool city_block_found {false};
        for (auto city_block : test_city_blocks) {
            if (city_block->GetGeometry()->contains(point.get())) {
                if (not TestIsCoordInBuilding(point.get(), city_block, Parameters::Get().TREES_RADIUS_TEST, false, building_nodes)) {
                    if (not TestIsCoordInBuilding(point.get(), remaining_block, Parameters::Get().TREES_RADIUS_TEST, false, building_nodes)) {
                        auto tree = std::make_shared<Tree>(geos::geom::Coordinate(random_coord->x, random_coord->y),
                                                           tree_origin, tree_type);
                        tree->SetCityBlockId(city_block->GetOSMId());
                        if (tree->AssignGridIndex(grid_index_boxes)) {
                            potential_trees.push_back(std::move(tree));
                        }
                    }
                }
                city_block_found = true;
                break;
            }
        }
        if (not city_block_found) {
            if (remaining_block->GetGeometry()->contains(point.get())) {
                if (not TestIsCoordInBuilding(point.get(), remaining_block, Parameters::Get().TREES_RADIUS_TEST, false, building_nodes)) {
                    auto tree = std::make_shared<Tree>(geos::geom::Coordinate(random_coord->x, random_coord->y),
                                                       tree_origin, tree_type);
                    tree->SetCityBlockId(remaining_block->GetOSMId());
                    if (tree->AssignGridIndex(grid_index_boxes)) {
                        potential_trees.push_back(std::move(tree));
                    }
                }
            }
        }
    }
}

/*! \brief Place additional trees based on specific land-use (not woods) in urban areas - aka./e.g. tree_areas.
 *
 * If it is a park, then a check is made against manually mapped trees - such that we do not add extra trees
 * if it is probable that the whole area was mapped manually and therefore should not be extended.
 * @param tree_areas
 * @param city_blocks
 */
void ProcessOSMTreesInTreeAreas(const std::vector<std::shared_ptr<geos::geom::Geometry>> &tree_areas,
                                const std::vector<std::shared_ptr<geos::geom::Geometry>> &test_against_areas,
                                std::vector<std::shared_ptr<Tree>> &existing_trees,
                                const std::set<CityBlock*> &city_blocks,
                                CityBlock* remaining_block,
                                std::map<osm_id_t, std::unique_ptr<std::vector<std::shared_ptr<Tree>>>> &trees_in_city_block_map,
                                const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                                const node_map_t &building_nodes,
                                const TreeOrigin tree_origin) {
    std::vector<std::shared_ptr<Tree>> potential_trees {};
    const auto mapped_factor = pow(Parameters::Get().TREES_DIST_BETWEEN_TREES_PARK_MAPPED, 2);
    std::vector<geos::geom::Point*> tree_points_to_check {};
    if (tree_origin == TreeOrigin::park) {
        for (const auto &tree : existing_trees) {
            if (tree->IsMappedTreeOrigin()) {
                tree_points_to_check.push_back(tree->GetPointPtr());
            }
        }
    }
    for (auto &my_geometry : tree_areas) {
        auto prep_tree_area_geom = std::make_unique<geos::geom::prep::PreparedPolygon>(my_geometry.get());
        std::vector<std::shared_ptr<geos::geom::Geometry>> verified_test_against_areas {};
        for (auto & test_area: test_against_areas) {
            if (prep_tree_area_geom->contains(test_area.get())) {
                verified_test_against_areas.push_back(test_area);
            }
        }
        bool place_trees = true;
        if (tree_origin == TreeOrigin::park) {
            place_trees = false;
            int trees_contained{0};
            // check whether any of the existing manually mapped trees is within the area.
            // if above a certain ratio, then most probably all trees where manually mapped
            for (const auto tree_point : tree_points_to_check) {
                if (prep_tree_area_geom->contains(tree_point)) {
                    trees_contained++;
                }
            }
            if (trees_contained == 0 or (my_geometry->getArea() / trees_contained) > mapped_factor) {
                place_trees = true;
            }
        }
        if (place_trees) {
            // we are good to try to place more trees
            const int default_distance = tree_origin == TreeOrigin::park ? Parameters::Get().TREES_DIST_BETWEEN_TREES_PARK : Parameters::Get().TREES_DIST_BETWEEN_TREES_SMALL_FOREST;
            const double keep_rate = tree_origin == TreeOrigin::park ? Parameters::Get().TREES_KEEP_RATE_TREES_PARK : Parameters::Get().TREES_KEEP_RATE_TREES_SMALL_FOREST;
            PlaceRandomTreesInArea(my_geometry->getEnvelopeInternal(),
                                   prep_tree_area_geom,
                                   verified_test_against_areas,
                                   city_blocks,
                                   remaining_block,
                                   grid_index_boxes,
                                   potential_trees,
                                   default_distance,
                                   keep_rate,
                                   tree_origin,
                                   building_nodes);
        }
    }
    ExtendTreesIfDistanceOkCityBlock(potential_trees, existing_trees, trees_in_city_block_map);
}

/*! \brief Place trees in open space - gardens for residential areas and other open space for e.g. commercial.
 *
 * We take a little risk here not to check against existing trees in tree areas (parks, forests, scrubs),
 * but the probability is not very large and all it would mean is the there are a few dense trees.
 * -> go with a faster process
 */
void ProcessOSMTreesInGardens(std::vector<std::shared_ptr<Tree>> &existing_trees,
                              const std::set<CityBlock*> &city_blocks,
                              const CityBlock * remaining_block,
                              std::map<osm_id_t, std::unique_ptr<std::vector<std::shared_ptr<Tree>>>> &trees_in_city_block_map,
                              const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                              const node_map_t &building_nodes) {
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    std::vector<std::shared_ptr<Tree>> potential_trees {};
    long counter {0};
    for (const auto city_block : city_blocks) {
        if (++counter % 100 == 0) {
            BOOST_LOG_TRIVIAL(info) << "Processed trees in gardens " << counter << " out of " << city_blocks.size() << " city blocks.";
        }
        if (city_block->GetBuildingZoneType() == BuildingZoneType::specialProcessing) {
            continue;  //no gardens for all those, who are not in a real city block
        }
        if (city_block->GetBuildingZoneType() == BuildingZoneType::aerodrome) {
            continue; //no trees on airport
        }
        auto default_distance = Parameters::Get().TREES_DIST_BETWEEN_TREES_GARDEN; //default - e.g. for farmyard and residential
        auto keep_rate = Parameters::Get().TREES_KEEP_RATE_TREES_GARDEN;
        if (city_block->GetBuildingZoneType() == BuildingZoneType::industrial or \
            city_block->GetBuildingZoneType() == BuildingZoneType::retail or \
            city_block->GetBuildingZoneType() == BuildingZoneType::commercial) {
            default_distance = Parameters::Get().TREES_DIST_BETWEEN_TREES_LOT;
            keep_rate = Parameters::Get().TREES_KEEP_RATE_TREES_LOT;
        }
        if (city_block->GetSettlementType() == SettlementType::centre) {
            keep_rate = Parameters::Get().TREES_KEEP_RATE_TREES_GARDEN_CENTRE;
        } else if (city_block->GetSettlementType() == SettlementType::block) {
            keep_rate = Parameters::Get().TREES_KEEP_RATE_TREES_GARDEN_BLOCK;
        }
        auto city_block_prep_geom = std::make_unique<geos::geom::prep::PreparedPolygon>(city_block->GetGeometry());
        std::vector<std::unique_ptr<geos::geom::Coordinate>> random_coords = GenerateRandomTreePointsInArea(city_block->GetGeometry()->getEnvelopeInternal(),
                                                                                                            city_block_prep_geom,
                                                                                                            default_distance,
                                                                                                            keep_rate);
        for (const auto &random_coord : random_coords) {
            auto coord = geos::geom::Coordinate(random_coord->x, random_coord->y);
            const auto point = factory->createPoint(coord);
            if (not TestIsCoordInBuilding(point.get(), city_block, Parameters::Get().TREES_RADIUS_TEST, true, building_nodes)) {
                if (not TestIsCoordInBuilding(point.get(), remaining_block, Parameters::Get().TREES_RADIUS_TEST, true, building_nodes)) {
                    auto tree = std::make_shared<Tree>(geos::geom::Coordinate(random_coord->x, random_coord->y),
                                                       TreeOrigin::garden, TreeType::garden_vegetation);
                    tree->SetCityBlockId(city_block->GetOSMId());
                    if (tree->AssignGridIndex(grid_index_boxes)) {
                        potential_trees.push_back(std::move(tree));
                    }
                }
            }
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Now check distance to existing trees";
    ExtendTreesIfDistanceOkCityBlock(potential_trees, existing_trees, trees_in_city_block_map);
}

/*! Maps from a TreeType to a FlightGear material type to be used for the tree.
 *
 * The mapping should be provided for all TreeTypes.
 *
 * @param tree_type
 * @return the FlightGear material name to be used for this tree type
 */
std::string MapTreeTypeToMaterial(const TreeType tree_type) {
    if (tree_type == TreeType::significant_trees) {
        return "SignificantTrees";
    }
    return "GardenVegetation"; // TreeType::garden_vegetation
}

void WriteTreesList(const std::vector<std::shared_ptr<Tree>> &trees, TreeType tree_type,
                    std::unique_ptr<STGManager> &stg_manager,
                    const std::string &tile_index,
                    ElevationProber &elev_prober) {
    std::vector<std::string> lines {};
    for (auto & tree : trees) {
        if (tree->GetTreeType() == tree_type) {
            std::stringstream stream;
            stream << std::fixed << std::setprecision(5);
            stream << tree->GetY()*-1 << " " << tree->GetX() << " ";
            double probed_elev = elev_prober.Probe(LonLat(tree->GetX(), tree->GetY()), false);
            probed_elev -= CalcHorizonElev(tree->GetY(), tree->GetX());  // the earth is round
            stream << probed_elev;
            lines.emplace_back(stream.str());
        }
    }
    if (lines.empty()) {
        return; //nothing to do
    }
    auto material = MapTreeTypeToMaterial(tree_type);
    std::stringstream file_name;
    file_name << tile_index << "_";
    file_name << material << "_tree_list.txt";
    stg_manager->AddTreeList(file_name.str(), material);
    std::ofstream tree_list_file {stg_manager->GetSTGDirectoryPath().append(file_name.str()), std::ios::out};
    for (auto p = lines.begin(); p != lines.end(); ++p) {
        tree_list_file << *p;
        if (p != lines.end() -1) { // FGFS complains with a WARNING if a file contains an empty last line
            tree_list_file << std::endl;
        }
    }
    BOOST_LOG_TRIVIAL(info) << "Total number of shader trees written to a tree_list " << material << ": " << lines.size();
}

std::vector<std::shared_ptr<Tree>> RemoveTreesFromBlockedAreas(const std::vector<std::shared_ptr<Tree>> &trees,
                                                               const std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> &natural_waters,
                                                               const std::map<osm_id_t, std::unique_ptr<Highway>> &highways,
                                                               const std::map<osm_id_t, std::unique_ptr<RailwayLine>> &railways,
                                                               const OSMDataReaderOverpass &osm_reader,
                                                               const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {

    std::vector<std::shared_ptr<Tree>> final_trees {};
    const auto blocked_spaces = ProcessOpenSpaces(osm_reader, grid_index_boxes, true);
    constexpr int cap_size = 4;
    constexpr int end_cap_style = geos::operation::buffer::BufferParameters::CAP_FLAT;
    std::map<osm_id_t, std::shared_ptr<GridIndexedGeometry>> grid_highways;
    for (const auto &[osm_id, highway] : highways){
        auto buffer = highway->GetWidth() / 2;
        if (highway->IsWayNeedingFence()) {
            buffer += Parameters::Get().TREES_LARGE_TRANSPORT_BUFFER;
        } else {
            buffer += Parameters::Get().TREES_BLOCKED_AREA_DEFAULT_BUFFER;
        }
        auto gig = std::make_shared<GridIndexedGeometry>(highway->GetGeometry()->buffer(buffer, cap_size, end_cap_style));
        gig->AssignGridIndices(grid_index_boxes);
        if (gig->HasGridIndices()) {
            grid_highways[osm_id] = std::move(gig);
        }
    }
    std::map<osm_id_t, std::shared_ptr<GridIndexedGeometry>> grid_railways;
    for (const auto &[osm_id, railway_line] : railways){
        const auto buffer = railway_line->GetWidth() / 2 + Parameters::Get().TREES_LARGE_TRANSPORT_BUFFER;
        auto gig = std::make_shared<GridIndexedGeometry>(railway_line->GetGeometry()->buffer(buffer, cap_size, end_cap_style));
        gig->AssignGridIndices(grid_index_boxes);
        if (gig->HasGridIndices()) {
            grid_railways[osm_id] = std::move(gig);
        }
    }

    long counter {0};
    for (auto & tree : trees) {
        if (++counter % 10000 == 0) {
            BOOST_LOG_TRIVIAL(info) << "Checked trees to maybe remove " << counter << " out of " << trees.size() << " trees.";
        }
        if (tree->IsMappedTreeOrigin()) {
            continue; // we respect what has been mapped, even though it might be in a blocked space
        }
        //natural waters
        bool keep {true};
        for (auto & gipp : natural_waters) {
            if (GridIndexed::HaveCommonIndices(tree->GetGridIndices(), gipp->GetGridIndices())) {
                if (gipp->ContainsOther(tree->GetPointPtr())) {
                    keep = false;
                    break;
                }
            }
        }
        //blocked spaces
        if (keep) {
            for (auto &it : blocked_spaces) {
                if (GridIndexed::HaveCommonIndices(tree->GetGridIndices(), it.second->GetGridIndices())) {
                    if (it.second->ContainsOther(tree->GetPointPtr())) {
                        keep = false;
                        break;
                    }
                }
            }
        }
        //highways
        if (keep) {
            for (auto &it : grid_highways) {
                if (GridIndexed::HaveCommonIndices(tree->GetGridIndices(), it.second->GetGridIndices())) {
                    if (it.second->ContainsOther(tree->GetPointPtr())) {
                        keep = false;
                        break;
                    }
                }
            }
        }
        //railways
        if (keep) {
            for (auto &it : grid_railways) {
                if (GridIndexed::HaveCommonIndices(tree->GetGridIndices(), it.second->GetGridIndices())) {
                    if (it.second->ContainsOther(tree->GetPointPtr())) {
                        keep = false;
                        break;
                    }
                }
            }
        }
        if (keep) {
            final_trees.push_back(tree);
        }
    }
    return final_trees;
}

void ProcessTrees(std::vector<std::shared_ptr<Building>> &buildings, OSMDataReaderOverpass &osm_reader,
                  const std::vector<std::unique_ptr<GridIndexedPreparedPolygon>> &natural_waters,
                  const std::map<osm_id_t, std::unique_ptr<Highway>> &highways,
                  const std::map<osm_id_t, std::unique_ptr<RailwayLine>> &railways,
                  const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                  const node_map_t &building_nodes) {
    BOOST_LOG_TRIVIAL(info) << "Processing trees";

    // create a special CityBlock that handles those buildings, which do not have a CityBlock assigned
    auto [min_coord, max_coord] = TileHandler::Get()->GetTileExtentLocal();
    std::unique_ptr<geos::geom::Polygon> box = MakeLocalBox(min_coord.x, min_coord.y, max_coord.x, max_coord.y);
    auto remaining_block = new CityBlock(REMAINING_CITY_BLOCK_ID, std::move(box), BuildingZoneType::specialProcessing);

    std::set<CityBlock*> city_blocks = PrepareCityBlocks(remaining_block, buildings);
    std::map<osm_id_t, std::unique_ptr<std::vector<std::shared_ptr<Tree>>>> trees_in_city_block_map {};
    for (auto city_block : city_blocks) {
        trees_in_city_block_map[city_block->GetOSMId()] = std::make_unique<std::vector<std::shared_ptr<Tree>>>();
    }
    trees_in_city_block_map[remaining_block->GetOSMId()] = std::make_unique<std::vector<std::shared_ptr<Tree>>>();

    std::vector<std::shared_ptr<Tree>> trees = ProcessOSMTreeNodes(osm_reader, grid_index_boxes);
    BOOST_LOG_TRIVIAL(info) << "Number of manually mapped trees found: " << trees.size();

    ProcessOSMTreeRows(trees, osm_reader, grid_index_boxes);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after trees in rows etc.: " << trees.size();
    ProcessOSMTreeLines(trees, osm_reader, grid_index_boxes);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after trees in lines etc.: " << trees.size();

    AssignTreesToCityBlocks(trees, city_blocks, remaining_block, trees_in_city_block_map);

    std::vector<std::shared_ptr<geos::geom::Geometry>> small_forests = ProcessTreeArea(osm_reader, TreeOrigin::small_forest);
    BOOST_LOG_TRIVIAL(info) << "Number of small forests used: " << small_forests.size();

    std::vector<std::shared_ptr<geos::geom::Geometry>> scrubs = ProcessTreeArea(osm_reader, TreeOrigin::scrub);
    BOOST_LOG_TRIVIAL(info) << "Number of scrubs used: " << scrubs.size();

    std::vector<std::shared_ptr<geos::geom::Geometry>> parks = ProcessTreeArea(osm_reader, TreeOrigin::park);
    BOOST_LOG_TRIVIAL(info) << "Number of park areas used: " << parks.size();

    std::vector<std::shared_ptr<geos::geom::Geometry>> test_against_areas {};

    //we take a bit of risk here that the underlying land-use data in the FG scenery also has a forest here
    //but because these are small forests the chance (e.g. for CORINE data) is small. And in the end we would
    //just have a dense forest, if they overlap
    ProcessOSMTreesInTreeAreas(small_forests, test_against_areas, trees, city_blocks, remaining_block, trees_in_city_block_map, grid_index_boxes, building_nodes, TreeOrigin::small_forest);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after placing in small forests: " << trees.size();

    ProcessOSMTreesInTreeAreas(scrubs, test_against_areas, trees, city_blocks, remaining_block, trees_in_city_block_map, grid_index_boxes, building_nodes, TreeOrigin::scrub);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after placing in scrubs: " << trees.size();

    //we only test against other tree areas for park, because a park in OSM can have mapped other stuff within it
    //e.g. a forest or scrub.
    test_against_areas.insert(test_against_areas.end(), small_forests.begin(), small_forests.end());
    test_against_areas.insert(test_against_areas.end(), scrubs.begin(), scrubs.end());

    ProcessOSMTreesInTreeAreas(parks, test_against_areas, trees, city_blocks, remaining_block, trees_in_city_block_map, grid_index_boxes, building_nodes, TreeOrigin::park);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after placing in parks: " << trees.size();

    ProcessOSMTreesInGardens(trees, city_blocks, remaining_block, trees_in_city_block_map, grid_index_boxes, building_nodes);
    BOOST_LOG_TRIVIAL(info) << "Total number of trees after placing in gardens: " << trees.size();

    std::vector<std::shared_ptr<Tree>> final_trees = RemoveTreesFromBlockedAreas(trees, natural_waters, highways, railways, osm_reader, grid_index_boxes);
    BOOST_LOG_TRIVIAL(info) << "Reduced number for trees due to blocked areas from " << trees.size() << " to " << final_trees.size() << ".";

    // =========== Do the plotting =============================================
    if (Parameters::Get().DEBUG_PLOT_TREES) {
        DrawTrees(buildings, final_trees);
    }

    // =========== Write to scenery folders ====================================
    ElevationProber ep {};

    auto stg_manager = std::make_unique<STGManager>(SceneryType::trees, TileHandler::Get()->GetTileCentre());
    WriteTreesList(final_trees, TreeType::significant_trees, stg_manager,TileHandler::Get()->GetTileIndex(), ep);
    WriteTreesList(final_trees, TreeType::garden_vegetation, stg_manager,TileHandler::Get()->GetTileIndex(), ep);
    stg_manager->Write(TileHandler::Get()->GetTileIndex());

    // =========== Clean up ====================================================
    delete remaining_block;
}
