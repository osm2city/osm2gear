// SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <geos/geom/Geometry.h>
#include <geos/geom/prep/PreparedPolygon.h>

#include <map>
#include <unordered_set>

#include "osm/osm_types.h"
#include "osm/data_reader_overpass.h"
#include "enumerations.h"
#include "parameters.h"


class Building; // forward declaration instead of include "buildings.h" -> circular dependency

class GridIndexed {
private:
    static constexpr int outside_index {-99};
    std::unordered_set<int> grid_indices_ {};
public:
    GridIndexed() = default;
    void AddGridIndex(int index) {
        grid_indices_.emplace(index);
    }
    void AddGridIndexOutside() {
        grid_indices_.emplace(GridIndexed::outside_index);
    }
    std::unordered_set<int>& GetGridIndices() {
        return grid_indices_;
    }
    [[nodiscard]] bool HasGridIndices() const {
        return not grid_indices_.empty();
    }
    [[nodiscard]] bool IsOutsideOfGrid() const {
        return HasGridIndices() and grid_indices_.count(GridIndexed::outside_index) == 1;
    }
    /*! \brief Return true if at one index is in common
     */
    [[nodiscard]] static bool HaveCommonIndices(std::unordered_set<int>&, std::unordered_set<int>&);
};

class GridIndexedGeometry : public GridIndexed {
private:
    std::unique_ptr<geos::geom::Geometry> geometry_;
public:
    explicit GridIndexedGeometry(std::unique_ptr<geos::geom::Geometry> &&);
    geos::geom::Geometry* GetGeometry() const;
    bool ContainsOther(const geos::geom::Geometry* other) const;
    bool WithinOther(const geos::geom::Geometry* other) const;
    bool Intersects(const geos::geom::Geometry* other) const;
    bool IsDisjoint(const geos::geom::Geometry* other) const;
    void AssignGridIndices(const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
        for (auto &gib : grid_index_boxes) {
            if (not this->IsDisjoint(gib.second.get())) {
                this->AddGridIndex(gib.first);
            }
        }
    }
};

class GridIndexedPreparedPolygon : public GridIndexed {
private:
    std::unique_ptr<geos::geom::prep::PreparedPolygon> prepared_polygon_;
    double area_;
    std::unique_ptr<geos::geom::Geometry> geometry_;
public:
    explicit GridIndexedPreparedPolygon(std::unique_ptr<geos::geom::Geometry> &&);
    bool ContainsOther(const geos::geom::Geometry* other) const;
    bool Intersects(const geos::geom::Geometry* other) const;
    bool IsDisjoint(const geos::geom::Geometry* other) const;
    [[nodiscard]] double GetArea() const {
        return area_;
    }
    void AssignGridIndices(const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
        for (auto &gib : grid_index_boxes) {
            if (not this->IsDisjoint(gib.second.get())) {
                this->AddGridIndex(gib.first);
            }
        }
    }
};

class OSMFeature {
protected:
    const osm_id_t osm_id_;
    std::unique_ptr<geos::geom::Geometry> geometry_;

public:
    OSMFeature(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&);
    [[nodiscard]] geos::geom::Geometry* GetGeometry() const;
    void SetGeometry(std::unique_ptr<geos::geom::Geometry>);
    [[nodiscard]] osm_id_t GetOSMId() const {
        return osm_id_;
    }
    [[nodiscard]] std::unique_ptr<geos::geom::Geometry> CloneOfGeometry() const {
        return geometry_->clone();
    }
    [[nodiscard]] virtual bool IsDefinedType() const = 0;
};

class OSMFeatureLinear : public OSMFeature {
protected:
    bool has_embankment_or_cutting_;
    bool tunnel_;
public:
    OSMFeatureLinear(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *);
    [[nodiscard]] bool IsTunnel() const {
        return tunnel_;
    }
    [[nodiscard]] bool HasEmbankmentOrCutting() const {
        return has_embankment_or_cutting_;
    }
    [[nodiscard]] virtual double GetWidth() const = 0;
    [[nodiscard]] virtual bool UseForBuildingZoneSplit() const = 0;
};

class Highway : public OSMFeatureLinear {
private:
    std::optional<HighwayType> highway_type_;
    bool roundabout_;
    bool oneway_;
    int lanes_;
public:
    Highway(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *);
    [[nodiscard]] bool IsDefinedType() const override {
        return highway_type_.has_value();
    }
    [[nodiscard]] double GetWidth() const override;
    [[nodiscard]] bool UseForBuildingZoneSplit() const override;
    [[nodiscard]] bool PopulateBuildingsAlong() const;
    [[nodiscard]] bool IsResidentialWay() const {
        if (IsDefinedType()) {
            return highway_type_.value() == HighwayType::residential;
        }
        return false;
    }
    [[nodiscard]] bool IsWayNeedingFence() const {
        if (IsDefinedType() and (highway_type_.value() == HighwayType::motorway or
                                 highway_type_.value() == HighwayType::trunk or
                                 highway_type_.value() == HighwayType::one_way_large)) {
            return true;
        }
        return false;
    }
};

class Waterway : public OSMFeatureLinear {
private:
    std::optional<WaterwayType> waterway_type_;
public:
    Waterway(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *);
    [[nodiscard]] bool IsDefinedType() const override {
        return waterway_type_.has_value();
    }
    [[nodiscard]] double GetWidth() const override {
        return 10;
    }
    [[nodiscard]] bool UseForBuildingZoneSplit() const override {
        if (tunnel_) {
            return false;
        }
        return waterway_type_ == WaterwayType::large;
    }
};

class RailwayLine : public OSMFeatureLinear {
private:
    std::optional<RailwayLineType> rail_way_line_type_;
    int tracks_;
    bool service_spur_;
public:
    RailwayLine(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *);
    [[nodiscard]] bool IsDefinedType() const override {
        return rail_way_line_type_.has_value();
    }
    [[nodiscard]] double GetWidth() const override;
    [[nodiscard]] bool UseForBuildingZoneSplit() const override;
};

class Place : public OSMFeature {
private:
    std::optional<PlaceType> place_type_;
    bool is_point_;
    std::optional<int> population_;
    void ParseTagsForPopulation(const tags_t*);
    [[nodiscard]] double CalculateCircleRadius(long population, double power) const;
public:
    Place(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, const tags_t *, bool is_point);
    void TransformToPoint();
    [[nodiscard]] bool IsDefinedType() const override {
        return place_type_.has_value();
    }
    [[nodiscard]] bool IsPoint() const {
        return is_point_;
    }
    [[nodiscard]] PlaceType GetPlaceType() const {
        return place_type_.value();
    }
    [[nodiscard]]
    std::tuple<std::unique_ptr<GridIndexedPreparedPolygon>, std::unique_ptr<GridIndexedPreparedPolygon>, std::unique_ptr<GridIndexedPreparedPolygon>>
    CreatePreparedPlacePolygons() const;
};

class Zone : public OSMFeature {
protected:
    ZoneType zone_type_;
    BuildingZoneType building_zone_type_;
    SettlementType settlement_type_ = SettlementType::rural;
    std::vector<std::shared_ptr<Building>> osm_buildings_ {};
public:
    Zone(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, ZoneType, BuildingZoneType);
    [[nodiscard]] ZoneType GetZoneType() const {
        return zone_type_;
    }
    [[nodiscard]] BuildingZoneType GetBuildingZoneType() const {
        return building_zone_type_;
    }
    [[nodiscard]] SettlementType GetSettlementType() const {
        return settlement_type_;
    }
    void RelateBuilding(std::shared_ptr<Building> &building);
    void UnRelateBuilding(const std::shared_ptr<Building> &building);
    [[nodiscard]] std::vector<std::shared_ptr<Building>> GetOSMBuildings() const {
        return osm_buildings_;
    }
    [[nodiscard]] bool HasOSMBuildings() const {
        return !this->osm_buildings_.empty();
    }
};

class GenBuilding;

class CityBlock : public Zone {
private:
    std::unique_ptr<geos::geom::prep::PreparedPolygon> prep_geom_;
    void MakePreparedGeometry();
    bool used_for_building_generation_ {false};
    int building_gen_attempts_{0};
    // used to make somewhat uniform buildings within CityBlock
    BuildingType building_gen_building_type_ {BuildingType::yes};
    double building_gen_default_width_ {}; // only used for terrace and apartment
    double building_gen_default_depth_ {}; // used for terrace, apartment and attached
    int building_gen_terrace_number_attached_ {}; // number of buildings in terrace attached to each other
public:
    CityBlock(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, BuildingZoneType);
    [[nodiscard]] bool IsDefinedType() const override {
        return true;
    }
    void SetSettlementType(SettlementType settlement_type) {
        settlement_type_ = settlement_type;
        //FIXME: self.__building_levels = bl.calc_levels_for_settlement_type(value, enu.BuildingClass.residential)
    }
    void ClearPreparedGeometry() {
        prep_geom_ = nullptr;
    }
    bool ContainsProperly(geos::geom::Geometry * other) {
        if (prep_geom_ == nullptr) {
            MakePreparedGeometry();
        }
        return prep_geom_->containsProperly(other);
    }
    bool Intersects(geos::geom::Geometry * other) {
        if (prep_geom_ == nullptr) {
            MakePreparedGeometry();
        }
        return prep_geom_->intersects(other);
    }
    [[nodiscard]] std::unique_ptr<GenBuilding> CreateTypicalGenBuilding();
    void SetUsedForBuildingGeneration() {
        used_for_building_generation_ = true;
    }
    [[nodiscard]] bool IsUsedForBuildingGeneration() const {
        return used_for_building_generation_;
    }
    [[nodiscard]] bool IsBuildingGenAttached() const {
        return building_gen_building_type_ == BuildingType::attached or building_gen_building_type_ == BuildingType::terrace;
    }
};

class BuildingZone : public GridIndexed, public Zone {
private:
    std::optional<BuildingZoneOrigin> building_zone_origin_;
    std::vector<std::unique_ptr<CityBlock>> city_blocks_ {};
    std::vector<std::shared_ptr<GridIndexedGeometry>> blocked_areas_ {};

public:
    BuildingZone(osm_id_t, std::unique_ptr<geos::geom::Geometry> &&, BuildingZoneType, std::optional<BuildingZoneOrigin>);
    bool ContainsOrIntersects(const geos::geom::Geometry *);
    /*! Geometry calculation of difference such that the result is the other's geometry reduced by this zone's geometry.
     *
     * @param other_geometry
     * @return
     */
    std::unique_ptr<geos::geom::Geometry> NonIntersectingDifference(const geos::geom::Geometry *other_geometry);
    std::shared_ptr<Building> PopOSMBuilding() {
        if (osm_buildings_.empty()) {
            return nullptr;
        }
        auto pointer = osm_buildings_.back();
        osm_buildings_.pop_back();
        return pointer;
    }
    void ReassignOSMBuildingsToCityBlocks();
    [[nodiscard]] bool IsDefinedType() const override {
        return true;
    }
    [[nodiscard]] bool IsOfBuildingZoneType(BuildingZoneType bz_type) const {
        if (this->IsDefinedType() and building_zone_type_ == bz_type) {
            return true;
        }
        return false;
    }
    [[nodiscard]] bool IsAerodrome() const {
        return this->IsOfBuildingZoneType(BuildingZoneType::aerodrome);
    }
    [[nodiscard]] bool IsFarmyard() const {
        return this->IsOfBuildingZoneType(BuildingZoneType::farmyard);
    }
    [[nodiscard]] bool IsMilitary() const {
        return this->IsOfBuildingZoneType(BuildingZoneType::military);
    }
    /** \brief Whether this zone was generated from buildings.
     *
     * NB: there are also building zones generated from apt_dat, but they are
     * treated as if these aerodrome zones were from OSM.
     *
     * @return true if the origin is from_buildings.
     */
    [[nodiscard]] bool IsGenerated() const {
        return (building_zone_origin_ == BuildingZoneOrigin::from_buildings or building_zone_origin_ == BuildingZoneOrigin::from_residential_streets);
    }
    [[nodiscard]] std::optional<BuildingZoneOrigin> GetBuildingZoneOrigin() const {
        return building_zone_origin_;
    }
    void SetMaxSettlementType(SettlementType new_type) {
        if (new_type > settlement_type_) {
            settlement_type_ = new_type;
        }
    }
    void SetSettlementTypeCityBlocks(SettlementType, std::unique_ptr<GridIndexedPreparedPolygon>&);
    void AddCityBlock(std::unique_ptr<CityBlock> block) {
        city_blocks_.push_back(std::move(block));
    }
    void ResetCityBlocks() {
        city_blocks_.clear();
    }
    std::vector<std::unique_ptr<CityBlock>> *GetCityBlocks() {
        return &city_blocks_;
    }

    void GuessBuildingZoneType(const std::vector<std::unique_ptr<Place>> &farm_places);

    /** \brief Check whether this zone should be used for generation of buildings.
     * If the zone is an aerodrome or the zone itself was generated, then do not use.
     *
     * @return true if at least one related city block does not have a building.
     */
    [[nodiscard]] bool UseForBuildingGeneration() const;

    void AssignBlockedArea(const std::shared_ptr<GridIndexedGeometry>& blocked_area) {
        blocked_areas_.push_back(blocked_area);
    }
    void ProcessOwnBuildingsAsBlockedAreas(const std::map<int, std::unique_ptr<geos::geom::Polygon>> &);

    /*! \brief If a CityBlock has at least one building, then we do not want to generate buildings there.
     */
    void ProcessCityBlocksWithBuildingsAsBlockedAreas(const std::map<int, std::unique_ptr<geos::geom::Polygon>> &);

    /*! \brief Check whether this building zone has at least one city block without buildings.
     * Also checks that if the zone does not have existing buildings, that it is large enough for
     * adding generated buildings.
     */
    [[nodiscard]] bool HasCityBlocksWithoutBuildings() const {
        for (auto & city_block : city_blocks_) {
            if (not city_block->HasOSMBuildings() and
                city_block->GetGeometry()->getArea() > Parameters::Get().OWBB_MIN_CITY_BLOCK_AREA_FOR_GENERATION) {
                return true;
            }
        }
        return false;
    }
    /*! \brief For a x/y local coordinate find the CityBlock containing the point - if any
     */
    std::optional<CityBlock*> FindCityBlockForPoint(double x, double y) const;

    [[nodiscard]] bool HasIntersectingBlockedArea(std::shared_ptr<GridIndexedGeometry> &) const;
};

/*! \brief Class to handle a Highway along which Buildings can be generated.
 *
 */
class GenBuildingWay {
   private:
    //NB: there can be more than one GenBuildingWay with the same osm_id from the original way
    osm_id_t osm_id_;
    double width_;
    std::unique_ptr<geos::geom::LineString> line_string_;
    BuildingZone* building_zone_;
   public:
    GenBuildingWay(std::unique_ptr<geos::geom::LineString> &&, double, BuildingZone *, osm_id_t);
    [[nodiscard]] double GetWidth() const {
        return width_;
    }
    [[nodiscard]] BuildingZone* GetBuilingZone() {
        return building_zone_;
    }
    [[nodiscard]] geos::geom::LineString* GetLineString() {
        return line_string_.get();
    }
};

class GenBuilding : public Element {
   private:
    double width_;
    double depth_;
    double buffer_side_;
    double buffer_front_;
    double buffer_back_;
    bool l_shaped_;
    /*! \brief The absolute minimal distance tolerable, e.g. in a curve at the edges of the lot.
     * The front_buffer will be used to place the building relative to the middle of the house along the street.
     * The min_front_buffer is a bit smaller and allows in a concave curve that the building still is placed,
     * because the buffer in the front of the house is only min_front_buffer and therefore smaller.
     */
    [[nodiscard]] double GetMinBufferFront() const {
        return sqrt(buffer_front_);
    }
    [[nodiscard]] std::vector<geos::geom::Coordinate>
    CreateCoordinatesForRectangle(double way_width, const geos::geom::Coordinate &origin_point, double angle_deg,
                                  bool with_buffer) const;
    [[nodiscard]] std::vector<geos::geom::Coordinate>
    CreateCoordinatesForLShape(double way_width, const geos::geom::Coordinate &origin_point, double angle_deg) const;
   public:
    GenBuilding(double width, double depth, double buffer_side, double buffer_front, double buffer_back, osm_id_t, tags_t *, bool);
    [[nodiscard]] std::shared_ptr<GridIndexedGeometry> CreatePerimeterRectangle(double way_width, const geos::geom::Coordinate &origin_point, double angle_deg,
                                                                                bool with_buffer, const std::map<int, std::unique_ptr<geos::geom::Polygon>> &) const;
    [[nodiscard]] double GetWidthWithBuffer() const {
        return width_ + 2 * buffer_side_;
    }
    /*! \brief Create a final Building out of this GenBuilding enhancing the building nodes as basis.
     */
    std::shared_ptr<Building> TransformToBuilding(double way_width, const geos::geom::Coordinate &origin_point, double angle_deg,
                                                  node_map_t &) const;
    [[nodiscard]] bool IsAttached() const {
        return buffer_side_ < 0.01;
    }
};

/*! \brief Fetches "open" space areas from OSM - to be used to block adding buildings, trees etc.
 * Trees are a bit relaxed to where they can be set compared to buildings.
 */
std::map<osm_id_t , std::shared_ptr<GridIndexedGeometry>> ProcessOpenSpaces(const OSMDataReaderOverpass &osm_reader,
                                                                            const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                                                                            bool use_for_trees);