// SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#include "models.h"

#include <geos/geom/Geometry.h>
#include <geos/geom/prep/PreparedPolygon.h>

#include <cassert>
#include <cmath>
#include <memory>

#include "../osm/constants.h"
#include "boost/log/trivial.hpp"
#include "buildings.h"
#include "wikidata_io.h"

bool GridIndexed::HaveCommonIndices(std::unordered_set<int>& set1, std::unordered_set<int>& set2) {
    assert(not set1.empty()); //otherwise we forgot to assign grid_indices in previous code
    assert(not set2.empty()); //ditto
    // now make the real test
    for (const auto first : set1) {
        for (const auto second : set2) {
            if (first == second) {
                return true;
            }
        }
    }
    return false;
}

GridIndexedGeometry::GridIndexedGeometry(std::unique_ptr<geos::geom::Geometry> && geometry): geometry_ {std::move(geometry)} {}

geos::geom::Geometry* GridIndexedGeometry::GetGeometry() const {
    geos::geom::Geometry* ptr = geometry_.get();
    return ptr;
}

bool GridIndexedGeometry::ContainsOther(const geos::geom::Geometry* other) const {
    return geometry_->contains(other);
}

bool GridIndexedGeometry::WithinOther(const geos::geom::Geometry* other) const {
    return geometry_->within(other);
}

bool GridIndexedGeometry::Intersects(const geos::geom::Geometry* other) const {
    return geometry_->intersects(other);
}

bool GridIndexedGeometry::IsDisjoint(const geos::geom::Geometry* other) const {
    return geometry_ -> disjoint(other);
}

GridIndexedPreparedPolygon::GridIndexedPreparedPolygon(std::unique_ptr<geos::geom::Geometry> && geometry): GridIndexed() {
    area_ = geometry->getArea();
    geometry_ = std::move(geometry);
    prepared_polygon_ = std::make_unique<geos::geom::prep::PreparedPolygon>(geometry_.get());
}

bool GridIndexedPreparedPolygon::ContainsOther(const geos::geom::Geometry* other) const {
    return prepared_polygon_->contains(other);
}

bool GridIndexedPreparedPolygon::Intersects(const geos::geom::Geometry* other) const {
    return prepared_polygon_->intersects(other);
}

bool GridIndexedPreparedPolygon::IsDisjoint(const geos::geom::Geometry* other) const {
    return prepared_polygon_->disjoint(other);
}

OSMFeature::OSMFeature(const osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry): osm_id_ {osm_id}, geometry_ {std::move(geometry)} {}

geos::geom::Geometry* OSMFeature::GetGeometry() const {
    return geometry_.get();
}

void OSMFeature::SetGeometry(std::unique_ptr<geos::geom::Geometry> new_geometry) {
    geometry_ = std::move(new_geometry);
}

OSMFeatureLinear::OSMFeatureLinear(const osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags) : OSMFeature(osm_id, std::move(geometry)){
    tunnel_ = ParseIsTunnel(tags);
    has_embankment_or_cutting_ = ParseHasEmbankmentOrCutting(tags);
}

Highway::Highway(const osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags): OSMFeatureLinear(osm_id, std::move(geometry), tags) {
    highway_type_ = ParseTagsForHighwayType(tags);
    roundabout_ = ParseIsRoundabout(tags);
    oneway_ = ParseIsOneWay(tags, highway_type_ == HighwayType::motorway);
    lanes_ = ParseLanes(tags, 1, highway_type_ == HighwayType::motorway);
}


double Highway::GetWidth() const {
    double my_width;
    if (highway_type_ == HighwayType::service or highway_type_ == HighwayType::residential or highway_type_ == HighwayType::living_street or highway_type_ == HighwayType::pedestrian) {
        my_width = 5.;
    } else if (highway_type_ == HighwayType::road or highway_type_ == HighwayType::unclassified or highway_type_ == HighwayType::tertiary) {
        my_width = 6.;
    } else if (highway_type_ == HighwayType::secondary or highway_type_ == HighwayType::primary or highway_type_ == HighwayType::trunk) {
        my_width = 7.;
    } else if (highway_type_ == HighwayType::one_way_multi_lane) {
        my_width = 3.5; // will be enhanced with more lanes below
    } else if (highway_type_ == HighwayType::one_way_large) {
        my_width = 4.;
    } else if (highway_type_ == HighwayType::one_way_normal) {
        my_width = 3.;
    } else { //HighwayType::slow
        my_width = 2.5;
    }
    if (highway_type_ == HighwayType::motorway) {
        my_width = 3.5 * lanes_;
        if (not oneway_) {
            my_width += 2 * 2. + 1.; //assuming limited hard shoulders, not much middle stuff
        } else {
            my_width += 2 * 3. + 2.; //hard shoulders, middle stuff
        }
    } else if (lanes_ > 1) {
        my_width += 0.8 * (lanes_ - 1) * my_width;
    }
    if (has_embankment_or_cutting_) {
        my_width += 4.;
    }
    return my_width;
}

bool Highway::UseForBuildingZoneSplit() const {
    if (tunnel_) {
        return false;
    }
    if (highway_type_ == HighwayType::motorway or highway_type_ == HighwayType::trunk or highway_type_ == HighwayType::primary) {
        return true;
    }
    return false;
}

bool Highway::PopulateBuildingsAlong() const {
    if (tunnel_) {
        return false;
    }
    if (highway_type_ == HighwayType::motorway or highway_type_ == HighwayType::trunk
        or highway_type_ == HighwayType::slow) { //pedestrian is ok, because might be in city centres
        return false;
    }
    if (GetGeometry()->getLength() < Parameters::Get().OWBB_MIN_STREET_LENGTH) {
        return false;
    }
    return true;
}

Waterway::Waterway(const osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags) : OSMFeatureLinear(osm_id, std::move(geometry), tags){
    waterway_type_ = ParseTagsForWaterwayType(tags);
}

RailwayLine::RailwayLine(const osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags) : OSMFeatureLinear(osm_id, std::move(geometry), tags){
    rail_way_line_type_ = ParseTagsForRailwayLineType(tags);
    tracks_ = ParseTracks(tags);
    service_spur_ = ParseIsServiceSpur(tags);
}

double RailwayLine::GetWidth() const {
    double my_width;
    if (rail_way_line_type_ == RailwayLineType::narrow_gauge or rail_way_line_type_ == RailwayLineType::tram or rail_way_line_type_ == RailwayLineType::funicular) {
        my_width = 5.0;
    } else if (rail_way_line_type_ == RailwayLineType::rail or rail_way_line_type_ == RailwayLineType::light_rail or rail_way_line_type_ == RailwayLineType::monorail or rail_way_line_type_ == RailwayLineType::subway) {
        my_width = 7.0;
    } else {
        my_width = 6.0;
    }
    if (tracks_ > 1) {
        my_width += 0.8 * (tracks_ - 1) * my_width;
    }
    if (has_embankment_or_cutting_) {
        my_width += 6.0;
    }
    return my_width;
}

bool RailwayLine::UseForBuildingZoneSplit() const {
    if (tunnel_ or service_spur_) {
        return false;
    }
    if (rail_way_line_type_ == RailwayLineType::rail or rail_way_line_type_ == RailwayLineType::light_rail or rail_way_line_type_ == RailwayLineType::subway) {
        return false;
    }
    return true;
}

Place::Place(const osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, const tags_t* tags, bool is_point) : OSMFeature(osm_id, std::move(geometry)){
    place_type_ = ParseTagsForPlaceType(tags);
    is_point_ = is_point;
    ParseTagsForPopulation(tags);
}

void Place::ParseTagsForPopulation(const tags_t* tags) {
    if (tags->contains(k_population)) {
        this->population_ = ParseInt(tags->at(k_population));
    }
    if (not population_.has_value() && tags->contains(k_wikidata)) {
        auto population_result = QueryPopulationWikidata(tags->at(k_wikidata));
        if (population_result.has_value()) {
            population_ = population_result.value();
        }
    }
}

void Place::TransformToPoint() {
    if (is_point_) {
        return;
    }
    if (place_type_ == PlaceType::farm) {
        BOOST_LOG_TRIVIAL(warning) << "Attempted to transform Place of type farm from area to point";
        return;
    }
    is_point_ = true;
    geometry_ = geometry_->getCentroid();
}

double Place::CalculateCircleRadius(const long population, const double power) const {
    double radius = pow((double)population, power);
    if (place_type_ == PlaceType::city) {
        radius *= Parameters::Get().OWBB_PLACE_RADIUS_FACTOR_CITY;
    } else {
        radius *= Parameters::Get().OWBB_PLACE_RADIUS_FACTOR_TOWN;
    }
    return radius;
}

std::tuple<std::unique_ptr<GridIndexedPreparedPolygon>, std::unique_ptr<GridIndexedPreparedPolygon>, std::unique_ptr<GridIndexedPreparedPolygon>>
Place::CreatePreparedPlacePolygons() const {
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    long population = population_.value_or(0);
    if (population == 0) {
        population = place_type_ == PlaceType::city ? Parameters::Get().OWBB_PLACE_POPULATION_DEFAULT_CITY : Parameters::Get().OWBB_PLACE_POPULATION_DEFAULT_TOWN;
    }
    std::unique_ptr<geos::geom::Geometry> centre_circle = factory->createPolygon();
    std::unique_ptr<geos::geom::Geometry> block_circle = factory->createPolygon();
    std::unique_ptr<geos::geom::Geometry> dense_circle;
    double radius;
    if (place_type_ == PlaceType::city) {
        radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_CENTRE);
        centre_circle = GetGeometry()->buffer(radius);
        radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_BLOCK);
        block_circle = GetGeometry()->buffer(radius);
        radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_DENSE);
        dense_circle = GetGeometry()->buffer(radius);
    } else {
        if (population_ > 0 and population > Parameters::Get().OWBB_PLACE_POPULATION_MIN_BLOCK) {
            radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_BLOCK);
            block_circle = GetGeometry()->buffer(radius);
        }
        radius = CalculateCircleRadius(population, Parameters::Get().OWBB_PLACE_RADIUS_EXPONENT_DENSE);
        dense_circle = GetGeometry()->buffer(radius);
    }
    return std::make_tuple(
            std::make_unique<GridIndexedPreparedPolygon>(std::move(centre_circle)),
            std::make_unique<GridIndexedPreparedPolygon>(std::move(block_circle)),
            std::make_unique<GridIndexedPreparedPolygon>(std::move(dense_circle)));
}

Zone::Zone(const osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, ZoneType zone_type, BuildingZoneType building_zone_type) : zone_type_ {zone_type}, building_zone_type_ {building_zone_type}, OSMFeature(osm_id, std::move(geometry)) {}

void Zone::RelateBuilding(std::shared_ptr<Building> &building) {
    this->osm_buildings_.push_back(building);
    building->SetZone(this);
}

void Zone::UnRelateBuilding(const std::shared_ptr<Building> &building) {
    auto res = find(osm_buildings_.begin(), osm_buildings_.end(), building);
    if (res != osm_buildings_.end()) {
        osm_buildings_.erase(res);
    }
}

CityBlock::CityBlock(const osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry, BuildingZoneType building_zone_type) : Zone(osm_id, std::move(geometry), ZoneType::city_block, building_zone_type) {}

void CityBlock::MakePreparedGeometry() {
    prep_geom_ = std::make_unique<geos::geom::prep::PreparedPolygon>(GetGeometry());
}

std::tuple<double, double, double, double, double> CreateTypicalIndustrialBuilding(double city_block_area) {
    const auto generator = TileHandler::Get()->GetRandomNumberGenerator();
    std::uniform_real_distribution<> zero_to_one(0.01, 1.0);
    double width, depth, buffer_side, buffer_front, buffer_back;
    bool use_large = false;
    int min_width, max_width, min_depth, max_depth;
    if (city_block_area > 5000) {
        if (zero_to_one(*generator) < Parameters::Get().OWBB_INDUSTRIAL_LARGE_SHARE) {
            use_large = true;
        }
    }
    if (use_large) {
        min_width = 20;
        max_width = 60;
        min_depth = 15;
        max_depth = 40;
    } else {
        min_width = 15;
        max_width = 30;
        min_depth = 10;
        max_depth = 25;
    }
    std::uniform_real_distribution<> dist_width(min_width, max_width);
    std::uniform_real_distribution<> dist_depth(min_depth, max_depth);
    std::uniform_real_distribution<> dist_buffer(Parameters::Get().OWBB_INDUSTRIAL_BUFFER_MIN, Parameters::Get().OWBB_INDUSTRIAL_BUFFER_MAX);
    width = dist_width(*generator);
    depth = dist_depth(*generator);
    buffer_side = dist_buffer(*generator);
    buffer_front = dist_buffer(*generator);
    buffer_back = dist_buffer(*generator);
    return std::make_tuple(width, depth, buffer_side, buffer_front, buffer_back);
}

std::unique_ptr<GenBuilding> CityBlock::CreateTypicalGenBuilding() {
    tags_t tags {};
    const auto generator = TileHandler::Get()->GetRandomNumberGenerator();
    double width, depth, buffer_side, buffer_front, buffer_back;
    bool l_shaped {false};

    std::uniform_real_distribution<> zero_to_one(0.01, 1.0);
    std::uniform_int_distribution<> one_to_hundred(1, 100);
    std::uniform_int_distribution<> zero_to_two(0, 2);
    std::uniform_int_distribution<> zero_to_four(0, 4);
    std::uniform_int_distribution<> zero_to_six(0, 6);

    if (this->building_zone_type_ == BuildingZoneType::residential) {
        if (building_gen_attempts_ == 0) {
            if (settlement_type_ == SettlementType::centre or settlement_type_ == SettlementType::block) {
                building_gen_building_type_ = BuildingType::attached;
            }
            auto ratio_array = Parameters::Get().OWBB_RESIDENTIAL_DENSE_TYPE_SHARE;
            if (settlement_type_ == SettlementType::periphery) {
                ratio_array = Parameters::Get().OWBB_RESIDENTIAL_PERIPHERY_TYPE_SHARE;
            } else if (settlement_type_ == SettlementType::rural) {
                ratio_array = Parameters::Get().OWBB_RESIDENTIAL_RURAL_TYPE_SHARE;
            }
            auto random_percentage = one_to_hundred(*generator);
            if (random_percentage < ratio_array[0]) {
                building_gen_building_type_ = BuildingType::detached;
            } else if (random_percentage < ratio_array[0] + ratio_array[1]) {
                building_gen_building_type_ = BuildingType::terrace;
                building_gen_default_width_ = Parameters::Get().OWBB_TERRACE_TYPICAL_WIDTH - 1 + zero_to_two(*generator);
                building_gen_default_depth_ = Parameters::Get().OWBB_TERRACE_TYPICAL_DEPTH - 1 + zero_to_two(*generator);
                building_gen_terrace_number_attached_ = Parameters::Get().OWBB_RESIDENTIAL_TERRACE_TYPICAL_NUMBER - 1 + zero_to_two(*generator);
            } else if (random_percentage < ratio_array[0] + ratio_array[1] + ratio_array[2]) {
                building_gen_building_type_ = BuildingType::apartments;
                building_gen_default_width_ = Parameters::Get().OWBB_APARTMENT_TYPICAL_WIDTH - 3 + zero_to_six(*generator);
                building_gen_default_depth_ = Parameters::Get().OWBB_APARTMENT_TYPICAL_DEPTH - 2 + zero_to_four(*generator);
            } else {
                building_gen_building_type_ = BuildingType::attached;
                // width is kept dynamic/random
                building_gen_default_depth_ = Parameters::Get().OWBB_ATTACHED_TYPICAL_DEPTH - 1 + zero_to_two(*generator);
            }
        } else if (building_gen_building_type_ == BuildingType::apartments
                   and building_gen_attempts_ > 80 / Parameters::Get().OWBB_STEP_DISTANCE
                   and not this->HasOSMBuildings()) {
            // let us give up trying to build a detached building with flats (apartments)
            // the virtual distance of 80 metres is chosen such that there is some chance to build such a building
            // 40 metres for such a building is already large
            building_gen_building_type_ = BuildingType::detached;
        }
        ++building_gen_attempts_;

        if (building_gen_building_type_ == BuildingType::detached) {
            tags[k_building] = v_detached;
            width = Parameters::Get().OWBB_DETACHED_TYPICAL_SIDE -2 + zero_to_four(*generator);
            const double max_depth = Parameters::Get().OWBB_DETACHED_MAX_AREA / width;
            depth = std::min(max_depth, Parameters::Get().OWBB_DETACHED_TYPICAL_SIDE -2 + zero_to_four(*generator));
            if (zero_to_one(*generator) < Parameters::Get().OWBB_DETACHED_SHARE_L_SHAPED) {
                l_shaped = true;
                // increase size as floor area gets smaller in L-shaped
                width += 2.0;
                depth += 2.0;
            }
        } else if (building_gen_building_type_ == BuildingType::terrace) {
            tags[k_building] = v_terrace;
            width = building_gen_default_width_;
            depth = building_gen_default_depth_;
        } else if (building_gen_building_type_ == BuildingType::apartments) {
            tags[k_building] = v_apartments;
            width = building_gen_default_width_;
            depth = building_gen_default_depth_;
        } else { // building_gen_building_type_ == BuildingType::attached
            tags[k_building] = v_attached;
            width = Parameters::Get().OWBB_ATTACHED_TYPICAL_WIDTH - 3 + zero_to_six(*generator);
            depth = building_gen_default_depth_;
        }

        //finally calculate the buffers
        buffer_side = std::sqrt(width) * Parameters::Get().OWBB_RESIDENTIAL_SIDE_BUFFER_FACTOR_PERIPHERY;
        buffer_back = std::sqrt(depth) * Parameters::Get().OWBB_RESIDENTIAL_BACK_BUFFER_FACTOR_PERIPHERY;
        buffer_front = std::sqrt(width) * Parameters::Get().OWBB_RESIDENTIAL_FRONT_BUFFER_FACTOR_PERIPHERY;
        if (building_gen_building_type_ == BuildingType::apartments || building_gen_building_type_ == BuildingType::detached) {
            if (settlement_type_ == SettlementType::dense || settlement_type_ == SettlementType::block || settlement_type_ == SettlementType::centre) {
                buffer_side = std::sqrt(width) * Parameters::Get().OWBB_RESIDENTIAL_SIDE_BUFFER_FACTOR_DENSE;
            }
        } else if (building_gen_building_type_ == BuildingType::terrace) {
            buffer_side = 0.0;
            if (building_gen_attempts_ % building_gen_terrace_number_attached_ == 0) {
                buffer_side = 4.0;
            }
        } else if (building_gen_building_type_ == BuildingType::attached) {
            buffer_side = 0.0;
        }

    } else { // right now we treat it like industrial
        tags[k_building] = v_industrial;
        std::tie(width, depth, buffer_side, buffer_front, buffer_back) = CreateTypicalIndustrialBuilding(this->GetGeometry()->getArea() > 5000);
    }

    // generic special handling depending on settlement type
    if (settlement_type_ == SettlementType::block or settlement_type_ == SettlementType::centre or settlement_type_ == SettlementType::dense) {
        buffer_front = Parameters::Get().OWBB_FRONT_BUFFER_DENSE;
        buffer_back = Parameters::Get().OWBB_FRONT_BUFFER_DENSE; // yes, it is small
    }

    return std::make_unique<GenBuilding>(width, depth, buffer_side, buffer_front, buffer_back,
                                         IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::building_generated),
                                         &tags, l_shaped);
}

BuildingZone::BuildingZone(const osm_id_t osm_id, std::unique_ptr<geos::geom::Geometry> && geometry,
                           BuildingZoneType building_zone_type, std::optional<BuildingZoneOrigin> building_zone_origin) : Zone(osm_id, std::move(geometry), ZoneType::building_zone, building_zone_type) {
    building_zone_origin_ = building_zone_origin;
}

bool BuildingZone::ContainsOrIntersects(const geos::geom::Geometry* other_geometry) {
    if (this->geometry_->contains(other_geometry) or this->geometry_->intersects(other_geometry)) {
        return true;
    }
    return false;
}

std::unique_ptr<geos::geom::Geometry> BuildingZone::NonIntersectingDifference(const geos::geom::Geometry * other_geometry) {
    return other_geometry->difference(GetGeometry());
}

void BuildingZone::ReassignOSMBuildingsToCityBlocks() {
    for (auto & osm_building : osm_buildings_) {
        for (auto & city_block : city_blocks_) {
            if (city_block->ContainsProperly(osm_building->GetOuterPolygon()) or city_block->Intersects(osm_building->GetOuterPolygon())) {
                city_block->RelateBuilding(osm_building);
                break;
            }
        }
    }
}

void BuildingZone::SetSettlementTypeCityBlocks(SettlementType current_settlement_type,
                                               std::unique_ptr<GridIndexedPreparedPolygon>& urban_settlement) {
    for (auto & city_block : city_blocks_) {
        if (city_block->GetSettlementType() < current_settlement_type) {
            if (urban_settlement->ContainsOther(city_block->GetGeometry()) or urban_settlement->Intersects(city_block->GetGeometry())) {
                city_block->SetSettlementType(current_settlement_type);
            }
        }
    }
}

void BuildingZone::GuessBuildingZoneType(const std::vector<std::unique_ptr<Place>> &farm_places) {
    // TODO: https://gitlab.com/osm2city/osm2gear/-/issues/18 map alternative land-uses to BuildingZones

    // now we should only have non_osm based on lonely buildings
    assert(this->building_zone_type_ == BuildingZoneType::nonOSM);
    long num_residential_buildings {0};
    long num_commercial_buildings {0};
    long num_industrial_buildings {0};
    long num_retail_buildings {0};
    long num_farm_buildings {0};
    for (auto & osm_building : osm_buildings_) {
        if (ParseIsYesBuilding(osm_building->GetTags())) {
            continue; //we do not really know what they are and by default they are mapped to residential
        }
        auto bc = ParseTagsForBuildingClass(osm_building->GetTags());
        if (bc == BuildingClass::residential_small or bc == BuildingClass::residential
                or bc == BuildingClass::terrace) {
            ++num_residential_buildings;
        } else if (bc == BuildingClass::publicx or bc == BuildingClass::commercial) {
            ++num_commercial_buildings;
        } else if (bc == BuildingClass::industrial or bc == BuildingClass::warehouse) {
            ++num_industrial_buildings;
        } else if (bc == BuildingClass::retail) {
            ++num_retail_buildings;
        } else if (bc == BuildingClass::farm) {
            ++num_farm_buildings;
        }
    }
    building_zone_type_ = BuildingZoneType::residential; //default
    auto max_value = std::max({num_residential_buildings, num_commercial_buildings, num_industrial_buildings, num_retail_buildings, num_farm_buildings});
    if (0 < max_value) {
        if (osm_buildings_.size() < 10) {
            BOOST_LOG_TRIVIAL(debug) << "Checking generated land-use for place=farm based on " << farm_places.size() << " place tags.";
            for (auto & place : farm_places) {
                if (place->IsPoint()) {
                    if (place->GetGeometry()->within(GetGeometry())) {
                        building_zone_type_ = BuildingZoneType::farmyard;
                        BOOST_LOG_TRIVIAL(debug) << "Found farmyard generated building zone based on node place";
                        return;
                    }
                } else {
                    if (place->GetGeometry()->intersects(GetGeometry())) {
                        building_zone_type_ = BuildingZoneType::farmyard;
                        BOOST_LOG_TRIVIAL(debug) << "Found farmyard generated building zone based on area place";
                        return;
                    }
                }
            }
        }
        if (num_farm_buildings == max_value) { //in small villages farm houses might be tagged, but rest just ="yes"
            if (osm_buildings_.size() >= 10 and num_farm_buildings < (long)0.5 * osm_buildings_.size()) {
                building_zone_type_ = BuildingZoneType::residential;
            } else {
                building_zone_type_ = BuildingZoneType::farmyard;
            }
        } else if (num_commercial_buildings == max_value) {
            building_zone_type_ = BuildingZoneType::commercial;
        } else if (num_industrial_buildings == max_value) {
            building_zone_type_ = BuildingZoneType::industrial;
        } else if (num_retail_buildings == max_value) {
            building_zone_type_ = BuildingZoneType::retail;
        }
    }
}

bool BuildingZone::UseForBuildingGeneration() const {
    bool use_me = true;
    // check based on parameters whether building zone should be used at all
    if (this->IsGenerated()){
        use_me = Parameters::Get().OWBB_USE_GENERATED_LANDUSE_FOR_BUILDING_GENERATION;
    }
    if (this->IsAerodrome() or this->IsFarmyard() or this->IsMilitary()){
        use_me = false;
    }
    if (this->IsOutsideOfGrid()) {
        use_me = false;
    }
    if(use_me and this->HasCityBlocksWithoutBuildings()){
        return true;
    }
    return false;
}

void BuildingZone::ProcessOwnBuildingsAsBlockedAreas(const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
    for (auto & building : osm_buildings_) {
        auto geom = std::make_shared<GridIndexedGeometry>(building->GetOuterPolygon()->clone());
        geom->AssignGridIndices(grid_index_boxes);
        this->AssignBlockedArea(geom);
    }
}

void BuildingZone::ProcessCityBlocksWithBuildingsAsBlockedAreas(const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) {
    for (auto & city_block : city_blocks_) {
        if (city_block->HasOSMBuildings()) {
            auto geom = std::make_shared<GridIndexedGeometry>(city_block->GetGeometry()->clone());
            geom->AssignGridIndices(grid_index_boxes);
            this->AssignBlockedArea(geom);
        }
    }
}

std::optional<CityBlock*> BuildingZone::FindCityBlockForPoint(double x, double y) const {
    std::optional<CityBlock*> found = std::nullopt;
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    const auto coord = geos::geom::Coordinate(x, y);
    const auto my_point = factory->createPoint(coord);
    for (auto &city_block : city_blocks_) {
        if (my_point->within(city_block->GetGeometry())) {
            found = city_block.get();
            break;
        }
    }
    return found;
}

bool BuildingZone::HasIntersectingBlockedArea(std::shared_ptr<GridIndexedGeometry> &other) const {
    for (auto &blocked_area : blocked_areas_) {
        if (GridIndexed::HaveCommonIndices(blocked_area->GetGridIndices(), other->GetGridIndices())) {
            if (not blocked_area->IsDisjoint(other->GetGeometry())) {
                return true;
            }
        }
    }
    return false;
}

GenBuildingWay::GenBuildingWay(std::unique_ptr<geos::geom::LineString> && line_string,
                               const double width, BuildingZone * building_zone,
                               const osm_id_t osm_id): width_ {width}, line_string_ {std::move(line_string)}, building_zone_ {building_zone}, osm_id_ {osm_id}{};

GenBuilding::GenBuilding(const double width, double const depth,
                         const double buffer_side, const double buffer_front, const double buffer_back,
                         const osm_id_t osm_id, tags_t *tags, const bool l_shaped): width_ {width}, depth_ {depth},
                         buffer_side_ {buffer_side}, buffer_front_ {buffer_front}, buffer_back_ {buffer_back}, l_shaped_ {l_shaped},
                                                                         Element(osm_id) {
    for (auto & it : *tags) {
        tags_[it.first] = it.second;
    }
}

std::vector<geos::geom::Coordinate> GenBuilding::CreateCoordinatesForLShape(double way_width, const geos::geom::Coordinate &origin_point, double angle_deg) const {
    const double dist_from_way = pow(way_width / 2, Parameters::Get().OWBB_HIGHWAY_WIDTH_FACTOR);
    double x_min, x_max, y_min, y_max;
    // create the rectangle points based on 0/0 as reference (i.e. point in the middle of horizontal way)
    x_min = -1*width_ / 2;
    y_min = dist_from_way + buffer_front_;
    x_max = width_ / 2;
    y_max = y_min + depth_;

    const auto generator = TileHandler::Get()->GetRandomNumberGenerator();
    std::uniform_int_distribution<> zero_to_one(0, 1);

    constexpr int SHORT_SIDE {5};
    // now do the rotation - just at the 0/0 point
    const double rotation_angle_deg = NormalizedDegrees(angle_deg + 90);

    std::pair<double, double> p1_rotated, p2_rotated, p3_rotated, p4_rotated, p5_rotated, p6_rotated;

    if (zero_to_one(*generator)) { // L-shaped, where looking like L
        p1_rotated = RotateAtZeroZero(x_min, y_min, rotation_angle_deg);
        p2_rotated = RotateAtZeroZero(x_max, y_min, rotation_angle_deg);
        p3_rotated = RotateAtZeroZero(x_max, y_max - SHORT_SIDE, rotation_angle_deg);
        p4_rotated = RotateAtZeroZero(x_min + SHORT_SIDE, y_max - SHORT_SIDE, rotation_angle_deg);
        p5_rotated = RotateAtZeroZero(x_min + SHORT_SIDE, y_max, rotation_angle_deg);
        p6_rotated = RotateAtZeroZero(x_min, y_max, rotation_angle_deg);
    } else { // L-shaped with side on right side and on top
        p1_rotated = RotateAtZeroZero(x_max - SHORT_SIDE, y_min, rotation_angle_deg);
        p2_rotated = RotateAtZeroZero(x_max, y_min, rotation_angle_deg);
        p3_rotated = RotateAtZeroZero(x_max, y_max, rotation_angle_deg);
        p4_rotated = RotateAtZeroZero(x_min, y_max, rotation_angle_deg);
        p5_rotated = RotateAtZeroZero(x_min, y_max - SHORT_SIDE, rotation_angle_deg);
        p6_rotated = RotateAtZeroZero(x_max - SHORT_SIDE, y_max - SHORT_SIDE, rotation_angle_deg);
    }

    std::vector<geos::geom::Coordinate> coordinates {};
    coordinates.emplace_back(p1_rotated.first + origin_point.x, p1_rotated.second + origin_point.y);
    coordinates.emplace_back(p2_rotated.first + origin_point.x, p2_rotated.second + origin_point.y);
    coordinates.emplace_back(p3_rotated.first + origin_point.x, p3_rotated.second + origin_point.y);
    coordinates.emplace_back(p4_rotated.first + origin_point.x, p4_rotated.second + origin_point.y);
    coordinates.emplace_back(p5_rotated.first + origin_point.x, p5_rotated.second + origin_point.y);
    coordinates.emplace_back(p6_rotated.first + origin_point.x, p6_rotated.second + origin_point.y);
    coordinates.emplace_back(p1_rotated.first + origin_point.x, p1_rotated.second + origin_point.y);  // must close linear ring
    return coordinates;
}

std::vector<geos::geom::Coordinate> GenBuilding::CreateCoordinatesForRectangle(const double way_width, const geos::geom::Coordinate &origin_point, const double angle_deg,
                                                                               const bool with_buffer) const {
    const double dist_from_way = pow(way_width / 2, Parameters::Get().OWBB_HIGHWAY_WIDTH_FACTOR);
    double x_min, x_max, y_min, y_max;
    // create the rectangle points based on 0/0 as reference (i.e. point in the middle of horizontal way
    if (with_buffer) {
        x_min = -1*(width_ / 2 + buffer_side_);
        y_min = dist_from_way + (buffer_front_ - GetMinBufferFront());
        x_max = -1 * x_min;
        y_max = dist_from_way + buffer_front_ + depth_ + buffer_back_;
    } else {
        x_min = -1*width_ / 2;
        y_min = dist_from_way + buffer_front_;
        x_max = width_ / 2;
        y_max = y_min + depth_;
    }
    // now do the rotation - just at the 0/0 point
    const double rotation_angle_deg = NormalizedDegrees(angle_deg + 90);
    auto p1_rotated = RotateAtZeroZero(x_min, y_min, rotation_angle_deg);
    auto p2_rotated = RotateAtZeroZero(x_max, y_min, rotation_angle_deg);
    auto p3_rotated = RotateAtZeroZero(x_max, y_max, rotation_angle_deg);
    auto p4_rotated = RotateAtZeroZero(x_min, y_max, rotation_angle_deg);

    std::vector<geos::geom::Coordinate> coordinates {};
    coordinates.emplace_back(p1_rotated.first + origin_point.x, p1_rotated.second + origin_point.y);
    coordinates.emplace_back(p2_rotated.first + origin_point.x, p2_rotated.second + origin_point.y);
    coordinates.emplace_back(p3_rotated.first + origin_point.x, p3_rotated.second + origin_point.y);
    coordinates.emplace_back(p4_rotated.first + origin_point.x, p4_rotated.second + origin_point.y);
    coordinates.emplace_back(p1_rotated.first + origin_point.x, p1_rotated.second + origin_point.y);  // must close linear ring
    return coordinates;
}

std::shared_ptr<GridIndexedGeometry> GenBuilding::CreatePerimeterRectangle(const double way_width, const geos::geom::Coordinate &origin_point, const double angle_deg,
                                                                           const bool with_buffer, const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes) const {
    const std::vector<geos::geom::Coordinate> coordinates = this->CreateCoordinatesForRectangle(way_width, origin_point, angle_deg, with_buffer);
    auto coord_sequence = geos::geom::CoordinateSequence();
    coord_sequence.setPoints(coordinates);
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    std::unique_ptr<geos::geom::LinearRing> ring = factory->createLinearRing(coord_sequence);
    auto poly = factory->createPolygon(std::move(ring));
    auto gig = std::make_shared<GridIndexedGeometry>(std::move(poly));
    gig->AssignGridIndices(grid_index_boxes);
    return gig;
}

std::shared_ptr<Building> GenBuilding::TransformToBuilding(const double way_width, const geos::geom::Coordinate &origin_point, const double angle_deg,
                                                           node_map_t & building_nodes) const {
    std::vector<geos::geom::Coordinate> coordinates;
    if (l_shaped_) {
        coordinates = CreateCoordinatesForLShape(way_width, origin_point, angle_deg);
    } else {
        coordinates = CreateCoordinatesForRectangle(way_width, origin_point, angle_deg, false);
    }
    refs_t refs;
    for (auto &coord : coordinates) {
        LonLat lon_lat = TileHandler::Get()->ToGlobal(coord);
        auto node = std::make_unique<Node>(IdCreator::Get().GetNextPseudoOSMId(OSMFeatureType::generic_node), lon_lat);
        refs.push_back(node->GetOSMId());
        building_nodes[node->GetOSMId()] = std::move(node);
    }
    const refs_t * const_refs_ptr = &refs;
    std::unique_ptr<geos::geom::LinearRing> outer_ring = Way::LinearRingFromRefs(this->GetOSMId(),
                                                                                 const_refs_ptr,
                                                                                 building_nodes);
    auto building = std::make_shared<Building>(this->GetOSMId(), std::move(outer_ring),
                                               this->GetTags(), const_refs_ptr, true);
    return building;
}

std::map<osm_id_t , std::shared_ptr<GridIndexedGeometry>> ProcessOpenSpaces(const OSMDataReaderOverpass &osm_reader,
                                                                            const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                                                                            const bool use_for_trees) {
    std::map<osm_id_t , std::shared_ptr<GridIndexedGeometry>> open_spaces{};

    //values for k_landuse tag
    std::vector<std::string> la_values = {v_railway, v_fairground, v_greenhouse_horticulture, v_quarry};
    if (not use_for_trees) {
        la_values.emplace_back(v_religious);
        la_values.emplace_back(v_cemetery);
        la_values.emplace_back(v_recreation_ground);
    }
    //values for k_amenity tag
    std::vector<std::string> am_values = {v_parking, v_bicycle_parking};
    if (not use_for_trees) {
        am_values.emplace_back(v_grave_yard);
    }
    //value for k_leisure tag
    std::vector<std::string> le_values = {v_horse_riding, v_marina, v_pitch, v_swimming_area, v_track};
    if (not use_for_trees) {
        le_values.emplace_back(v_beach_resort);
        le_values.emplace_back(v_common);
        le_values.emplace_back(v_dog_park);
        le_values.emplace_back(v_nature_reserve);
        le_values.emplace_back(v_playground);
    } else {
        le_values.emplace_back(v_park);
    }

    auto result = osm_reader.FetchWaysFromKeys({k_public_transport, k_railway, k_amenity, k_leisure, k_natural, k_highway});
    for (auto &it : result.ways_dict) {
        try {
            bool add_to_open_spaces = false;
            if (it.second->GetTags()->contains(k_public_transport) or it.second->GetTags()->contains(k_natural)) {
                if (it.second->GetTags()->contains(k_area)) {
                    if (it.second->GetTags()->at(k_area) == v_yes) {
                        add_to_open_spaces = true;
                    }
                }
            } else if (it.second->GetTags()->contains(k_railway)) {
                if (it.second->GetTags()->at(k_railway) == v_station) {
                    add_to_open_spaces = true;
                }
            } else if (it.second->GetTags()->contains(k_landuse)) {
                if (std::find(la_values.begin(), la_values.end(), it.second->GetTags()->at(k_landuse)) != la_values.end()) {
                    add_to_open_spaces = true;
                }
            } else if (it.second->GetTags()->contains(k_amenity)) {
                if (std::find(am_values.begin(), am_values.end(), it.second->GetTags()->at(k_amenity)) != am_values.end()) {
                    add_to_open_spaces = true;
                }
            } else if (it.second->GetTags()->contains(k_leisure)) {
                if (std::find(le_values.begin(), le_values.end(), it.second->GetTags()->at(k_leisure)) != le_values.end()) {
                    add_to_open_spaces = true;
                }
            } else if (it.second->GetTags()->contains(k_highway)) {
                if (it.second->GetTags()->at(k_highway) == v_pedestrian) {
                    if (it.second->GetTags()->contains(k_area)) {
                        if (it.second->GetTags()->at(k_area) == v_yes) {
                            add_to_open_spaces = true;
                        }
                    }
                }
            }
            const std::unique_ptr<geos::geom::Polygon> poly = it.second->PolygonFromOSMWay(result.nodes_dict);
            if (add_to_open_spaces) {
                std::shared_ptr<GridIndexedGeometry> gig;
                if (use_for_trees) {
                    gig = std::make_shared<GridIndexedGeometry>(poly->releaseExteriorRing()->buffer(Parameters::Get().TREES_BLOCKED_AREA_DEFAULT_BUFFER));
                } else {
                    gig = std::make_shared<GridIndexedGeometry>(poly->releaseExteriorRing());
                }
                gig->AssignGridIndices(grid_index_boxes);
                if (gig->HasGridIndices()) {
                    open_spaces[it.second->GetOSMId()] = std::move(gig);
                }
            }
        } catch (const InvalidGeometryFromOSM &e) {
            LogDebugWarning(e.what());
        }
    }
    return open_spaces;
}
