// SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <chrono>
#include <filesystem>
#include <sstream>

#include <geos/geom/GeometryFactory.h>


constexpr static double EQURAD {6378137.0};
constexpr static double FLATTENING {298.257223563};
constexpr static double SQUASH {0.9966471893352525192801545};
constexpr static double RA2 = 1 / (EQURAD * EQURAD);


std::stringstream LocalTimeString();

/** \brief Gets the requested environment variable from the OS and throws an exception if not found
 * @param env_variable
 * @return
 */
char* GetEnv(const char * env_variable);

struct LonLat {
    double lon;
    double lat;

    LonLat() = default;
    LonLat(double c_lon, double c_lat);
};

struct Boundary {
    double west {};
    double south {};
    double east {};
    double north {};

    Boundary() = default;
    Boundary(double west, double south, double east, double north);

    [[nodiscard]] bool IsInsideBoundary(const LonLat lon_lat) const {
        if (lon_lat.lon < west or lon_lat.lon > east or lon_lat.lat < south or lon_lat.lat > north) {
            return false;
        }
        return true;
    }
};

/*! \brief Calculates how much a given point a distance away is elevated over the round world.
 * The distance_1 and distance_2 are right-angled in a cartesian coordinate system in metres.
 * (e.g. x-direction and y_direction).
 */
double CalcHorizonElev(double distance_1, double distance_2);

/*! \brief Calculates the difference between two bearings. If positive with clock, if negative against the clock sense.
 * I.e. from bearing1 to bearing2 you need to turn delta with or against the clock.
 * The bearings are in degrees - North is 0, East is 90.
 */
double CalcDeltaBearing(double bearing_1, double bearing_2);

class TimeLogger {
private:
    std::chrono::time_point<std::chrono::system_clock> reference_;
public:
    TimeLogger();
    void Log(std::string message);
};

/*! \brief Write a log message to debug level and mark it as a warning
 * If this warning is really for debugging/programming and not for the overall process.
 * Basically just makes sure that there is a standardised warning mark, so the log
 * can be searched systematically
 * @param message
 */
void LogDebugWarning(std::string message);

double RadiansToDegrees(double radians);

double DegreesToRadians(double degrees);

/*! \brief Makes sure that degrees are between 0 and 360.
 */
double NormalizedDegrees(double degree);

/*! \brief Returns the distance between two points based on local coordinates (x,y).
 */
double CalcDistanceLocal(double x_1, double y_1, double x_2, double y_2);

/*! \brief Returns the angle in degrees of a line relative to North.
 Based on local coordinates (x,y) of two points.
 */
double CalcAngleOfLineLocal(double x_1, double y_1, double x_2, double y_2);

/*! \brief Rotates a point x/y around an origin at 0/0 clockwise, where 0 degs is at North
 */
std::pair<double, double> RotateAtZeroZero(double x, double y, double angle_deg);

std::pair<double, double> CalcPointAngleAway(double x, double y, double added_distance, double angle_deg);

/*! \brief Validate that the smallest value is within a tolerance smaller than the largest value.
 * The tolerance is relative: e.g. for values 0.8 and 1.0 a tolerance of 0.2 would just make it.
 *
 * @param value_1
 * @param value_2
 * @param relative_tolerance
 * @return
 */
bool CheckWithinRelativeToleranceOfMax(double value_1, double value_2, double relative_tolerance);

/*! \brief Tokenizes a string based on a delimiter and returns the result into an existing vector.
 *
 * @param s
 * @param delim
 * @param out
 */
void TokenizeString(const std::string &s, char delim, std::vector<std::string> &out);

/*! \brief Handle the geometry difference from an input into distinct polygons of a minimal size.
 *
 * The result can be empty e.g. if the second geometry covers the first geometry or if the remaining difference parts
 * are all smaller than the min_area size.
 *
 * @param first_geom the geometry from which the difference gets removed
 * @param second_geom the geometry part we do not want
 * @param min_area
 * @return nullopt if there is no overlap or a (maybe empty) list of polygons that make up the first_geom
 *         but not the other
 */
std::optional<std::vector<std::unique_ptr<geos::geom::Geometry>>> PolygonsFromGeometryDifference(const geos::geom::Geometry* first_geom,
                                                                                                 const geos::geom::Geometry* second_geom,
                                                                                                 int min_area);