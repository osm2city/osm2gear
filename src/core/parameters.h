// SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <string>

class Parameters {
private:
    Parameters() = default;
public:
    static Parameters& Get() {
        static Parameters instance;
        return instance;
    }
    Parameters(Parameters const&) = delete;
    Parameters(Parameters &&) = delete;
    Parameters operator=(Parameters const&) = delete;
    Parameters operator=(Parameters &&) = delete;

    // from command line options
    std::string PATH_TO_SCENERY {};
    std::string PATH_TO_OUTPUT {};
    bool USE_WS30_SCENERY = false;

    // fixed
    double OWBB_PLACE_BOUNDARY_EXTENSION {10'000};

    bool DEBUG_PLOT_LANDUSE {true};
    bool DEBUG_PLOT_TREES {true};

    int OWBB_GENERATE_LANDUSE_BUILDING_BUFFER_DISTANCE {30};
    int OWBB_GENERATE_LANDUSE_BUILDING_BUFFER_DISTANCE_MAX {50};
    // a compromise in having enough space for larger plausible buildings and intersecting with
    // existing landuse zones
    int OWBB_GENERATE_LANDUSE_RESIDENTIAL_WAY_BUFFER_DISTANCE {40};
    int OWBB_GENERATE_LANDUSE_HOLES_MIN_AREA {20000};
    int OWBB_GENERATE_LANDUSE_SIMPLIFICATION_TOLERANCE {20};

    int OWBB_BUILT_UP_BUFFER {50};

    long OWBB_BUILT_UP_MIN_LIT_AREA {100000};

    int OWBB_PLACE_POPULATION_DEFAULT_CITY {200000};
    int OWBB_PLACE_POPULATION_DEFAULT_TOWN {20000};
    int OWBB_PLACE_POPULATION_MIN_BLOCK = 15000;
    double OWBB_PLACE_RADIUS_EXPONENT_CENTRE {0.5};  // 1/2
    double OWBB_PLACE_RADIUS_EXPONENT_BLOCK {0.6};  // 5/8
    double OWBB_PLACE_RADIUS_EXPONENT_DENSE {0.666};  // 2/3
    double OWBB_PLACE_RADIUS_FACTOR_CITY {1.};
    double OWBB_PLACE_RADIUS_FACTOR_TOWN {1.};

    int OWBB_GRID_SIZE {2000}; // tested with 500 - not faster in trees

    // This is for defining city blocks based on highways and building zones -> split into
    int OWBB_MIN_CITY_BLOCK_AREA {200};
    int OWBB_MIN_CITY_BLOCK_AREA_FOR_GENERATION {1000};
    // Buffer around highways to split building zones into city blocks
    int OWBB_CITY_BLOCK_HIGHWAY_BUFFER {3};
    // Buffer around buildingZones to find highways, which might be used to generate plausible buildings.
    // Needs to be a bit large because at zone borders often there is a bit of distance mapped to highways in OSM.
    int OWBB_BUILDING_ZONE_HIGHWAY_SEARCH_BUFFER {12};
    bool OWBB_SPLIT_AT_MAJOR_LINES {false};

    bool OWBB_GENERATE_BUILDINGS {false}; // set by command line argument
    bool OWBB_USE_GENERATED_LANDUSE_FOR_BUILDING_GENERATION {true};
    int OWBB_MIN_STREET_LENGTH {5};
    int OWBB_STEP_DISTANCE {2};
    double OWBB_HIGHWAY_WIDTH_FACTOR {1.3};

    double OWBB_INDUSTRIAL_LARGE_SHARE {0.4};
    int OWBB_INDUSTRIAL_BUFFER_MIN{3};
    int OWBB_INDUSTRIAL_BUFFER_MAX {10};
    double OWBB_FRONT_BUFFER_DENSE{3.0}; //same for front and back

    double OWBB_RESIDENTIAL_SIDE_BUFFER_FACTOR_PERIPHERY {1.0};
    double OWBB_RESIDENTIAL_SIDE_BUFFER_FACTOR_DENSE {0.8};
    double OWBB_RESIDENTIAL_BACK_BUFFER_FACTOR_PERIPHERY {2.0};
    double OWBB_RESIDENTIAL_FRONT_BUFFER_FACTOR_PERIPHERY {1.0};
    double OWBB_DETACHED_TYPICAL_SIDE {10.};
    double OWBB_DETACHED_MAX_AREA {130.};
    double OWBB_DETACHED_SHARE_L_SHAPED {0.2};
    double OWBB_TERRACE_TYPICAL_WIDTH {10.};
    double OWBB_TERRACE_TYPICAL_DEPTH {10.};
    double OWBB_APARTMENT_TYPICAL_WIDTH {18.};
    double OWBB_APARTMENT_TYPICAL_DEPTH {12.};
    double OWBB_ATTACHED_TYPICAL_WIDTH {20.};
    double OWBB_ATTACHED_TYPICAL_DEPTH {14.};

    double OWBB_ATTACHED_NODES_MAX_DIST {2.5};

    // the number (plus/minus 1) of houses within a terrace attached to each other
    // // use large number like 999 in order not to have interruptions - e.g. in typical terraces in UK
    int OWBB_RESIDENTIAL_TERRACE_TYPICAL_NUMBER {4};

    // the share in % of different types of residential buildings depending on the settlement type
    // sequence is detached (house), terrace (row house), flats (apartment) detached, flats (apartment) attached
    // sum must add up to 100 (%), otherwise distribution will get calculated wrongly
    // no need for BLOCK and CENTRE, at there ATTACHED is default
    std::array<int, 4> OWBB_RESIDENTIAL_RURAL_TYPE_SHARE = {80, 10, 10, 0};
    std::array<int, 4> OWBB_RESIDENTIAL_PERIPHERY_TYPE_SHARE = {45, 25, 30, 0};
    std::array<int, 4> OWBB_RESIDENTIAL_DENSE_TYPE_SHARE = {10, 10, 30, 50};

    double BUILDING_MIN_AREA {50.0}; // minimum area for a building to be included in output (not used for buildings with parent)
    double BUILDING_INNER_MIN_AREA {BUILDING_MIN_AREA}; // minimum area for an inner ring - we want it to be visible
    double BUILDING_REDUCE_THRESHOLD {200.0}; // threshold area of a building below which a rate of buildings gets reduced from output
    double BUILDING_REDUCE_RATE {0.1}; // rate (between 0 and 1) of buildings below a threshold which get reduced randomly in output

    double BUILDING_MIN_NODE_DISTANCE {0.2}; // make sure nodes in buildings hava a minimum distance from each other

    double BUILDING_BALCONY_TOLERANCE_BASE {1.0};
    double BUILDING_BALCONY_TOLERANCE_RAILING {2.5};
    double BUILDING_BALCONY_TOLERANCE_DIFF {0.2};

    int TREES_DIST_BETWEEN_TREES_PARK {10}; // the average distance between trees in a park
    // the rate at which randomly generated trees are kept (0.7 means 7 out of 10 are kept)
    // relative high value due to use of Perlin
    double TREES_KEEP_RATE_TREES_PARK {0.9};
    int TREES_DIST_BETWEEN_TREES_PARK_MAPPED {100}; // the average distance between trees in a park for testing manually mapped trees
    int TREES_PARK_MIN_SIZE {600}; // the minimal size of a park to be taken into account

    int TREES_DIST_BETWEEN_TREES_SMALL_FOREST {8}; // the average distance between trees in a small forest
    double TREES_KEEP_RATE_TREES_SMALL_FOREST {0.9};
    int TREES_SMALL_FOREST_MAX_SIZE {10000}; // the maximal size of a forest to be taken into account

    // testing against the mid-point, not the whole tree with branches
    int TREES_NATURAL_WATER_BUFFER {5};
    int TREES_BLOCKED_AREA_DEFAULT_BUFFER {5};
    int TREES_LARGE_TRANSPORT_BUFFER {10};

    //garden is for residential and farmyards
    //keep rates are for dense/rural settlement areas
    int TREES_DIST_BETWEEN_TREES_GARDEN {5}; // the average distance between trees in a garden
    double TREES_KEEP_RATE_TREES_GARDEN {0.7}; // the rate at which trees are randomly kept (max = 1.0)
    double TREES_KEEP_RATE_TREES_GARDEN_BLOCK {0.6};
    double TREES_KEEP_RATE_TREES_GARDEN_CENTRE {0.4};
    //lot is for surrounding of industrial, retail, commercial
    int TREES_DIST_BETWEEN_TREES_LOT {15};
    double TREES_KEEP_RATE_TREES_LOT {0.5};

    int TREES_MAX_AVG_DIST_TREES_ROW {20}; // if average distance between mapped trees smaller, then replace with calculated
    int TREES_DIST_TREES_ROW_CALCULATED {15}; // the distance used between trees in a row/line if calculated manually
    int TREES_DIST_MINIMAL {10}; // minimal distance between trees unless mapped manually
    double TREES_RADIUS_TEST {3.0}; //radius to test around tree coordinate and for random distribution
};
