// SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#include "utils.h"

#include <cmath>
#include <iomanip> // put_time

#include "boost/log/trivial.hpp"


std::stringstream LocalTimeString() {
    const auto now = std::chrono::system_clock::now();
    const auto in_time_t = std::chrono::system_clock::to_time_t(now);
    std::stringstream datetime;
    datetime << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d_%H%M%S");
    return datetime;
}

char* GetEnv(const char * env_variable) {
    const auto result = std::getenv(env_variable);
    if (result == nullptr) {
        throw std::invalid_argument(std::string(env_variable) + " must be set as an environment variable on operating system level");
    }
    return result;
}

Boundary::Boundary(const double west, const double south, const double east, const double north): west {west}, south {south}, east {east}, north {north} {}

LonLat::LonLat(const double c_lon, const double c_lat): lon {c_lon}, lat {c_lat} {}

double CalcHorizonElev(const double distance_1, const double distance_2) {
    const auto distance_square = pow(distance_1, 2) + pow(distance_2, 2);
    const auto hypotenuse = sqrt(distance_square + pow(EQURAD, 2));
    return hypotenuse - EQURAD;
}

double CalcDeltaBearing(double bearing_1, double bearing_2) {
    if (bearing_1 == 360) {
        bearing_1 = 0.;
    }
    if (bearing_2 == 360) {
        bearing_2 = 0.;
    }
    double delta = bearing_2 - bearing_1;
    if (delta > 180) {
        delta -= 360;
    } else if (delta < -180) {
        delta += 360;
    }
    return delta;
}

bool CheckWithinRelativeToleranceOfMax(const double value_1, const double value_2, const double relative_tolerance) {
    return fmax(value_1, value_2) - value_1 <= relative_tolerance * fmax(value_1, value_2);
}

TimeLogger::TimeLogger() {
    reference_ =std::chrono::system_clock::now();
}

void TimeLogger::Log(std::string message) {
    auto now_time = std::chrono::system_clock::now();
    auto elapsed_time_s = std::chrono::duration<double>(now_time - reference_).count();
    BOOST_LOG_TRIVIAL(info) << message << ": " << elapsed_time_s;
    reference_ = now_time;
}

void LogDebugWarning(std::string message) {
    BOOST_LOG_TRIVIAL(debug) << "**WARNING** " << message;
}

double RadiansToDegrees(const double radians) {
    return radians * (180.0 / std::numbers::pi);
}

double DegreesToRadians(const double degrees) {
    return degrees * (std::numbers::pi / 180.0);
}

double NormalizedDegrees(const double degree) {
    if (degree < 0) {
        return degree + 360;
    } else if (degree >= 360) {
        return degree - 360;
    }
    return degree;
}

double CalcDistanceLocal(const double x_1, const double y_1, const double x_2, const double y_2) {
    return sqrt(pow(x_1 - x_2, 2) + pow(y_1 - y_2, 2));
}

double CalcAngleOfLineLocal(const double x_1, const double y_1, const double x_2, const double y_2) {
    const double angle = atan2(x_2 - x_1, y_2 - y_1);
    const double degree = RadiansToDegrees(angle);
    return NormalizedDegrees(degree);
}

std::pair<double, double> RotateAtZeroZero(const double x, const double y, const double angle_deg) {
    const double angle_rad = DegreesToRadians(angle_deg);
    const double s = sin(angle_rad);
    const double c = cos(angle_rad);
    double x_new = x * c + y * s;
    double y_new = - x * s + y * c;
    return std::make_pair(x_new, y_new);
}

std::pair<double, double> CalcPointAngleAway(const double x, const double y, const double added_distance, const double angle_deg) {
    const double angle_rad = DegreesToRadians(angle_deg);
    double x_new = x + added_distance * sin(angle_rad);
    double y_new = y + added_distance * cos(angle_rad);
    return std::make_pair(x_new, y_new);
}


void TokenizeString(const std::string &s, const char delim, std::vector<std::string> &out) {
    std::string::size_type beg = 0;
    for (auto end = 0; (end = s.find(delim, end)) != std::string::npos; ++end) {
        out.push_back(s.substr(beg, end - beg));
        beg = end + 1;
    }
    out.push_back(s.substr(beg));
}

std::optional<std::vector<std::unique_ptr<geos::geom::Geometry>>> PolygonsFromGeometryDifference(const geos::geom::Geometry* first_geom,
                                                                                                 const geos::geom::Geometry* second_geom,
                                                                                                 const int min_area) {
    std::vector<std::unique_ptr<geos::geom::Geometry>> polygons {};
    auto geometry_difference = first_geom->difference(second_geom);
    if (geometry_difference->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON and
        !geometry_difference->isEmpty() and
        geometry_difference->isValid() and
        geometry_difference->getArea() >= min_area) {
        polygons.push_back(std::move(geometry_difference));
    } else if (geometry_difference->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTIPOLYGON) {
        const auto number_geometries = geometry_difference->getNumGeometries();
        for (std::size_t i = 0; i < number_geometries; ++i) {
            auto part_geom = geometry_difference->getGeometryN(i)->clone();
            if (part_geom->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON and
                !part_geom->isEmpty() and
                part_geom->isValid() and
                part_geom->getArea() >= min_area) {
                polygons.push_back(std::move(part_geom));
            }
        }
    } else {  // no overlap and therefore no difference
        return std::nullopt;
    }
    return polygons;
}
