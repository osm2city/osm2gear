//
// Generates plausible buildings where they would be if only they had been mapped in OSM
//

// SPDX-FileCopyrightText: (C) 2023 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "buildings.h"
#include "osm/data_reader_overpass.h"

void GeneratePlausibleBuildings(OSMDataReaderOverpass &osm_reader,
                                const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                                std::map<osm_id_t, std::unique_ptr<Highway>> &highways,
                                std::map<osm_id_t, std::unique_ptr<RailwayLine>> &railways,
                                std::map<osm_id_t, std::unique_ptr<Waterway>> &waterways,
                                const std::map<int, std::unique_ptr<geos::geom::Polygon>> &grid_index_boxes,
                                node_map_t &building_nodes,
                                std::vector<std::shared_ptr<Building>> &buildings);
