// SPDX-FileCopyrightText: (C) 2024 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#include "simgear/bucket/newbucket.hxx"

#include <geos/geom/CoordinateSequence.h>
#include <geos/geom/LinearRing.h>

#include "tile_handler.h"

const double TileHandler::E2 = fabs(1. - SQUASH * SQUASH);
const double TileHandler::E4 = E2 * E2;

std::unique_ptr<TileHandler> TileHandler::instance_ = nullptr;

TileHandler::TileHandler(const long tile_index): tile_index_ {tile_index} {
    const auto sg_bucket = SGBucket(tile_index);
    if (not sg_bucket.isValid()) {
        throw std::invalid_argument("Submitted tile index is not valid");
    }
    // we could keep using sg_bucket instead of transforming - for now we simulate the way it was in Python
    tile_centre_ = {};
    tile_centre_.lon = sg_bucket.get_center_lon();
    tile_centre_.lat = sg_bucket.get_center_lat();
    boundary_ = {sg_bucket.get_center_lon() - sg_bucket.get_width() / 2,
                 sg_bucket.get_center_lat() - sg_bucket.get_height() / 2,
                 sg_bucket.get_center_lon() + sg_bucket.get_width() / 2,
                 sg_bucket.get_center_lat() + sg_bucket.get_height() / 2};

    //Compute radii for local origin
    constexpr double f = 1. / FLATTENING;
    constexpr double e2 = f * (2.-f);

    this->cos_lat_ = cos(DegreesToRadians(tile_centre_.lat));
    const double sin_lat = sin(DegreesToRadians(tile_centre_.lat));
    this->r1_ = EQURAD * (1.-e2)/pow((1. - e2 * pow(sin_lat, 2)), (3./2.));
    this->r2_ = EQURAD / sqrt(1 - e2 * pow(sin_lat, 2));

    proj_context_ = proj_context_create();
    const std::string local_projection = std::format("+proj=laea +lon_0={0} +lat_0={1} +ellps=WGS84", tile_centre_.lon, tile_centre_.lat);
    proj_ = proj_create(proj_context_, local_projection.c_str());

    const std::unique_ptr<geos::geom::PrecisionModel> pm(new geos::geom::PrecisionModel());
    geometry_factory_ = geos::geom::GeometryFactory::create(pm.get(), -1);
}

TileHandler::~TileHandler() {
    proj_destroy(proj_);
    proj_context_destroy(proj_context_);
}

geos::geom::Coordinate TileHandler::ToLocal(const LonLat &global) const {
    const double y = r1_ * DegreesToRadians(global.lat - tile_centre_.lat);
    const double x = r2_ * DegreesToRadians(global.lon - tile_centre_.lon) * cos_lat_;
    /*
    PJ_COORD global_coord;
    global_coord.xy.x = global.lon;
    global_coord.xy.y = global.lat;
    PJ_COORD local_coord = proj_trans(proj_, PJ_FWD, global_coord);
    auto foo = proj_errno(proj_);
    double proj_x = local_coord.xy.x;
    double proj_y = local_coord.xy.y;
     */
    return geos::geom::Coordinate {x, y};
}

LonLat TileHandler::ToGlobal(const geos::geom::Coordinate &local) const {
    const LonLat converted(local.x, local.y);
    return ToGlobal(converted);
}

LonLat TileHandler::ToGlobal(const LonLat &local) const {
    LonLat global {};
    global.lat = RadiansToDegrees(local.lat / r1_) + tile_centre_.lat;
    global.lon = RadiansToDegrees(local.lon / (r2_ * cos_lat_)) + tile_centre_.lon;
    /*
    PJ_COORD local_coord = proj_coord(local.lon, local.lat, 0, 0);
    PJ_COORD global_coord = proj_trans(proj_, PJ_INV, local_coord);
    LonLat global_proj {};
    global_proj.lat = global_coord.xy.y;
    global_proj.lon = global_coord.xy.x;
    */
    return global;
}

std::pair<geos::geom::Coordinate, geos::geom::Coordinate>
TileHandler::GetTileExtentLocal() const {
    geos::geom::Coordinate lower_left = ToLocal(LonLat{boundary_.west, boundary_.south});
    geos::geom::Coordinate upper_right = ToLocal(LonLat{boundary_.east, boundary_.north});
    return std::make_pair(lower_left, upper_right);
}

std::unique_ptr<geos::geom::Polygon> TileHandler::GetTileExtentLocalPolygon() const {
    auto [min_coord, max_coord] = this->GetTileExtentLocal();
    return MakeLocalBox(min_coord.x, min_coord.y, max_coord.x, max_coord.y);
}

Boundary TileHandler::CreateExtendedBoundary(const double extension) const {
    const auto lower_left_local = this->ToLocal(LonLat{boundary_.west, boundary_.south});
    const auto lower_left_global = this->ToGlobal(geos::geom::Coordinate {lower_left_local.x - extension,
                                                                   lower_left_local.y - extension});
    const auto upper_right_local = this->ToLocal(LonLat{boundary_.east, boundary_.north});
    const auto upper_right_global = this->ToGlobal(geos::geom::Coordinate {upper_right_local.x + extension,
                                                                    upper_right_local.y + extension});
    return Boundary {lower_left_global.lon, lower_left_global.lat, upper_right_global.lon, upper_right_global.lat};
}

std::unique_ptr<geos::geom::Polygon> MakeLocalBox(const double x_min, const double y_min, const double x_max, const double y_max) {
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    auto coord_sequence = geos::geom::CoordinateSequence();
    coord_sequence.add(x_min, y_min);
    coord_sequence.add(x_max, y_min);
    coord_sequence.add(x_max, y_max);
    coord_sequence.add(x_min, y_max);
    coord_sequence.add(x_min, y_min);  // must close linear ring

    std::unique_ptr<geos::geom::LinearRing> ring = factory->createLinearRing(coord_sequence);
    return factory->createPolygon(std::move(ring));
}

std::vector<std::unique_ptr<geos::geom::Polygon>> MergePolygons(std::vector<std::unique_ptr<geos::geom::Polygon>> &original_list) {
    const auto factory = TileHandler::Get()->GetGeometryFactory();
    const std::unique_ptr<geos::geom::MultiPolygon> collection = factory->createMultiPolygon(std::move(original_list));
    const auto unified = collection->Union();

    std::vector<std::unique_ptr<geos::geom::Polygon>> handled_list {};
    if (unified->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON) {
        auto* p = dynamic_cast<geos::geom::Polygon*>(unified.get());
        if (p) {
            handled_list.push_back(p->clone());
        }
    } else if (unified->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTIPOLYGON) {
        const auto number_geometries = unified->getNumGeometries();
        for (std::size_t i = 0; i < number_geometries; ++i) {
            auto part_unified = unified->getGeometryN(i)->clone();
            if (not part_unified->isEmpty() and part_unified->isValid() and
                part_unified->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON) {
                auto* p = dynamic_cast<geos::geom::Polygon*>(part_unified.get());
                if (p) {
                    handled_list.push_back(p->clone());
                }
            }
        }
    }
    return handled_list;
}

std::vector<std::unique_ptr<geos::geom::Polygon>> MergeGeomsToPolys(std::vector<std::unique_ptr<geos::geom::Geometry>> &original_list) {
    //first make sure that the polygons to be merged are actually good polygons
    std::vector<std::unique_ptr<geos::geom::Polygon>> cleaned_list {};
    for (auto &poly : original_list) {
        if (poly->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_POLYGON) {
            if (not poly->isEmpty() and poly->isValid()) {
                auto* p = dynamic_cast<geos::geom::Polygon*>(poly.get());
                if (p) {
                    cleaned_list.push_back(p->clone());
                }
            }
        } else if (poly->getGeometryTypeId() == geos::geom::GeometryTypeId::GEOS_MULTIPOLYGON) {
            const auto number_geometries = poly->getNumGeometries();
            for (std::size_t i = 0; i < number_geometries; ++i) {
                auto part_poly = poly->getGeometryN(i)->clone();
                if (not part_poly->isEmpty() and part_poly->isValid()) {
                    auto* p = dynamic_cast<geos::geom::Polygon*>(part_poly.get());
                    if (p) {
                        cleaned_list.push_back(p->clone());
                    }
                }
            }
        }
    }
    if (cleaned_list.size() < 2) {
        return cleaned_list;
    }
    return MergePolygons(cleaned_list);
}

std::string LocationDirectoryName(const LonLat &lon_lat) {
    const SGBucket bucket {lon_lat.lon, lon_lat.lat};
    return bucket.gen_base_path();
}

std::filesystem::path ConstructPathToFiles(const std::string &base_directory,
                                           const std::string &scenery_type,
                                           const LonLat &lon_lat) {
    std::filesystem::path p {};
    p.append(base_directory);
    p.append(scenery_type);
    p.append(LocationDirectoryName(lon_lat));
    return p;
}

std::string BuildFileNameWithTileIndex(const std::string &title, const std::string &post_fix, const bool include_time) {
    std::stringstream file_name;
    file_name << "osm2gear_" << title << "_";
    file_name << TileHandler::Get()->GetTileIndex();
    if (include_time) {
        file_name << "_" << LocalTimeString().str();
    }
    file_name << "." << post_fix;
    return file_name.str();
}

