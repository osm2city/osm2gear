// SPDX-FileCopyrightText: (C) 2021 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include "../osm/data_reader_overpass.h"

void ProcessLandUse(OSMDataReaderOverpass &osm_reader, const bool &create_trees);
