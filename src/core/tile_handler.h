// SPDX-FileCopyrightText: (C) 2024 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <random>

#include <proj.h>

#include "utils.h"


class TileHandler {
   private:
    static std::unique_ptr<TileHandler> instance_;
    const static double E2;  //cannot be constexpr due to use of fabs()
    const static double E4;

    Boundary boundary_;
    LonLat tile_centre_{};
    long tile_index_;

    double cos_lat_;
    double r1_;
    double r2_;

    PJ_CONTEXT * proj_context_;
    PJ * proj_;

    // we do not mind a generator, which always does the same due to constant seed -> debugging
    std::unique_ptr<std::mt19937> generator_ = std::make_unique<std::mt19937>(12345);

    geos::geom::GeometryFactory::Ptr geometry_factory_;

    explicit TileHandler(long tile_index);

   public:
    static void init(const long tile_index) {
        if (!instance_) {
            instance_ = std::unique_ptr<TileHandler>(new TileHandler(tile_index));
        }
    }
    static TileHandler* Get() {
        if (instance_) {
            return instance_.get();
        } else {
            throw std::runtime_error("Error: Singleton TileHandler not initialized! Call init() before Get().");
        }
    }
    TileHandler(TileHandler const&) = delete;
    TileHandler(TileHandler &&, long) = delete;
    TileHandler operator=(TileHandler const&) = delete;
    TileHandler operator=(TileHandler &&) = delete;
    ~TileHandler();

    [[nodiscard]] geos::geom::GeometryFactory* GetGeometryFactory() const {
        return geometry_factory_.get();
    }
    [[nodiscard]] std::mt19937 * GetRandomNumberGenerator() const {
        return generator_.get();
    }
    [[nodiscard]] geos::geom::Coordinate ToLocal(const LonLat &) const;
    [[nodiscard]] LonLat ToGlobal(const geos::geom::Coordinate &) const;
    [[nodiscard]] LonLat ToGlobal(const LonLat &) const;
    [[nodiscard]] std::pair<geos::geom::Coordinate, geos::geom::Coordinate> GetTileExtentLocal() const;
    [[nodiscard]] std::unique_ptr<geos::geom::Polygon> GetTileExtentLocalPolygon() const;
    [[nodiscard]] Boundary CreateExtendedBoundary(double extension) const;
    [[nodiscard]] Boundary GetTileBoundary() const {
        return boundary_;
    }
    [[nodiscard]] LonLat GetTileCentre() const {
        return tile_centre_;
    }
    [[nodiscard]] std::string GetTileIndex() const {
        return std::to_string(tile_index_);
    }
};

std::unique_ptr<geos::geom::Polygon> MakeLocalBox(double x_min, double y_min, double x_max, double y_max);

/*! \brief Merge geometries into as few polygons as possible.
 * It makes sure that the original type is Polygon or Multipolygon and only returns type Polygon.
 */
std::vector<std::unique_ptr<geos::geom::Polygon>> MergeGeomsToPolys(std::vector<std::unique_ptr<geos::geom::Geometry>> &original_list);

/*! \brief Merge polygons into as few polygons as possible.
 * It makes sure that the type is Polygon and not e.g. MultiPolygon.
 */
std::vector<std::unique_ptr<geos::geom::Polygon>> MergePolygons(std::vector<std::unique_ptr<geos::geom::Polygon>> &original_list);

std::string LocationDirectoryName(const LonLat &lon_lat);

/*! \brief """Returns the path to e.g. stg-files in an FG scenery directory hierarchy at a given global lat/lon location.
 *The scenery type is e.g. 'Terrain', 'Object', 'Buildings'.
 */
std::filesystem::path ConstructPathToFiles(const std::string &base_directory,
                                           const std::string &scenery_type,
                                           const LonLat &lon_lat);

std::string BuildFileNameWithTileIndex(const std::string &title, const std::string &post_fix, bool include_time);
