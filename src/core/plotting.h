// SPDX-FileCopyrightText: (C) 2022 - 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <memory>
#include <vector>

#include "geos/geom/Geometry.h"

#include "models.h"
#include "trees.h"


void DrawLandUse(const std::vector<std::shared_ptr<Building>> &buildings,
                 const std::vector<std::unique_ptr<BuildingZone>> &built_up_zones,
                 const std::vector<std::unique_ptr<geos::geom::Geometry>> &lit_areas,
                 const std::vector<std::unique_ptr<GridIndexedGeometry>> &settlement_clusters);

void DrawTrees(const std::vector<std::shared_ptr<Building>> &buildings,
               const std::vector<std::shared_ptr<Tree>> &trees);
