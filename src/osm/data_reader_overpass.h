// SPDX-FileCopyrightText: (C) 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

// https://wiki.openstreetmap.org/wiki/Overpass_API
// https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide
// https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_API_by_Example
// https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL

#pragma once

#include <vector>

#include "osm_types.h"
#include "../core/utils.h"

class OSMDataReaderOverpass {
    const std::string overpass_api_endpoint_; // e.g. https://overpass-api.de/api/interpreter
    const Boundary &boundary_;
    const Boundary &extended_boundary_;
    bool use_extended_boundary_ = false;

    void QueryFlat(const std::string &, bool, node_map_t &nodes_dict, way_map_t &ways_dict) const;
    void QueryRelations(const std::string &, node_map_t &nodes_dict, shared_way_map_t &shared_ways_dict, relation_map_t &relations_dict) const;
    void Query(const std::string &, bool, bool, node_map_t &nodes_dict,
               way_map_t &ways_dict, shared_way_map_t &shared_ways_dict,
               relation_map_t &relations_dict) const;
    [[nodiscard]] OSMReadResult ReadFlat(const std::string &, bool) const;
    [[nodiscard]] OSMReadRelationsResult ReadRelations(const std::string &) const;
    [[nodiscard]] OSMReadRelationsResult ReadRelationsSimple(const std::string &) const;
    [[nodiscard]] std::string CreateOverpassBoundaryString() const {
        const auto boundary_to_use = use_extended_boundary_ ? extended_boundary_ : boundary_;
        return std::format("{},{},{},{}", boundary_to_use.south, boundary_to_use.west, boundary_to_use.north, boundary_to_use.east);
    }

public:
    OSMDataReaderOverpass(std::string, const Boundary &, const Boundary &);
    [[nodiscard]] OSMReadResult FetchWaysFromKeyValues(const std::vector<std::pair<std::string, std::string>> &) const;
    [[nodiscard]] OSMReadResult FetchWaysFromKeys(const std::vector<std::string> &) const;

    [[nodiscard]] OSMReadResult FetchNodesIsolatedFromKeyValues(const std::vector<std::pair<std::string, std::string>> &) const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsAerodrome() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsLanduse() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsBuildings() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsPlaces() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsRiverbanks() const;

    [[nodiscard]] OSMReadRelationsResult FetchRelationsNaturalWater() const;

    void SetUseExtendedBoundary(bool);

    //static std::string CreateKeyValuePair(const std::string &key, const std::string &value);
};
