// SPDX-FileCopyrightText: (C) 2024, rick@vanosten.net
// SPDX-License-Identifier: GPL-2.0-or-later

#include "data_reader_overpass.h"

#include <cpr/cpr.h>

#include <nlohmann/json.hpp>
#include <utility>

#include "boost/log/trivial.hpp"
#include "constants.h"
#include "osm_types.h"

using json = nlohmann::json;

constexpr char overpass_elements[] = "elements";
constexpr char overpass_id[] = "id";
constexpr char overpass_lat[] = "lat";
constexpr char overpass_lon[] = "lon";
constexpr char overpass_members[] = "members";
constexpr char overpass_node[] = "node";
constexpr char overpass_nodes[] = "nodes";
constexpr char overpass_ref[] = "ref";
constexpr char overpass_relation[] = "relation";
constexpr char overpass_role[] = "role";
constexpr char overpass_tags[] = "tags";
constexpr char overpass_type[] = "type";
constexpr char overpass_way[] = "way";


static std::string ConstructOverpassKey(const std::string &key) {
    std::stringstream query;
    query << "[\"" << key << "\"]"; //needing "" because some keys (and values) contain e.g. ':', which is a special character in Overpass QL
    return query.str();
}

static std::string ConstructOverpassKeyValue(const std::string &key, const std::string &value) {
    std::stringstream query;
    query << "[\"" << key << "\"=\"" << value << "\"]";
    return query.str();
}

OSMDataReaderOverpass::OSMDataReaderOverpass(std::string overpass_api_endpoint,
                                             const Boundary &boundary, const Boundary &extended_boundary) : overpass_api_endpoint_{std::move(overpass_api_endpoint)},
                                                                                                            boundary_{boundary},
                                                                                                            extended_boundary_{extended_boundary} {}

void OSMDataReaderOverpass::SetUseExtendedBoundary(const bool use_extended_boundary) {
    use_extended_boundary_ = use_extended_boundary;
}

void OSMDataReaderOverpass::QueryFlat(const std::string &query, const bool read_node_tags,
                                      node_map_t &nodes_dict, way_map_t &ways_dict) const {
    shared_way_map_t shared_ways_dict;
    relation_map_t relations_dict;
    return Query(query, read_node_tags, false, nodes_dict, ways_dict, shared_ways_dict, relations_dict);
}

void OSMDataReaderOverpass::QueryRelations(const std::string &query,
                                           node_map_t &nodes_dict, shared_way_map_t &shared_ways_dict, relation_map_t &relations_dict) const {
    way_map_t ways_dict;
    return Query(query, false, true, nodes_dict, ways_dict, shared_ways_dict, relations_dict);
}

void OSMDataReaderOverpass::Query(const std::string &query, const bool read_node_tags, const bool is_relations,
                                  node_map_t &nodes_dict,
                                  way_map_t &ways_dict, shared_way_map_t &shared_ways_dict,
                                  relation_map_t &relations_dict) const {
    cpr::Response r = Post(cpr::Url{overpass_api_endpoint_},
                           cpr::Payload{{"data", query}},
                           cpr::Timeout{60000});
    if (r.status_code != 200 and r.status_code != 203) {
        throw std::runtime_error("Cannot continue without OSM data: Overpass API returned not suitable status code: " + std::to_string(r.status_code));
    }
    for (auto json_result = json::parse(r.text); auto &element : json_result[overpass_elements]) {
        auto osm_id = element[overpass_id];
        std::map<std::string, std::string> tags {};
        auto read_tags = true;
        if (element[overpass_type] == overpass_node and read_node_tags == false) {
            read_tags = false;
        }
        if (read_tags) {
            try {
                for (auto element_tags = element.at(overpass_tags); auto& [key, val] : element_tags.items()) {
                    tags[key] = val;
                }
            } catch (json::basic_json::out_of_range &) {
                // no tags can happen and is no problem
            }
        }
        if (element[overpass_type] == overpass_node) {
            LonLat lon_lat {element[overpass_lon], element[overpass_lat]};
            auto node = std::make_unique<Node>(osm_id, lon_lat, tags);
            nodes_dict[osm_id] = std::move(node);
        } else if (element[overpass_type] == overpass_way) {
            std::vector<osm_id_t> refs;
            for (auto& [key, val] : element[overpass_nodes].items()) {
                refs.push_back(val);
            }
            if (is_relations) {
                auto way = std::make_shared<Way>(osm_id, refs, tags);
                shared_ways_dict[osm_id] = std::move(way);
            } else {
                auto way = std::make_unique<Way>(osm_id, refs, tags);
                ways_dict[osm_id] = std::move(way);
            }
        } else if (element[overpass_type] == overpass_relation) {
            auto relation = std::make_unique<Relation>(osm_id, tags);
            for (auto& [key, val] : element[overpass_members].items()) {
                auto member_role = ParseMemberRole(val[overpass_role], true);
                auto member = std::make_unique<Member>(val[overpass_ref], MemberType::way, member_role);
                relation->AddMember(std::move(member));
            }
            relations_dict[osm_id] = std::move(relation);
        }
    }
}

OSMReadResult OSMDataReaderOverpass::ReadFlat(const std::string &required_key_values, const bool read_node_tags) const {
    std::stringstream query;
    query << "[out:json][bbox:" << CreateOverpassBoundaryString() << "];(";
    query << required_key_values;
    query << ");(._;>;);out;";
    BOOST_LOG_TRIVIAL(debug) << "ReadFlat Overpass QL: " << query.str();

    OSMReadResult read_result {};
    QueryFlat(query.str(), read_node_tags, read_result.nodes_dict, read_result.ways_dict);
    return read_result;
}

OSMReadResult OSMDataReaderOverpass::FetchWaysFromKeys(const std::vector<std::string> &required_keys) const {
    std::stringstream query;
    for (const auto &key : required_keys) {
        query << overpass_way << ConstructOverpassKey(key) << ";";
    }
    return ReadFlat(query.str(), false);
}

OSMReadResult OSMDataReaderOverpass::FetchWaysFromKeyValues(const std::vector<std::pair<std::string, std::string>> &required_key_values) const {
    std::stringstream query;
    for (const auto& [key, val] : required_key_values) {
        query << overpass_way << ConstructOverpassKeyValue(key, val) << ";";
    }
    return ReadFlat(query.str(), false);
}

OSMReadResult OSMDataReaderOverpass::FetchNodesIsolatedFromKeyValues(const std::vector<std::pair<std::string, std::string>> &required_key_values) const {
    std::stringstream query;
    for (const auto& [key, val] : required_key_values) {
        query << overpass_node << ConstructOverpassKeyValue(key, val) << ";";
    }
    return ReadFlat(query.str(), true);
}

OSMReadRelationsResult OSMDataReaderOverpass::ReadRelations(const std::string &features_part) const {
    std::stringstream query;
    query << "[out:json][bbox:" << CreateOverpassBoundaryString() << "];";
    query << features_part;
    query << "(._;>;);out;";
    BOOST_LOG_TRIVIAL(debug) << "ReadRelations Overpass QL: " << query.str();

    OSMReadRelationsResult read_result {};
    QueryRelations(query.str(), read_result.rel_nodes_dict, read_result.rel_ways_dict, read_result.relations_dict);
    return read_result;
}

OSMReadRelationsResult OSMDataReaderOverpass::ReadRelationsSimple(const std::string &required_key_values) const {
    std::stringstream query;
    query << "nwr" << ConstructOverpassKeyValue(k_type, v_multipolygon) << required_key_values << ";";
    return ReadRelations(query.str());
}

OSMReadRelationsResult OSMDataReaderOverpass::FetchRelationsAerodrome() const {
    return ReadRelationsSimple(ConstructOverpassKeyValue(k_aeroway, v_aerodrome));
}

OSMReadRelationsResult OSMDataReaderOverpass::FetchRelationsLanduse() const {
    return ReadRelationsSimple(ConstructOverpassKey(k_landuse));
}

OSMReadRelationsResult OSMDataReaderOverpass::FetchRelationsBuildings() const {
    std::stringstream query;
    query << "(";
    query << "nwr" << ConstructOverpassKeyValue(k_type, v_buildings) << ";";
    query << "nwr" << ConstructOverpassKeyValue(k_type, v_multipolygon) << ConstructOverpassKey(k_building) << ";";
    query << "nwr" << ConstructOverpassKeyValue(k_type, v_multipolygon) << ConstructOverpassKey(k_building_part) << ";";
    query << ");";
    return ReadRelations(query.str());
}

OSMReadRelationsResult OSMDataReaderOverpass::FetchRelationsPlaces() const {
    std::stringstream query;
    query << "(";
    query << "nwr" << ConstructOverpassKeyValue(k_type, v_multipolygon) << ConstructOverpassKeyValue(k_place, v_city) << ";";
    query << "nwr" << ConstructOverpassKeyValue(k_type, v_multipolygon) << ConstructOverpassKeyValue(k_place, v_town) << ";";
    query << "nwr" << ConstructOverpassKeyValue(k_type, v_boundary) << ConstructOverpassKeyValue(k_place, v_city) << ";";
    query << "nwr" << ConstructOverpassKeyValue(k_type, v_boundary) << ConstructOverpassKeyValue(k_place, v_town) << ";";
    query << ");";
    return ReadRelations(query.str());
}

OSMReadRelationsResult OSMDataReaderOverpass::FetchRelationsRiverbanks() const {
    return ReadRelationsSimple(ConstructOverpassKeyValue(k_waterway, v_riverbank));
}

OSMReadRelationsResult OSMDataReaderOverpass::FetchRelationsNaturalWater() const {
    return ReadRelationsSimple(ConstructOverpassKeyValue(k_natural, v_water));
}
